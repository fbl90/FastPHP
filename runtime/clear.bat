rd /s /q doc
mkdir doc

rd /s /q cache
mkdir cache

rd /s /q config
mkdir config

rd /s /q developer_log
mkdir developer_log

rd /s /q log
mkdir log

rd /s /q order
mkdir order

rd /s /q session
mkdir session

rd /s /q temp
mkdir temp

rd /s /q temp_xls
mkdir temp_xls
