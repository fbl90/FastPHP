<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
// [ 应用入口文件 ]
define('BIND_MODULE','index');//绑定前台模块
// 定义应用目录
define('APP_PATH', __DIR__ . '/application/');
// 加载框架引导文件
require __DIR__ . '/core/thinkphp/start.php';