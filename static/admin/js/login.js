$(function(){
	var Aroot = $("body").attr('bind-root');
    //自动处理密码输入框避免类似360浏览器自动填充信息
    $(".password_input").focusin(function(){
    	$(this).attr('type','password');
    });
    
	$(".login-form #password").keypress(function(event){
        if(event.keyCode == 13){
            $("#login_btn").click();
        }
	});
	$("#checkcode").keypress(function(event){
        if(event.keyCode == 13){
            $("#login_btn").click();
        }
	});	
  	$(".reload_checkcode_btn").click(function(){
  		$(this).find("img").attr('src',Aroot+'login/showcheckcode?'+Math.floor(Math.random()*10000000000));
  	});
	$("#login_btn").click(function(){
		if(FormCheck("#login_frm")){
	        data=Form_Post(
	            "#login_frm",
	            Aroot+'login/login',
	            Aroot+'/index/'
	        );
		}
		return false;
	});
	$("#unlock_password_input").keypress(function(event){
        if(event.keyCode == 13){
            $("#unlock_btn").click();
            return false;
        }
	});
	$("#unlock_btn").click(function(){
		if(FormCheck("#unlock_frm")){
	        data=Form_Post(
	            "#unlock_frm",
	            Aroot+'login/unlock',
	            Aroot+'/index/'
	        );
		}
		return false;
	});
	
});
