/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function () {
	var Aroot = $("body").attr('bind-root');
	//展开管理菜单主分类
	var url = window.location.pathname;
	var sel = $(".side-nav li a[href='" + url + "']");
	if (sel.length >= 1) {
		$.each(sel, function (index, item) {
			//menu-type
			if ($(this).attr('menu-type') == 'main') {

			} else if ($(this).attr('menu-type') == 'sub') {
				$(this).parent('li').addClass('active');
				$(this).parent('li').parent('ul').parent('li').addClass('active');
			}
		});
	}

	//自动处理密码输入框避免类似360浏览器自动填充信息
	$(".password_input").focusin(function () {
		$(this).attr('type', 'password');
	});
	$("#clear_all_cache_btn").click(function () {
		get_json(Aroot + 'index/CacheClear', {}, false);
	});
	
	//显示/隐藏密钥类型配置
	$(".secret_txt_show").click(function(){
		var id=$(this).attr('attr-id');
		var ele=$("#"+id);
		if(ele.attr('type')=='password'){
			$(this).find('i').removeClass('icon-eye-fill');
			$(this).find('i').addClass('icon-eye');
			ele.attr('type','text');
		}else{
			$(this).find('i').removeClass('icon-eye');
			$(this).find('i').addClass('icon-eye-fill');			
			ele.attr('type','password');
		}
	});	
	
	//全局保存配置
	$(".add_config_save_btn").click(function () {
		var classname = $(this).attr("attr-class");
		var frm_id = "#config_" + classname + "_frm";
		data = Form_Post(frm_id, Aroot + 'sysset/save?classname=' + classname,'reload');
	});
	//显示配置与删除
	$(".config_edit_frm .form-group").hover(function () {
		$(this).find('.del_config').show();
		$(this).find('.edit_config').show();
	}, function () {
		$(this).find('.del_config').hide();
		$(this).find('.edit_config').hide();
	});

	//删除一个配置
	$(".config_edit_frm").find('.del_config').click(function () {
		var id = $(this).attr('attr-id');
		Confirm("删除此配置有可能导致系统严重故障！您确认删除此配置？", function () {
			get_json(Aroot + 'sysset/del_config', { id: id }, false);
		}, function () {
			return false;
		});
	});
	
	//修改配置获得原配置信息
	$(".config_edit_frm").find('.edit_config').click(function () {
		var id = $(this).attr('attr-id');
		var data = get_json(Aroot + 'sysset/get_config_data', {
			id: id
		}, true);
		data=data.data;
		var edit_frm = $("#edit_config_frm");
		Data_loader(edit_frm, 'input', 'id', data.id, '');
		Data_loader(edit_frm, 'select', 'classid', data.classid, '');
		Data_loader(edit_frm, 'input', 'group', data.group, '');
		Data_loader(edit_frm, 'input', 'name', data.name, '');
		Data_loader(edit_frm, 'input', 'title', data.title, '');
		Data_loader(edit_frm, 'radio', 'type', data.type, '');
		Data_loader(edit_frm, 'textarea', 'data', data.data, '');
		Data_loader(edit_frm, 'input', 'defaultvalue', data.defaultvalue, '');
		Data_loader(edit_frm, 'input', 'hint', data.hint, '');
		Data_loader(edit_frm, 'radio', 'isdeveloper', data.isdeveloper, '');
		Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
	});
	//提交配置修改信息
	$("#edit_config_btn").click(function () {
		if (FormCheck("#edit_config_frm")) {
			data = Form_Post("#edit_config_frm", Aroot + 'sysset/save_config','reload');
		}
		return false;
	});


	//新版通用删除操作
	$(".Fast_Del").click(function(){
		var id = $(this).attr('attr-id');
		var msg = $(this).attr('attr-msg');
		var model = $(this).attr('attr-model');
		if (msg == "" || typeof (msg) == 'undefined') {
			msg = '您确认删除此信息?';
		}
		//扩展属性
		var excid=$(this).attr('attr-excid');
		if (excid == "" || typeof (excid) == 'undefined') {
			excid = 0;
		}

		Confirm(msg, function(){
			get_json(Aroot + model+'/del', { id: id, excid:excid}, false);
			return true;
		}, function () {
			return true;
		});
	});

	//新版通用更新排序
	$("#Fast_Sort").click(function () {
		var model = $(this).attr('attr-model');
		var frmId = $(this).attr('attr-frm')?"#"+$(this).attr("attr-frm"):"#"+model;
		Form_Post(frmId, Aroot + model+"/update_sort", "reload");
	});
	
	//新版通用批量操作
	$(".Fast_Bat").click(function(){
		var type = $(this).attr('attr-type');
		var classname = $(this).attr('attr-class');
		var model = $(this).attr('attr-model');
		var ids = Get_Checkbox_Vals(classname);

		//扩展属性
		var excid=$(this).attr('attr-excid');
		if (excid == "" || typeof (excid) == 'undefined') {
			excid = 0;
		}

		if(ids.length<=0){
			layer.msg('您最少要选择一个才能操作！');
			return false;
		}
		var txt = $(this).text();
		var ids = ids.join(',');
		Confirm('您确认【' + txt + '】选中的信息', function () {
			get_json(Aroot + model+'/bat_set', {
				type: type,
				ids: ids,
				excid:excid
			}, false);
			return true;
		}, function () {
			return true;
		});
	});
	
	//新版通用数据表字段修改
	$(".Fast_Action").click(function(){
		var id = $(this).attr('attr-id');
		var model = $(this).attr('attr-model');
		var field = $(this).attr('attr-field');
		var value = $(this).attr('attr-val');
		get_json(Aroot + model+'/common_action', {
			id: id,
			field: field,
			value: value
		}, false);
	});
	//新版通用表单提交方法
	$(".Fast_Form_Post").click(function(){
		var frm_id = $(this).attr("attr-frm");
		var model = $(this).attr("attr-model");
		var active = $(this).attr("attr-active");
		var callback = $(this).attr("attr-callback");
		if (active == "" || typeof (active) == 'undefined') {
			active = "save_" + model;
		}
		if (callback == "" || typeof (callback) == 'undefined') {
			callback = "";
		}
		if (FormCheck(frm_id)){
			data = Form_Post(frm_id, Aroot + model + '/' + active,callback);
		}
		return false;
	});
	
	//全局批量选择事件处理
	$(".check_select_btn").click(function () {
		var bindcls = $(this).attr('attr-bind');
		var sel_type = $(this).attr('attr-type');
		Checkbox_Select("." + bindcls, sel_type);
	});

	//通用搜索处理
	$(".search_btn").click(function () {
		var kw_ipt = $(this).attr('attr-ipt');
		var fields = $(this).attr('attr-search-fields');
		var cid = $(this).attr('attr-cid');
		var surl = $(this).attr('attr-url');
		var kw = $('.' + kw_ipt).val();
		
		if (cid == "" || typeof (cid) == 'undefined') {
			cid="";
		}
		if (fields == "" || typeof (fields) == 'undefined') {
			fields="";
		}
		
		var url = surl + '?cid='+cid+'&kw=' + kw+'&kw_field='+fields;
		window.location.href = url;
	});

	//通用搜索回车事件处理
	$(".enter_goto").keypress(function (event) {
		var eve = $(this).attr('attr-event');
		if (event.keyCode == 13) {
			$("." + eve).click();
		}
	});
	/*
		通用图片放大预览
	*/
	$("body").on("click",".preview_img", function () {
		var src = $(this).attr('src');
		console.log(src);
		layer.open({
			type: 1,
			title: '图片预览',
			closeBtn: 1,
			area: ['900px', '700px'],
			skin: 'layui-layer-nobg', //没有背景色
			shadeClose: true,
			content: '<div class="preview_box"><img src="' + src + '" /></div>'
		});
	});
	//通用图标选择
	$(".select_icon_box").click(function () {
		layer.open({
			type: 1,
			title: false,
			closeBtn: 0,
			area: ['900px','600px'],
			skin: 'layui-layer-nobg', //没有背景色
			shadeClose: true,
			content: $('#select_icon_box')
		});
		
		$("#setinputid").val($(this).attr('attr-id'));
	});

	//选择图标
	$(".icon_list a").click(function(){
		var classname = $(this).attr('attr-icon');
		$($("#setinputid").val()).val(classname);
		layer.closeAll();
	});
	//生成新的安装数据库
	$("#create_install_db_btn").click(function(){
		layer.msg('正在生成中,请稍后...',{time:5000});
		Confirm('您确认生成新的安装包？',function(){
			get_json(Aroot+'install/create_install_db',{},false);
		},function(){
			return false;
		});
	});
	//清理缓存日志目录
	$("#clear_cache_log_btn").click(function(){
		Confirm('如果您在生产环境请慎用!您确认清理？',function(){
			get_json(Aroot+'install/clear_cache_log_dir',{},false);
		},function(){
			return false;
		});
	});
});