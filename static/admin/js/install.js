$(function(){
	var Aroot = $("body").attr('bind-root');
	
	//通用表单检查提交
	$(".post_btn").click(function () {
		var frm_id = $(this).attr("attr-frm");
		var model = $(this).attr("attr-model");
		var active = $(this).attr("attr-active");
		var callback = $(this).attr("attr-callback");
		if (active == "" || typeof (active) == 'undefined'){
			active = "save_" + model;
		}
		if (callback == "" || typeof (callback) == 'undefined'){
			callback = "";
		}
		
		layer.msg('正在安装中,请稍后...',{time:5000});
		if (FormCheck(frm_id)){
			data = Form_Post(frm_id, Aroot + model + '/' + active,callback);
		}
		
		return false;
	});
});
