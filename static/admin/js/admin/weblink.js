/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function () {
    var Aroot = $("body").attr('bind-root');
    var Method = $("body").attr('bind-Method');
    
    var edit_frm = $("#edit_weblink_frm");

    $("#add_weblink_dig").click(function () {
        Form_Reset("#edit_weblink_frm");
        Data_loader(edit_frm, 'image', 'picurl','', '.picurl');
        $(".dig_title").text('创建友情连接');
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot + 'weblink/get_weblink_info', {
            id: id
        }, true);
        data=data.data;
        $(".dig_title").text('修改友情连接【' + data.webname + '】');
        Data_loader(edit_frm, 'select', 'classid', data.classid, '');
        Data_loader(edit_frm, 'input', 'webname', data.webname, '');
        Data_loader(edit_frm, 'input', 'linkurl', data.linkurl, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
    });

});