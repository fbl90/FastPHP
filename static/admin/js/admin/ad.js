/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    
    var add_edit_frm = $("#edit_ad_frm");

    //广告类型切换
    add_edit_frm.find('input[name=type]').click(function () {
        var type = $(this).val();
        $(".ad_type").hide();
        $(".ad_type_" + type).show();
        if (type == 2) {
            UE.getEditor('editer', {
                serverUrl: '/static/plugin/ueditor/php/controller.php',
                toolbars: Get_UEditor_Config('single')
            }
            )
        } else if (type == 3) {
            File_Upload('.picurl', 'image', 'picurl', false);
        } else if (type == 4) {
            File_Upload('.picarr', 'image', 'picarr', true);
        }
    });
    add_edit_frm.find('input[name=type][value=1]').click();
    //开始处理加载修改相关数据和处理
    var edit_id = add_edit_frm.find('input[name=id]').val();
    if (edit_id >= 1) {
        var data = get_json(Aroot +'ad/get_ad_info', { id: edit_id }, true);
        var data = data.data;

        Data_loader(add_edit_frm, 'input', 'aid', data.aid, '');
        Data_loader(add_edit_frm, 'input', 'title', data.title, '');
        Data_loader(add_edit_frm, 'input', 'linkurl', data.linkurl, '');
        Data_loader(add_edit_frm, 'input', 'remark', data.remark, '');
        Data_loader(add_edit_frm, 'radio', 'type', data.type, '');
        Data_loader(add_edit_frm, 'input', 'orderid', data.orderid, '');
        Data_loader(add_edit_frm, 'select', 'classid', data.classid, '');
        if (data.type == 1) {
            Data_loader(add_edit_frm, 'textarea', 'text', data.text, '');
        } else if (data.type == 2) {
            Data_loader(add_edit_frm, 'textarea', 'editer', data.editer, '');
        } else if (data.type == 3) {
            Data_loader(add_edit_frm, 'image', 'picurl', data.picurl, '.picurl');
        } else if (data.type == 4) {
            Data_loader(add_edit_frm, 'image', 'picarr', data.picarr, '.picarr');
        } else if (data.type == 5) {
            Data_loader(add_edit_frm, 'textarea', 'code', data.code, '');
        }
        add_edit_frm.find('input[name=type][value=' + data.type + ']').click();
              
    }      
});