/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    var edit_frm = $("#edit_slider_frm");
    $("#add_slider_dig").click(function () {
        Form_Reset("#edit_slider_frm");
        Data_loader(edit_frm, 'image', 'picurl_pc', '', '.picurl_pc');
        Data_loader(edit_frm, 'image', 'picurl_m', '', '.picurl_m');        
        $(".dig_title").text('创建幻灯片');
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'slider/get_silder_info', { id: id }, true);
        data=data.data;
        $(".dig_title").text('修改幻灯片【' + data.title + '】');
        Data_loader(edit_frm, 'select', 'classid', data.classid, '');
        Data_loader(edit_frm, 'select', 'gclassid', data.gclassid, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'title', data.title, '');
        Data_loader(edit_frm, 'input', 'stitle', data.stitle, '');
        Data_loader(edit_frm, 'input', 'linkurl', data.linkurl, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'image', 'picurl_pc', data.picurl_pc, '.picurl_pc');
        Data_loader(edit_frm, 'image', 'picurl_m', data.picurl_m, '.picurl_m');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
    });  
});