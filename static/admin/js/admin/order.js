/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    $(".more_btn").click(function () {
        var id = $(this).attr('attr-id');
        var status = $("#goods_" + id).css('display');
        $(".goods_more").hide();
        if (status == 'table-row') {
            $("#goods_" + id).hide();
        } else {
            $("#goods_" + id).show();
        }
    });

    $(".set_status_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'order/get_order_info', { id: id }, true);
        data=data.data;
        Data_loader($("#order_status_frm"), 'input', 'id', data.id, '');
        Data_loader($("#order_status_frm"), 'radio', 'status', data.status, '');
    });

    $(".order_send_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'order/get_order_info', { id: id }, true);
        data=data.data;
        Data_loader($("#order_send_frm"), 'input', 'id', data.id, '');
        Data_loader($("#order_send_frm"), 'input', 'express_name', data.express_name, '');
        Data_loader($("#order_send_frm"), 'input', 'express_id', data.express_id, '');
        Data_loader($("#order_send_frm"), 'input', 'express_tel', data.express_tel, '');
        Data_loader($("#order_send_frm"), 'input', 'express_weight', data.express_weight, '');
    });
    
    $(".set_amount_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'order/get_order_info', { id: id }, true);
        data=data.data;
        Data_loader($("#order_amount_frm"), 'input', 'amount', data.amount, '');
        Data_loader($("#order_amount_frm"), 'input', 'id', data.id, '');
    });

    $("#save_order_status_btn").click(function () {
        if (FormCheck("#order_status_frm")) {
            data = Form_Post("#order_status_frm", Aroot +'order/update_status', 'reload');
        }
        return false;
    });
    $("#save_order_send_btn").click(function () {
        if (FormCheck("#order_send_frm")) {
            data = Form_Post("#order_send_frm", Aroot +'order/update_express', 'reload');
        }
        return false;
    });
    $("#save_order_amount_btn").click(function () {
        if (FormCheck("#order_amount_frm")) {
            data = Form_Post("#order_amount_frm", Aroot +'order/update_amount', 'reload');
        }
        return false;
    });

    //订单导出
    $("#export_xls_btn").click(function () {
        var data = Form_Post_Return("#search_frm", Aroot +'order/export_xls');
		if(data.status!=0){
			layer.msg(data.msg);
			return false;
		}
        if ($("#xls_download_iframe").length >= 1) {
            $("#xls_download_iframe").remove();
        }
        var html = '<iframe src="' + data.data + '" id="xls_download_iframe" width="0" height="0"></iframe>';
        $("html").append(html);
    });

    File_Upload('.xlsfile', 'file', 'xlsfile', false);

    //批量发货操作
    $("#save_order_bat_send_btn").click(function () {
        if (FormCheck("#order_bat_send_frm")) {
            data = Form_Post_Return("#order_bat_send_frm", Aroot +'order/order_bat_send');
			if(data.status!=0){
				layer.msg(data.msg);
				return false;
			}
            $.each(data.data, function (i, n) {
                if ($.trim(n.J) == '') {
                    t2 = '<span class="label label-important">无物流公司</span>';
                } else {
                    t2 = n.J;
                }

                if ($.trim(n.K) == '') {
                    t3 = '<span class="label label-important">无物流单号</span>';
                } else {
                    t3 = n.K;
                }
                try {
                    var type = n.order.oid;
                    var status = n.order.status;
                    if (status == 2) {
                        if ($.trim(n.J) != "" && $.trim(n.K) != "") {
                            t5 = '<span class="label label-info ok_send_li">可发货</span>';
                        } else {
                            t5 = '<span class="label label-important">不可发货</span>';
                        }
                    } else {
                        t5 = '<span class="label label-important">不可发货</span>';
                    }
                    enable = '';
                } catch (err) {
                    var type = '';
                    enable = '<span class="label label-important">无效</span>';
                    t5 = '<span class="label label-important">不可发货</span>';
                }
                $('#order_batsend_data_list').append('<tr>' +
                    '<td>' + n.A + enable + '</td>' +
                    '<td>' + n.B + '</td>' +
                    '<td>' + n.D + '</td>' +
                    '<td>' + n.E + '</td>' +
                    '<td>' + n.F + '</td>' +
                    '<td>' + n.G + '</td>' +
                    '<td>' + n.H + '</td>' +
                    '<td>' + t2 + '</td>' +
                    '<td>' + t3 + '</td>' +
                    '<td>' + n.L + t5 + '</td>' +
                    '<td id="send_status_' + $.trim(n.A) + '"></td>' +
                    '</tr>');
            });
        }
        return false;
    });

    //发货
    $("#order_bat_send_btn").click(function () {
        if ($('.ok_send_li').length >= 1) {
            if (confirm('您确认发货？')) {
                var result = get_json_nomsg(Aroot +'order/batorder_sends', {});
                $.each(result.data, function (i, n) {
                    $("#send_status_" + n).html('<span class="label label-success">发货成功</span>');
                });
                return true;
            } else {
                return true;
            }
        } else {
            layer.msg('当前没有可发货的订单，请检查！');
        }
    });

    //清空历史订单
    $("#clear_all_btn").click(function () {
        Confirm('您确认清空所有历史订单?', function () {
            var data = get_json_nomsg(Aroot +'order/clear_all_history', {});
            if (data.status == 0) {
                window.location.reload();
            }
        }, function () {
            return true;
        });

    });
});