/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    $("#add_menu_btn").click(function () {

        $(".dig_title").text('创建新菜单');
        Form_Reset("#edit_node_frm");

        var orderid = get_json(Aroot +'menu/get_orderid', { pid: 0 }, true);
        $("#edit_node_frm").find("input[name='orderid']").val(orderid.data.count);
    });
    $("#edit_node_frm").find("select[name='pid']").change(function () {
        var pid = $(this).val();
        var orderid = get_json(Aroot +'menu/get_orderid', { pid: pid }, true);
        $("#edit_node_frm").find("input[name='orderid']").val(orderid.data.count);
    });
    //管理菜单修改获得原数据
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var frm = $("#edit_node_frm");
        var data = get_json(Aroot +'menu/get_data', { id: id }, true);
        data=data.data;
        Data_loader(frm, 'input', 'id', data.id, '');
        Data_loader(frm, 'input', 'menu_icon', data.menu_icon, '');
        Data_loader(frm, 'input', 'orderid', data.orderid, '');
        Data_loader(frm, 'input', 'menu_name', data.menu_name, '');
        Data_loader(frm, 'input', 'menu_url', data.menu_url, '');
        Data_loader(frm, 'select', 'menu_pid', data.menu_pid, '');
        Data_loader(frm, 'radio', 'isshow', data.isshow, '');
    });    
});