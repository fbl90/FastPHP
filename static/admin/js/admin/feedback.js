/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    $(".show_more_btn").click(function () {
        var id = $(this).attr('attr-id');
        $(".feedback_content").hide();
        $("#feedback_content_" + id).show();
    });	
});