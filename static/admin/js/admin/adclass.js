/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    var edit_frm = $("#edit_ad_class_frm");
    $("#add_ad_dig").click(function () {
        $(".dig_title").text('创建新的广告分类');
        Form_Reset("#edit_ad_class_frm");
        File_Upload('.picurl', 'image', 'picurl', false);
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'adclass/get_adclass_info', { id: id }, true);
        var data = data.data;
        $(".dig_title").text('修改广告【' + data.classname + '】');
        Data_loader(edit_frm, 'input', 'classname', data.classname, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl');
    }); 
});