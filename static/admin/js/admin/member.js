/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

	//创建新会员初始化相关信息
    var area = "";
    $("#add_member_btn").click(function () {
        File_Upload('.faceupload', 'face', 'facepic', false);
        new PCAS("area_p", "area_c", "area_a", "湖北省", "宜昌市", "西陵区");
        $("select[name=area_p],select[name=area_c],select[name=area_a]").change(function () {
            var p = $("select[name=area_p]").val();
            var c = $("select[name=area_c]").val();
            var a = $("select[name=area_a]").val();
            area = p + c + a;
        });
        $("select[name=area_p],select[name=area_c],select[name=area_a]").change();
		$(".baidumap_ipt_btn").click(function(){
			var bind=$(this).attr('attr-bind');
			var defaults=$(this).attr('attr-default');
			var porints=Show_BaiduMap(bind,defaults);
		});
    });
    //加载详情界面
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        layer.open({
            type: 2,
            title: '会员详情',
            shadeClose: true,
            shade: 0.8,
            area: ['800px', '500px'],
            content: 'member_info?id='+id //iframe的url
          }); 
    });
	//修改密码
    $(".passwd_btn").click(function () {
        var pass_frm = $("#edit_passwd_frm");
        var id = $(this).attr('attr-id');
        pass_frm.find('input[name=id]').val(id);
    });
    //快速登录
    $(".login_btn").click(function(){
        var uid=$(this).attr('attr-id');
        var userinfo=get_json_nomsg(Aroot+'member/user_login',{'uid':uid});
        if(userinfo.status==0){
            window.open('/');
        }
    }); 	
});