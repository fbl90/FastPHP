/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    var edit_frm = $("#edit_express_frm");
    $("#add_express_btn").click(function () {
        //初始化表单值
        $(".dig_title").text('创建新的物流公司');
        Form_Reset("#edit_express_frm");
        Data_loader(edit_frm, 'image', 'logo', '', '.picurl');
    });
    //修改
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'express/get_express', { id: id }, true);
        data=data.data;
        $(".dig_title").text('修改【' + data.name + '】物流公司');
        Data_loader(edit_frm, 'input', 'name', data.name, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'no', data.no, '');
        Data_loader(edit_frm, 'image', 'logo', data.logo, '.picurl');
        Data_loader(edit_frm, 'radio', 'common', data.common, '');
        
    });
});