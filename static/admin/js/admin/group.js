/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    var edit_frm = $("#edit_group_frm");
    $("#add_group_btn").click(function () {
        Form_Reset("#edit_group_frm");
        $(".dig_title").text('创建新分组');
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'group/get_group_info', { id: id }, true);
        $(".dig_title").text('修改分组');

        Data_loader(edit_frm, 'input', 'group_name', data.group_name, '');
        Data_loader(edit_frm, 'input', 'min_amount', data.min_amount, '');
        Data_loader(edit_frm, 'input', 'min_amount_y', data.min_amount_y, '');
        Data_loader(edit_frm, 'input', 'max_amount', data.max_amount, '');
        Data_loader(edit_frm, 'input', 'max_amount_y', data.max_amount_y, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'image', 'group_picurl', data.group_picurl, '.group_picurl');
        Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
        Data_loader(edit_frm, 'input', 'discount', data.discount, '');

    });
});