/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    var Method = $("body").attr('bind-Method');
    var edit_frm = $("#edit_weblink_class_frm");

    $("#add_weblink_dig").click(function () {
        Form_Reset("#edit_weblink_class_frm");
        Data_loader(edit_frm, 'image', 'picurl', '', '.picurl');
        $(".dig_title").text('创建链接分类');
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot + 'weblinkclass/get_class_info', { id: id }, true);
        data=data.data;
        $(".dig_title").text('修改链接分类【' + data.classname + '】');
        Data_loader(edit_frm, 'input', 'classname', data.classname, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl');
    });

});