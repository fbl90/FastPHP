/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function () {
    var Aroot = $("body").attr('bind-root');
    var Method = $("body").attr('bind-Method');
    var edit_frm = $("#edit_nav_class_frm");


    $("#add_navclass_dig").click(function () {
        //初始化表单值
        $(".dig_title").text('创建新的导航分类');
        Form_Reset("#edit_nav_class_frm");
        Data_loader(edit_frm, 'image', 'picurl', '', '.picurl');
    });

    //修改
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot + 'navclass/get_class_info', {
            id: id
        }, true);
        data=data.data;
        $(".dig_title").text('修改导航分类【' + data.classname + '】');
        Data_loader(edit_frm, 'input', 'classname', data.classname, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl');
    });

});