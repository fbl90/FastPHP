/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    File_Upload('.profile_faceupload', 'face', 'facepic', false);

    //修改我的基本信息
    $("#change_profile_btn").click(function () {
        if (FormCheck("#change_profile_frm")) {
            data = Form_Post("#change_profile_frm", Aroot +'profile/save', 'reload');
        }
        return false;
    });
    //修改我的密码
    $("#change_password_btn").click(function () {
        if (FormCheck("#change_password_frm")) {
            data = Form_Post("#change_password_frm", Aroot +'profile/change_password', 'reload');
        }
        return false;
    }); 
});