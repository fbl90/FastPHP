/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    $("#add_admin_btn").click(function () {
        File_Upload('.faceupload', 'face', 'facepic', false);
    });
    //编辑修改管理员
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'admin/get_admininfo', { id: id }, true);
        var edit_frm = $("#edit_admin_frm");
		data=data.data;
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'select', 'type', data.type, '');
        Data_loader(edit_frm, 'input', 'username', data.username, '');
        Data_loader(edit_frm, 'input', 'nickname', data.nickname, '');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
        Data_loader(edit_frm, 'image', 'faceupload_edit', data.facepic, '.faceupload_edit');
        Data_loader(edit_frm, 'text', '.login_time', data.logintime, '');
        Data_loader(edit_frm, 'text', '.reg_time', data.regtime, '');
        Data_loader(edit_frm, 'text', '.reg_ip', data.regip, '');
        Data_loader(edit_frm, 'text', '.login_ip', data.loginip, '');
    });

    $(".passwd_btn").click(function () {
        var pass_frm = $("#edit_passwd_frm");
        var id = $(this).attr('attr-id');
        pass_frm.find('input[name=id]').val(id);
    });  

    $("#select_all_btn").click(function () {
        Checkbox_Select('.cbox', 'all');
    });
    $("#select_un_btn").click(function () {
        Checkbox_Select('.cbox', 'reverse');
    });
    $("#select_cancel_btn").click(function () {
        Checkbox_Select('.cbox', 'cancel');
    });
    $(".cats_select_btn").click(function () {
        var cid = $(this).attr('attr-cid');
        var checked = $(this).prop('checked');
        var cls_name = ".menu_node_" + cid;
        
        if (checked == true) {
            Checkbox_Select(cls_name, 'all');
        } else {
            Checkbox_Select(cls_name, 'cancel');
        }
    });
    //选择后加载对应权限设置
    $("select[name=admin_id]").change(function () {
        var uid = $(this).val();
        if (uid <= 0) {
            layer.msg('请选择一个管理员！');
            return false;
        }
        var data = get_json(Aroot +'admin/get_privilege', { uid: uid }, true);
        // console.log(data.data.menu);
        var str = data.data.menu;
        var edit_frm = $("#edit_privilege_frm");
        Checkbox_Select('.cbox', 'cancel');
        Data_loader(edit_frm, 'checkbox', 'menu', str, '');

    });
    $("select[name=admin_id]").change();    
});