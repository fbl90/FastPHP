/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
	
	$("#table_select").change(function(){
		var table=$(this).val();
		var list=get_json_nomsg(Aroot+'fileclear/get_field',{table:table});
		$("#field_select").html('<option value="0">请选择数据字段</option>');
		$.each(list.data,function(index,item){
			$("#field_select").append('<option value="'+item.field_name+'">【'+item.field_name+'】-'+item.comment+'-'+item.field_type+'</option>');
		});
	});
	
	//立即清理
	$("#fileclear_btn").click(function(){
		Confirm('您确认清理目录？<br />无效文件将移动到"\\uploads_invalid\\"目录,不会删除，您可以手动恢复!',function(){
			var result=get_json_nomsg(Aroot+'fileclear/scan_database',{});
			if(result.status==0){
				layer.msg('共清理：'+result.data+'个文件');
			}else{
			 	layer.msg(result.msg);
			}
		},function(){});
	});
	
	//完全删除无效文件
	$("#clearinvalid_btn").click(function(){
		Confirm('您确认完全删除"\\uploads_invalid\\"目录中的无效文件,将无法恢复!',function(){
			var result=get_json_nomsg(Aroot+'fileclear/clearinvalid',{});
			if(result.status==0){
				layer.msg('删除完成');
			}else{
			 	layer.msg(result.msg);
			}
		},function(){});
	});
});