/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    var edit_frm = $("#edit_goodsclass_frm");
    //创建子栏目
    $(".add_sub_btn").click(function () {
        var id = $(this).attr('attr-id');
        Form_Reset("#edit_goodsclass_frm");
        edit_frm.find('select[name=parent]').val(id);
        Data_loader(edit_frm, 'image', 'picurl','', '.picurl_upload');
        $(".dig_title").text('创建产品子栏目');
    });
    $("#add_goodsclass_btn").click(function () {
        //加载上传组件
        Form_Reset("#edit_goodsclass_frm");
        $(".dig_title").text('创建新产品栏目');
        Data_loader(edit_frm, 'image', 'picurl','', '.picurl_upload');
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'goodsclass/get_goodsclass_info', { id: id }, true);
        $(".dig_title").text('修改产品栏目');
        data=data.data;
        Data_loader(edit_frm, 'select', 'parent', data.parent, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'classname', data.classname, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl_upload');
        Data_loader(edit_frm, 'radio', 'isnav', data.isnav, '');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
    });
});