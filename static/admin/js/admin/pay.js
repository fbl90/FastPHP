/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
	
	var payrecord_list=$("#payrecord_list_page");
	if(payrecord_list.length>=1){
		$("#export_xls_btn").click(function () {
			var data = Form_Post_Return("#payrecord_search_frm", Aroot +'pay/export_xls');
			
			if(data.status!=0){
				layer.msg(data.msg);
				return false;
			}
			
			if ($("#xls_download_iframe").length >= 1) {
				$("#xls_download_iframe").remove();
			}
			var html = '<iframe src="' + data.data + '" id="xls_download_iframe" width="0" height="0"></iframe>';
			$("html").append(html);
		});
	}
	
	var pay_drawcash=$("#pay_drawcash_page");
	if(pay_drawcash.length>=1){
		$(".allow_btn").click(function () {
			var id = $(this).attr('attr-id');
			get_json(Aroot +'pay/drawcash_allow', { id: id }, false);
		});
		$(".cancel_btn").click(function () {
			var id = $(this).attr('attr-id');
			get_json(Aroot +'pay/drawcash_cancel', { id: id }, false);
		});

		$("#export_xls_btn").click(function () {
			var data = Form_Post_Return("#record_search_frm", Aroot +'pay/drawcash_export_xls');
			if(data.status!=0){
				layer.msg(data.msg);
				return false;
			}			
			if ($("#xls_download_iframe").length >= 1) {
				$("#xls_download_iframe").remove();
			}
			var html = '<iframe src="' + data.data + '" id="xls_download_iframe" width="0" height="0"></iframe>';
			$("html").append(html);
		});
	}

});