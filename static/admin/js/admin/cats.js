/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    var edit_frm = $("#edit_cats_frm");
    $("#add_cats_btn").click(function () {
        $(".dig_title").text('创建新栏目');
        Form_Reset("#edit_cats_frm");
        Data_loader(edit_frm, 'image', 'picurl', '', '.picurl_upload');
    });
    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'cats/get_cats_info', { id: id }, true);
        data=data.data;
        $(".dig_title").text('修改栏目');
        Data_loader(edit_frm, 'select', 'parent', data.parent, '');
        Data_loader(edit_frm, 'select', 'mid', data.mid, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'classname', data.classname, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl_upload');
        Data_loader(edit_frm, 'radio', 'isadmindel', data.isadmindel, '');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
    });
    //创建子栏目
    $(".add_sub_btn").click(function () {
        var id = $(this).attr('attr-id');
        $(".dig_title").text('创建子栏目');
        Form_Reset("#edit_cats_frm");
        edit_frm.find('select[name=parent]').val(id);
        Data_loader(edit_frm, 'image', 'picurl', '', '.picurl_upload');
    });
});