/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function () {
    var Aroot = $("body").attr('bind-root');
    var Method = $("body").attr('bind-Method');
    var edit_frm = $("#edit_model_frm");


    $("#add_mobelfield_btn").click(function () {
        Form_Reset("#edit_model_field_frm");
        $(".dig_title").text('创建新字段');
    });

    var edit_field_frm = $("#edit_model_field_frm");

    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot + 'modelfield/get_field_info', {
            id: id
        }, true);
        var edit_frm = $("#edit_model_field_frm");
        data = data.data;
        $(".dig_title").text('修改字段');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'field_name', data.field_name, '');
        Data_loader(edit_frm, 'input', 'field_title', data.field_title, '');
        Data_loader(edit_frm, 'input', 'filed_length', data.filed_length, '');
        Data_loader(edit_frm, 'input', 'data_length', data.data_length, '');
        Data_loader(edit_frm, 'input', 'hint', data.hint, '');
        Data_loader(edit_frm, 'input', 'default_value', data.default_value, '');
        Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
        Data_loader(edit_frm, 'textarea', 'field_data', data.field_data, '');
        Data_loader(edit_frm, 'select', 'mid', data.mid, '');
        Data_loader(edit_frm, 'select', 'field_type', data.field_type, '');
        Data_loader(edit_frm, 'select', 'data_type', data.data_type, '');
        Data_loader(edit_frm, 'select', 'verify_type', data.verify_type, '');
        Data_loader(edit_frm, 'input', 'verify_len', data.verify_len, '');

        Data_loader(edit_frm, 'radio', 'status', data.status, '');
        Data_loader(edit_frm, 'radio', 'list_show', data.list_show, '');
        Data_loader(edit_frm, 'radio', 'search_type', data.search_type, '');
        Data_loader(edit_frm, 'radio', 'unsigned', data.unsigned, '');
    });

});