/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    var edit_frm = $("#edit_goodsbrand_frm");

    $("#add_goodsbrand_btn").click(function () {
        Form_Reset("#edit_goodsbrand_frm");
        $(".dig_title").text('创建新品牌');
    });

    $(".edit_btn").click(function () {

        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'goodsbrand/get_goodsbrand_info', { id: id }, true);
        $(".dig_title").text('修改品牌');
        Data_loader(edit_frm, 'select_mult', 'parents', data.parents, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'classname', data.brand_name, '');
        Data_loader(edit_frm, 'textarea', 'description', data.brand_description, '');
        Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
        Data_loader(edit_frm, 'image', 'picurl', data.brand_pic, '.picurl_upload');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');

    }); 
});