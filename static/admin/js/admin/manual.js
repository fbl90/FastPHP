/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    $(".show_manual_btn").click(function () {
        var id = $(this).attr('attr-id');
        layer.open({
            type: 2,
            title: '手册详情',
            shadeClose: true,
            shade: 0.8,
            area: ['80%', '80%'],
            content: Aroot +'manual/show/id/' + id //iframe的url
        });
    });
});