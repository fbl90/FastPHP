/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function () {
    var Aroot = $("body").attr('bind-root');
    var Method = $("body").attr('bind-Method');

    var edit_frm = $("#edit_nav_frm");
    $("#add_nav_dig").click(function () {
        Form_Reset("#edit_nav_frm");
        Data_loader(edit_frm, 'image', 'picurl', '', '.picurl');
        $(".dig_title").text('创建导航');
    });

    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot + 'nav/get_nav_info', {
            id: id
        }, true);
        data = data.data;
        $(".dig_title").text('修改导航【' + data.navname + '】');
        Data_loader(edit_frm, 'select', 'classid', data.classid, '');
        Data_loader(edit_frm, 'select', 'parentid', data.parentid, '');
        Data_loader(edit_frm, 'input', 'navname', data.navname, '');
        Data_loader(edit_frm, 'input', 'linkurl', data.linkurl, '');
        Data_loader(edit_frm, 'input', 'icon_class', data.icon_class, '');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'image', 'picurl', data.picurl, '.picurl');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
        Data_loader(edit_frm, 'radio', 'openmode', data.openmode, '');
        Data_loader(edit_frm, 'input', 'orderid', data.orderid, '');
    });
    //创建子导航
    $(".add_subnav_dig").click(function () {
        var parentid = $(this).attr('attr-parentid');
        var classid = $(this).attr('attr-classid');
        Form_Reset("#edit_nav_frm");
        $(".dig_title").text('创建子导航');
        Data_loader(edit_frm, 'select', 'classid', classid, '');
        Data_loader(edit_frm, 'select', 'parentid', parentid, '');
        Data_loader(edit_frm, 'image', 'picurl', '', '.picurl');
    });

});