/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');

    //切换配置分组分类
    $("#group_select").change(function () {
        var group = $(this).val();
        var data = get_json(Aroot +'sysset/get_config_group', { group: group }, true);
        $("#classidlist").html('');
        data=data.data;
        $.each(data, function (i, n) {
            $("#classidlist").append('<option value="' + n.classid + '">' + n.classname + '</option>');
        });
    });
    //创建一个配置分类
    $("#add_config_class_btn").click(function () {
        if (FormCheck("#add_config_class_frm")) {
            data = Form_Post(
                "#add_config_class_frm",
                Aroot +'sysset/add_config_class',
                "reload"
            );
        }
        return false;
    });
    //创建一个配置
    $("#add_config_btn").click(function () {
        if (FormCheck("#add_config_frm")) {
            data = Form_Post(
                "#add_config_frm",
                Aroot +'sysset/add_config',
                "reload"
            );
        }
        return false;
    });
});