/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    $("select[name=classid]").val($("input[name='cid']").val());

    //产品编辑、加载原数据
    var goods_edit_frm = $("#goods_edit_frm");
    var goods_id = goods_edit_frm.find('input[name=id]');
    if (goods_id.val() >= 1) {
        var goods = get_json(Aroot +'goods/get_goods_info', { id: goods_id.val() }, true);
        goods=goods.data;
        Data_loader(goods_edit_frm, 'select', 'classid', goods.classid, '');
        goods_edit_frm.find('select[name=classid]').change();
        Data_loader(goods_edit_frm, 'input', 'gid', goods.gid, '');
        Data_loader(goods_edit_frm, 'input', 'orderid', goods.orderid, '');
        Data_loader(goods_edit_frm, 'input', 'goodsname', goods.goodsname, '');
        Data_loader(goods_edit_frm, 'checkbox', 'flag', goods.flag, '');
        Data_loader(goods_edit_frm, 'textarea', 'keyword', goods.keyword, '');
        Data_loader(goods_edit_frm, 'textarea', 'description', goods.description, '');
        Data_loader(goods_edit_frm, 'textarea', 'content', goods.content, '');
        Data_loader(goods_edit_frm, 'text', '.salesvolume', '已经销售:' + goods.salesvolume + '件', '');
        Data_loader(goods_edit_frm, 'input', 'marketprice', goods.marketprice, '');
        Data_loader(goods_edit_frm, 'input', 'salesprice', goods.salesprice, '');
        Data_loader(goods_edit_frm, 'input', 'freight', goods.freight, '');
        Data_loader(goods_edit_frm, 'input', 'weight', goods.weight, '');
        Data_loader(goods_edit_frm, 'radio', 'payfreight', goods.payfreight, '');
        Data_loader(goods_edit_frm, 'input', 'housenum', goods.housenum, '');
        Data_loader(goods_edit_frm, 'input', 'integral', goods.integral, '');
        Data_loader(goods_edit_frm, 'image', 'picurl', goods.picurl, '.picurl');
        Data_loader(goods_edit_frm, 'image', 'picarr', goods.picarr, '.picarr');
    }

    //产品标志管理
    $("#show_goodsflag_btn").click(function () {
        var goods_flag_box = $("#goods_flag_box");
        function Get_GoodsFlagList(page) {
            var data = get_json(Aroot +'goods/get_flaglist', { page: page }, true);
            data=data.data;
            goods_flag_box.find("table tbody").html('');
            goods_flag_box.find(".pagination").html('');
            $.each(data, function () {
                goods_flag_box.find("table tbody").append('<tr><td>' + this.id + '</td><td>' + this.flag + '</td><td>' + this.flagname + '</td><td><a href="#" class="badge badge-primary del_btn" attr-id="' + this.id + '"><i class="iconfont icon-times-circle"></i> 删除</a></td></tr>');
            });
            // goods_flag_box.find(".pagination").append(data.pagelist);
        }
        Get_GoodsFlagList(1);
        //标志删除
        goods_flag_box.find("table tr td").delegate('.del_btn', "click", function () {
            var id = $(this).attr('attr-id');
            Confirm('您确认删除此标志?', function () {
                get_json(Aroot +'goods/del_goodsflag', { id: id }, false);
                return true;
            }, function () {
                return true;
            });
            return false;
        });
        //标志列表分页
        goods_flag_box.find(".pagination").delegate('ul li a', "click", function () {
            var page = $(this).attr('attr-page');
            Get_GoodsFlagList(page);
            return false;
        });
    });
    //创建一个新标志
    $("#add_goodsflag_btn").click(function () {
        if (FormCheck("#goods_flag_frm")) {
            data = Form_Post("#goods_flag_frm", Aroot +'goods/add_flag','reload');
        }
        return false;
    });

    $(".reply_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot +'goods/get_goods_comment', { id: id }, true);
        var edit_frm = $("#goods_comment_frm");

        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'posttime', data.posttime, '');
        Data_loader(edit_frm, 'textarea', 'comment', data.comment, '');
        Data_loader(edit_frm, 'textarea', 'reply', data.reply, '');

    });
    $("#add_goodscomment_btn").click(function () {
        if (FormCheck("#goods_comment_frm")) {
            data = Form_Post("#goods_comment_frm", Aroot +'goods/save_goodscomment', 'reload');
        }
        return false;
    });    
});