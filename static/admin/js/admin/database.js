/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    //创建一个新备份
	$("#new_backup_btn").click(function(){
        Confirm('您确认创建一个新备份?', function () {
            get_json(Aroot +'database/new_backup', {},false);
        }, function () {
            return true;
        });
    });    
    //删除备份
    $(".deletefile_btn").click(function(){
        var filename=$(this).attr('attr-filename');
        Confirm('您确认删除此备份?', function () {
            get_json(Aroot +'database/deletefile', { filename:filename },false);
        }, function () {
            return true;
        });
    });
    //恢复备份
    $(".recover_btn").click(function(){
        var filename=$(this).attr('attr-filename');
        Confirm('您确认恢复此备份?', function () {
            get_json(Aroot +'database/recoverfile', { filename:filename },false);
        }, function () {
            return true;
        }); 
    });
    //设置为模板数据库
    $(".set_temp_btn").click(function(){
        var filename=$(this).attr('attr-filename');
        Confirm('您确认设置此数据库为模板?', function () {
            get_json(Aroot +'database/set_temp', { filename:filename },false);
        }, function () {
            return true;
        });
    });
    //清空所有业务数据
    $("#clear_database_btn").click(function(){
        Confirm('您确认要清除所有业务数据?', function () {
            get_json(Aroot +'database/clear_database', { },false);
        }, function () {
            return true;
        });
    });
});