/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
$(function(){
    var Aroot = $("body").attr('bind-root');
    var Method = $("body").attr('bind-Method');
    var edit_frm = $("#edit_model_frm");

    $(".edit_btn").click(function () {
        var id = $(this).attr('attr-id');
        var data = get_json(Aroot + 'model/get_model_info', { id: id }, true);
        data=data.data;
        $(".dig_title").text('修改模型');
        Data_loader(edit_frm, 'input', 'id', data.id, '');
        Data_loader(edit_frm, 'input', 'model_name', data.model_name, '');
        Data_loader(edit_frm, 'input', 'model_table_comment', data.model_table_comment, '');
        Data_loader(edit_frm, 'input', 'model_table', data.model_table, '');
        Data_loader(edit_frm, 'textarea', 'description', data.description, '');
        Data_loader(edit_frm, 'radio', 'status', data.status, '');
        Data_loader(edit_frm, 'radio', 'model_engine', data.model_engine, '');
    });
    $("#add_mobel_btn").click(function () {
        $(".dig_title").text('创建新模型');
        Form_Reset("#edit_model_frm");
    });
    //模型数据表生成与重构
    var model_table_data_frm = $("#model_table_data_frm");
    $(".create_table_btn").click(function () {
        var id = $(this).attr('attr-id');
        model_table_data_frm.find('input[name=mid]').val(id);
    });
    model_table_data_frm.find('a.btn').click(function () {
        var act = $(this).attr('attr-act');
        model_table_data_frm.find('input[name=act]').val(act);
        data = Form_Post(
            "#model_table_data_frm",
            Aroot + 'model/table_rebuild',
            'reload'
        );
        return false;
    });
    
    
});