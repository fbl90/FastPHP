(function($){

    var defaults = {
        url:'',
        basePid:1,
        defaultPath:[0,0,0],
        selected:0,
        maxWidth:'120px'
    }
    $.fn.extend({
        selectcity:function(options){
            var options=$.extend(defaults,options);
            if(typeof options.defaultPath ==='string'){
                options.defaultPath = options.defaultPath.split(',');
            }
            var ipt = $(this);
            var self = this;
            var fbox = $(ipt).parent();
            fbox.find("select").remove();
            var basePid = options.basePid;
            var pbox = $("<select class='form-control' style='max-width:"+options.maxWidth+";' data-default='"+options.defaultPath[0]+"'>");
            var cbox = $("<select class='form-control' style='max-width:"+options.maxWidth+";' data-default='"+options.defaultPath[1]+"'>");
            var abox = $("<select class='form-control' style='max-width:"+options.maxWidth+";' data-default='"+options.defaultPath[2]+"'>");
            self.pbox = pbox.clone();
            self.cbox = null;
            self.abox = null;
            fbox.append(this.pbox);
            ipt.printAreaOption(options.url,this.pbox,basePid,options.defaultPath[0]);
            this.setVal = function(val){
                ipt.val(val);
            }
            //监听省变化
            self.pbox.change(function(){
                var val = parseInt($(this).val());
                if(null !== self.cbox){
                    self.cbox.remove();
                }
                if(null !== self.abox){
                    self.abox.remove();
                }
                if(val>0){
                    self.cbox = cbox.clone();
                    fbox.append(self.cbox);
                    ipt.printAreaOption(options.url,self.cbox,val,options.defaultPath[1]);
                    self.setVal(val);
                    //监听市变化
                    self.cbox.change(function(){
                        var val = parseInt($(this).val());
                        if(null !== self.abox){
                            self.abox.remove();
                        }
                        if(val>0){
                            self.abox = abox.clone();
                            fbox.append(self.abox);
                            ipt.printAreaOption(options.url,self.abox,val,options.defaultPath[2]);
                            self.setVal(val);
                            //监听区县变化
                            self.abox.change(function(){
                                var val = parseInt($(this).val());
                                if(val>0){
                                    self.setVal(val);
                                }else{
                                    self.setVal($(self.cbox).val());
                                }
                            })
                            if(options.defaultPath[2]!=0){
                                self.abox.change();
                            }
                        }else{
                            self.setVal($(self.pbox).val())
                        }
                    })
                    if(options.defaultPath[1]!=0){
                        self.cbox.change();
                    }
                }
            })
            if(parseInt(options.defaultPath[0])!=0){
                self.pbox.change();
            }
        },
        printAreaOption:function(url,selectbox,pid,selected){
            selectbox.html('');
            if(typeof selected ==='undefined'){
                selected = 0;
            }
            data = get_json(url,{pid:pid},true);
            var myHtml = "";
            var selectedstr = "";
            if(selected==0){
                selectedstr="selected";
            }
            myHtml+="<option value='0' "+selectedstr+">请选择</option>";
            for(var i=0;i<data.length;i++){
                var aselectstr = "";
                if(data[i].id == selected){
                    aselectstr = "selected";
                }
                myHtml+="<option value='"+data[i].id+"' "+aselectstr+">"+data[i].name+"</option>";
            }
            selectbox.html(myHtml);
        }
    })
})(jQuery)