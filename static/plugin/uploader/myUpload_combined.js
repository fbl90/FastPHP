/*
 	复合式上传组支持单图，多图片，带标题，带链接输入框
 	
 	复合式数据结构
 	Array(
 		1=>array(image='path',title='text',linkurl='url'),
 		2=>array(image='path',title='text',linkurl='url'),
 	)
 	
 */
(function($){
	
	function ClearArray(arr){
		arr2=new Array();
		$.each(arr, function(i,n) {
			if($.trim(n)!=""){
				arr2[i]=n;
			}
		});
		return arr2;
	}
	
	var defaults_com={
		fileInputName:'file',
		pictureInputName:'picture',
		type:'image',
		initValue:'',
		isMulti:false,
		uploadPath:null,
		callback:null
	};

	var imageEdit_com = null;
	var type=null;
	
	$.fn.extend({
		upload_combined:function(options){
			var thisObj = $(this);
			var opts=$.extend(defaults_com,options);
			var fileInputName = opts.fileInputName;
			var pictureInputName = opts.pictureInputName;
			var type = opts.type;
			var initValue = opts.initValue;
			var isMulti = opts.isMulti;
			var uploadPath = opts.uploadPath;
			var callback = opts.callback;
			
			if(type=='image' || type=='face'){
				var uploadContainer = $('<div class="myupload_image_container_com"></div>');	
			}
			
			thisObj.html('');
			
			thisObj.append(uploadContainer);
			
			if(type=='image' || type=='face'){
				var uploadButton = $('<div class="table_uploader" role="myupload-file-input-btn"><a href="javascript:;" class="image_upload_btn">点击上传</a></div>');
			}

			if(initValue.length >= 1 && !isMulti) {//如果有初始值并且是单图模式则隐藏上传按钮
				uploadButton.hide();
			}
			
			uploadContainer.append(uploadButton);
			
			if(isMulti){
				var uploadForm = $('<form class="uploader_frm" action="#" enctype="multipart/form-data" method="post" role="myupload-file-form"><input type="file" name="'+fileInputName+'[]" role="myupload-file-input" accept="*" multiple="true" style="display:none;"/><input type="hidden" name="type" value="'+type+'" /></form>');
			}else{
				var uploadForm = $('<form class="uploader_frm" action="#" enctype="multipart/form-data" method="post" role="myupload-file-form"><input type="file" name="'+fileInputName+'" role="myupload-file-input" accept="*" style="display:none;"/><input type="hidden" name="type" value="'+type+'" /></form>');
			}
			
			uploadContainer.append(uploadForm);
		
			if(initValue.length >= 1) {
				
				$.each(initValue, function(i,n) {
					thisObj.imagePreview_com(fileInputName,n,isMulti,uploadButton,uploadForm,uploadContainer,type);
				});
				
//				for(var i=0;i<initValue.length;i++) {
//					
//				}
			}

			thisObj.find('div[role="myupload-file-input-btn"]').click(function(){
				imageEdit_com = null;
				uploadForm.find('input[type="file"]').val('').trigger("click");
			});

			uploadForm.find('input[type="file"]').change(function(){
				uploadForm.ajaxSubmit({
						url:uploadPath,
						dataType: "json",
						success : function(data,status){
							if(data.status==0){
								
								$.each(data.data, function(i,n) {
									if(imageEdit_com) {
										imageEdit_com.find('input[role="myupload-picture-input"]').val(n);
										if(type=='image' || type=='face'){
											imageEdit_com.find('img[role="myupload-picture-show"]').attr("src",n);
											imageEdit_com.find('input[role="myupload-title-show"]').val('');
											imageEdit_com.find('input[role="myupload-url-show"]').val('');
										}											
									}else{
										thisObj.imagePreview_com(fileInputName,n,isMulti,uploadButton,uploadForm,uploadContainer,type);
									}
								});
								
	
							}else{
								layer.msg(data.error);
							}
						}
				})
			})
		},

		imagePreview_com : function(p,d,i,b,f,c,type) {
			if(i){
				mu='[]';
			}else{
				mu='';
			}
			if(type=='image' || type=='face'){
				var imagePreview_com = $('<div class="myupload_box">' + 
	            			'<a href="#" class="preview_thumb"><img src="#" title="点击可以放大" role="myupload-picture-show"/></a>' +
	            			'<div class="combined_box"><input type="text" placeholder="请输入标题" name="' + p +'_title'+ mu+ '" class="form-control" role="myupload-title-show">' +
	            			'<input type="text" name="' + p +'_url'+ mu+ '" placeholder="请输入地址" class="form-control" role="myupload-url-show"></div>'+
	            			'<div class="ctrl_box">' +
	            			'<a href="javascript:;" class="btn btn-danger cancel">删除</a></div>' +
	            			'<input type="text" name="' + p + mu+ '" role="myupload-picture-input" value="" style="display:none;"/></span>');				
			}
			
			imagePreview_com.find('.upload').click(function() {
				imageEdit_com = $(this).parent().parent().parent();
				f.find('input[type="file"]').val('').trigger("click");
			});
			
			imagePreview_com.find('.cancel').click(function() {
				$(this).parent().parent().remove();
				if(!i) {
					b.show();
				}
			});
			if($.type(d)=='object'){
				
				imagePreview_com.find('input[role="myupload-picture-input"]').val(d.picurl);
				if(type=='image' || type=='face'){
					imagePreview_com.find('img[role="myupload-picture-show"]').attr("src",d.picurl);
					imagePreview_com.find('input[role="myupload-title-show"]').val(d.title);
					imagePreview_com.find('input[role="myupload-url-show"]').val(d.url);
				}
			}else{
				imagePreview_com.find('input[role="myupload-picture-input"]').val(d);
				
				if(type=='image' || type=='face'){
					imagePreview_com.find('img[role="myupload-picture-show"]').attr("src",d);
				}
			}
			if(!i) {
				b.hide();
			}
			c.prepend(imagePreview_com);
		}
	})
})(jQuery);