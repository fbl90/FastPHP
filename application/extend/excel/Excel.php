<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
require_once(__DIR__.'/PHPExcel.php');
require_once(__DIR__.'/PHPExcel/IOFactory.php');
require_once(__DIR__.'/PHPExcel/Writer/Excel5.php');		

/*
    二维数组保存到excel表格中
*/ 
function ArrayToXls($data,$save_path)
{
	$Excels = new PHPExcel();
	foreach($data as $key=>$row){
		$i=0;
		foreach($row as $ckey=>$col){
			$Excels->getActiveSheet()->setCellValue(IntToChr($i).$key,' '.$col,PHPExcel_Cell_DataType::TYPE_STRING);
			$i++;
		}
	}
    $xlsWriter = new PHPExcel_Writer_Excel5($Excels);
    $xlsWriter->save($save_path);
}
/*
    Excel表格转换成数组
    2021-07-30:解决列超过26列
*/ 
function XlsToArray($xlsfile,$sheetid=0,$start=2)
{
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
    $PHPExcel = PHPExcel_IOFactory::load($xlsfile);
    $sheet = $PHPExcel->getSheet($sheetid);//选择工作簿
     /**取得最大的列号*/ 
    $allColumn = $sheet->getHighestColumn(); 
     /**取得一共有多少行*/ 
    $allRow = $sheet->getHighestRow(); 
    ++$allColumn;   
    for($Row = $start;$Row <= $allRow;$Row++){
        /**从第A列开始输出*/
        for($Column= 'A';$Column != $allColumn; $Column++){ 
            $val = $sheet->getCellByColumnAndRow(ord($Column) - 65,$Row)->getValue();   //获得详细内容
            $w   = $sheet->getColumnDimension($Column)->getWidth();                     //获得列的宽度
            $list[$Row][$Column]=$val;
        }
    }
    return $list;
}
/**
 * 数字转字母 （类似于Excel列标）
 * @param Int $index 索引值
 * @param Int $start 字母起始值
 * @return String 返回字母
 * @author Anyon Zou <Anyon@139.com>
 * @date 2013-08-15 20:18
 */
function  IntToChr($index, $start = 65) {
    $str = '';
    if (floor($index / 26) > 0) {
        $str .= IntToChr(floor($index / 26)-1);
    }
    return $str . chr($index % 26 + $start);
}

