<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
require_once(__DIR__.'/qrlib.php');
/*
    //调用示例
    // include_once(APP_PATH.'/extend/phpqrcode/qrcode.php');
    // $qrcode=CreateQRCode('http://www.baidu.com/','400','D:\www\WhatPHPv2019\uploads\images\20190211\1549868835zDfBVGhpEC9893.jpg');
*/
//创建一个二维码  $data:生成二维码包含的数据 $width:生成二维码的宽度  $logofile:生成二维码是否生成加入LOGO $savefile:保存到文件
function CreateQRCode($data,$width,$logofile="",$issave=true,$savefile="")
{
    //二维码图片保存路径(若不生成文件则设置为false)
    $filename =  "qrcode_" . time() . ".png";
    //二维码容错率，默认L
    $level = "L";
    //二维码图片每个黑点的像素，默认4
    $size = get_qrcode_size($width);
    //二维码边框的间距，默认2
    $padding = 2;
    //保存二维码图片并显示出来，$filename必须传递文件路径
    $saveandprint = false;
    if($issave==true){
        if($savefile==""){
            $DIR1="/uploads/qrcode/".MyDate('Ymd',time())."/";
            $FILENAME="QR_".time().GetRandStr(20).".jpg";
            $path=ROOT_PATH.$DIR1;
            if(!file_exists($path)){
                mkdir($path);
            }
            $savefile=$path.$FILENAME;
            $return_path=$DIR1.$FILENAME;
        }else{
            $return_path="";
        }
        //生成二维码图片
        \QRcode::png($data,$savefile,$level,$size,$padding,$saveandprint);
        if($logofile!=""){
            QRCodeLogo($savefile,$logofile);
        }
        return $return_path;
    }else{
        //生成二维码图片
        \QRcode::png($data,false,$level,$size,$padding,$saveandprint);
    }
}
//根据宽度计算二维码大小值
function get_qrcode_size($w)
{
    $size=floor($w/29*100)/100 + 0.01;
    return $size;
}
//给二维码中加入LOGO,并覆盖到二维码文件中
function QRCodeLogo($Qrfile,$logofile)
{
    $QR = imagecreatefromstring(file_get_contents($Qrfile));        //目标图象连接资源。
    $logo = imagecreatefromstring(file_get_contents($logofile));    //源图象连接资源。
    
    $QR_width = imagesx($QR);            //二维码图片宽度
    $QR_height = imagesy($QR);            //二维码图片高度
    $logo_width = imagesx($logo);        //logo图片宽度
    $logo_height = imagesy($logo);        //logo图片高度
    $logo_qr_width = $QR_width / 4;       //组合之后logo的宽度(占二维码的1/5)
    $scale = $logo_width/$logo_qr_width;       //logo的宽度缩放比(本身宽度/组合后的宽度)
    $logo_qr_height = $logo_height/$scale;  //组合之后logo的高度
    $from_width = ($QR_width - $logo_qr_width) / 2;   //组合之后logo左上角所在坐标点
    
    //重新组合图片并调整大小
    //imagecopyresampled() 将一幅图像(源图象)中的一块正方形区域拷贝到另一个图像中
    imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,$logo_qr_height, $logo_width, $logo_height);
    //输出图片
    imagejpeg($QR,$Qrfile);
    imagedestroy($QR);
    imagedestroy($logo);
}
?>