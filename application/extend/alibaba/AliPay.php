<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\extend\alibaba;

/*
	支付宝支付接口(支付宝支付) 新版接口-PC、手机网页支付
*/
use \app\base\Common;
class AliPay extends Common
{
	/*
		支付宝支付配置
    */
	//支付宝APPID
    private $App_ID="";
    //支付完成后同步返回地址
    private $Return_URL="";
    //支付完成后异步回调地址
    private $Notify_url="";
    //支付宝私钥
    private $Alipay_PKey ="";
	
	public function _initialize()
	{
    	//执行父类的构造方法
    	parent::_initialize();
    	//读取支付宝配置
	 	$this->App_ID		    =$this->Config['alipay_appid'];
        $this->Alipay_PKey		="-----BEGIN RSA PRIVATE KEY-----\n".$this->Config['alipay_privatekey']."\n-----END RSA PRIVATE KEY-----";
	 	$this->Return_URL		=$this->Config['weburl']."/Paynotify/Alipay_Callback";
    }
    
    public function Pay($type,$out_trade_no,$amount,$subject="商品支付")
    {
        //开始组建参数
        $data=array(
            'app_id'=>$this->App_ID,
            'method'=>'',
            'format'=>'JSON',
            'return_url'=>$this->Return_URL,
            'charset'=>'utf-8',
            'sign_type'=>'RSA2',
            'timestamp'=>MyDate("Y-m-d H:i:s",time()),
            'version'=>'1.0',
            'notify_url'=>$this->Notify_url,
            'biz_content'=>'',
        );

        if($type=='mobile'){
            $data['method']='alipay.trade.wap.pay';
        }else{
            $data['method']='alipay.trade.page.pay';
        }
        $Order_Data=array(
            'out_trade_no'=>$out_trade_no,
            'product_code'=>'FAST_INSTANT_TRADE_PAY',
            'total_amount'=>$amount,
            'subject'=>$subject
        );
        $data['biz_content']=json_encode($Order_Data);
        //开始签名
        ksort($data);
        $string=$this->ToUrlParams($data);
        $sign=openssl_sign($string , $sign, $this->Alipay_PKey,OPENSSL_ALGO_SHA256 )? base64_encode($sign) : null;
        $data['sign']=$sign;
        $this->CreatePost_From($data);
        // return $data;
    }

    public function CreatePost_From($alipay_data){
		
        $gateway=$this->Config['alipay_gateway'];
			
        $html='<form action="'.$gateway.'" method="POST" id="alipay_frm" name="alipay_frm">
            <input type="hidden" value="'.$alipay_data['app_id'].'" name="app_id">
            <input type="hidden" value=\''.$alipay_data['biz_content'].'\' name="biz_content">
            <input type="hidden" value="'.$alipay_data['charset'].'" name="charset">
            <input type="hidden" value="'.$alipay_data['format'].'" name="format">
            <input type="hidden" value="'.$alipay_data['method'].'" name="method">
            <input type="hidden" value="'.$alipay_data['notify_url'].'" name="notify_url">
            <input type="hidden" value="'.$alipay_data['return_url'].'" name="return_url">
            <input type="hidden" value="'.$alipay_data['sign_type'].'" name="sign_type">
            <input type="hidden" value="'.$alipay_data['timestamp'].'" name="timestamp">
            <input type="hidden" value="'.$alipay_data['version'].'" name="version">
            <input type="hidden" value="'.$alipay_data['sign'].'" name="sign">
        </form>
        <script>document.forms[\'alipay_frm\'].submit();</script>';
        echo $html;
    }
	/**
	 * 格式化参数格式化成url参数
	 */
	public function ToUrlParams($arr)
	{
		$buff = "";
		foreach ($arr as $k => $v)
		{
			if($k != "sign" && $v != "" && !is_array($v)){
				$buff .= $k . "=" . $v . "&";
			}
		}
		$buff = trim($buff, "&");
		return $buff;
	}
}