<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\extend\phpmailer;
/*
	PHPMailer插件
*/
use \app\base\Common;

class Phpmailer extends Common
{

	protected function _initialize()
	{
    	//执行父类的构造方法
    	parent::_initialize();
	}
	/*	
		//控制器中调用示例
		$PHPMailer=new \app\extend\phpmailer\Phpmailer;
		$PHPMailer->SendMail('771919173@qq.com','测试邮件');
	*/
	
	public function SendMail($To,$Body)
	{
		include_once(APP_PATH."/extend/phpmailer/class.phpmailer.php");
		$smtp_server=$this->Config['smtp_server'];
		$smtp_account=$this->Config['smtp_account'];
		$smtp_password=$this->Config['smtp_password'];
		$email_subject=$this->Config['email_subject'];

		$mail= new \PHPMailer();
		$mail->IsSMTP();
		$mail->Host = $smtp_server;//邮件服务器
		$mail->SMTPDebug = 0;//是否启用调试模式
		$mail->SMTPAuth = true;//使用SMPT验证
		$mail->SMTPSecure = 'ssl';//使用SMPT验证
		$mail->Port = 465;//使用SMPT验证
		$mail->Username = $smtp_account;//SMTP验证的用户名称
		$mail->Password =$smtp_password;//SMTP验证的秘密
		$mail->CharSet = "utf-8";//设置编码格式
		$mail->Subject = $email_subject;//设置主题
		$mail->SetFrom($smtp_account,$email_subject);//设置发送者
		$mail->MsgHTML($Body);//采用html格式发送邮件
		$mail->AddAddress($To,$Body);//接受者邮件名称
		if($mail->Send()){
			return true;
		}else{
			return false;
		}
	}
	
}
?>