<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\extend\baidu;
/*
	百度地图接口插件
*/
use \app\base\Common;

class Map extends Common
{
	private $AK="";
	protected function _initialize()
	{
    	//执行父类的构造方法
    	parent::_initialize();
	}
	//wgs84转百度坐标
	public function wgs84_to_baidu($location)
	{
		$url='https://api.map.baidu.com/geoconv/v1/?coords='.$location['lng'].','.$location['lat'].'&from=1&to=5&ak='.$this->Config['baidumapserver_ak'];
		$result=json_decode(file_get_contents($url),true)['result'][0];
		
		if(isset($result['x'])){
			$loc['lng']=$result['x'];
			$loc['lat']=$result['y'];
			return $loc;	
		}else{
			return false;
		}
	}
	//地址转经纬度
	public function address_to_location($Address)
	{
      	$Address=str_replace(' ','',$Address);
		$url='https://api.map.baidu.com/geocoder/v2/?address='.$Address.'&output=json&ak='.$this->Config['baidumapserver_ak'];
		$result=json_decode(file_get_contents($url),true);
		if(isset($result['result']['location'])){
			return $result['result']['location'];
		}else{
			return false;
		}
	}
}
?>