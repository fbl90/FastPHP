<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
require_once(__DIR__.'/vendor/autoload.php');
use QL\QueryList;


//外部调用 include_once(APP_PATH.DS.'extend'.DS.'querylist'.DS.'querylist.php');
function get_all_img($url)
{
    //采集某页面所有的图片
    $data = QueryList::get($url)->find("li:col-md-3")->html();
    //打印结果
    print_r($data);
}