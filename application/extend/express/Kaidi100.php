<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\extend\express;

/*
	Kaidi100接口插件
	调用：
	$Kaidi100=new \app\extend\express\Kaidi100;
	$result=$Kaidi100->query('huitongkuaidi','71636853792559');	
*/
use \app\base\Common;

class Kaidi100 extends Common
{
	private $URL="https://poll.kuaidi100.com/poll/query.do";
	private $Key="";
	private $Customer="";
	
	protected function _initialize()
	{
    	//执行父类的构造方法
    	parent::_initialize();
		$this->Key=$this->Config['kaidi100_authkey'];
		$this->Customer=$this->Config['kaidi100_customer'];
	}
	
	public function query($com,$num){
		$cache= cache($com.'_'.$num);
		if($cache!=""){
			//直接读取缓存
			$data = json_decode($cache,true);
			return $data;
		}
		//参数设置
		$key = $this->Key;				//客户授权key
		$customer = $this->Customer;	//查询公司编号
		$param = array (
			'com' => $com,				//快递公司编码
			'num' => $num,				//快递单号
			'phone' => '',				//手机号
			'from' => '',				//出发地城市
			'to' => '',					//目的地城市
			'resultv2' => '1'			//开启行政区域解析
		);
		
		//请求参数
		$post_data = array();
		$post_data["customer"] = $customer;
		$post_data["param"] = json_encode($param);
		$sign = md5($post_data["param"].$key.$post_data["customer"]);
		$post_data["sign"] = strtoupper($sign);
		
		$url = 'http://poll.kuaidi100.com/poll/query.do';	//实时查询请求地址
		
		$params = "";
		foreach ($post_data as $k=>$v) {
			$params .= "$k=".urlencode($v)."&";		//默认UTF-8编码格式
		}
		$post_data = substr($params, 0, -1);
		
		//发送post请求
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$data = str_replace("\"", '"', $result );
		//设置查询缓存
		cache($com.'_'.$num,$data,86400);
		$data = json_decode($data,true);
		return $data;
	}
	//获得快递编码
	public function get_express_code()
	{
		$data=array(
			'yunda'=>'韵达快递',
			'yuantong'=>'圆通速递',
			'shentong'=>'申通快递',
			'youzhengguonei'=>'邮政快递包裹',
			'shunfeng'=>'顺丰速运',
			'jd'=>'京东物流',
			'zhongtong'=>'中通快递',
			'huitongkuaidi'=>'百世快递',
			'tiantian'=>'天天快递',
			'ems'=>'EMS',
			'debangwuliu'=>'德邦',
			'dhlen'=>'DHL-全球件',
			'auspost'=>'澳大利亚(Australia Post)',
			'youshuwuliu'=>'优速物流',
			'zhaijisong'=>'宅急送',
			'dhl'=>'DHL-中国件',
			'baishiwuliu'=>'百世快运',
			'debangkuaidi'=>'德邦快递',
			'pjbest'=>'品骏快递',
			'zhimakaimen'=>'芝麻开门',
			'kuayue'=>'跨越速运',
			'suning'=>'苏宁物流',
			'zhongtongkuaiyun'=>'中通快运',
			'suer'=>'速尔快递',
			'yundakuaiyun'=>'韵达快运',
			'rrs'=>'日日顺物流',
			'fedexus'=>'FedEx-美国件',
			'quanfengkuaidi'=>'全峰快递'
		);
		return $data;
	}
}
?>