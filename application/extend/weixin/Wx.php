<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\extend\weixin;
/*
	微信公众号基础接口
	---JSSDK
	---二维码
	---微信菜单
*/
use \app\base\Common;
use \app\extend\weixin\Wxcommon;

class Wx extends Common
{
	private $Appid;
	private $Appsecret;
	private $access_token;
	
	protected function _initialize()
	{
		//执行父类的构造方法
		parent::_initialize();
			
		$this->Appid		=$this->Config['wx_appid'];
		$this->Appsecret	=$this->Config['wx_appsecret'];

		$this->access_token=cache('client_credential_token');
		if($this->access_token==""){
			$this->access_token=file_get_contents('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$this->Appid.'&secret='.$this->Appsecret);
			cache('client_credential_token',$this->access_token,7200);
		}
		$this->access_token=json_decode($this->access_token,true);
	}
	//获得微信用户基本信息
	public function GetUserinfo($openid)
	{
		$result=file_get_contents('https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$this->access_token['access_token'].'&openid='.$openid.'&lang=zh_CN');
		return json_decode($result,true);
	}
	//创建唯一带参数的公众号二维码
  	public function CreateQrcode($uid)
    {
      	$rwm_file=RUNTIME_PATH.'rwm'.DS.base64_encode('generalize_'.$uid).'.jpg';
        $json='{"action_name": "QR_LIMIT_SCENE", "action_info": {"scene": {"scene_id": '.$uid.'}}}';
        $result=Wxcommon::postXmlCurl($json,'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.$this->access_token['access_token']);
      	WLog('wx_create_qrcode',$result);
        $result=json_decode($result,true);
        $result=file_get_contents('https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$result['ticket']);
        file_put_contents($rwm_file,$result);
      	return true;
	}
	//获得JSSDK票据数据
  	public function JsapiTicket()
    {	
		$result=file_get_contents('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='.$this->access_token.'&type=jsapi');
		$result=json_decode($result,true);
		return $result['ticket'];
	}
	//获得分享页面签名参数
  	public function GetSign($url)
    {
      	$data['noncestr']=GetRandStr(32);
      	$data['jsapi_ticket']=$this->JsapiTicket();
      	$data['timestamp']=time();
      	$data['url']=$url;
      	$data['sign']=Wxcommon::MakeSign_SHA1($data);
		$data['appid']=$this->Config['wx_appid'];
      	return $data;
    }
	//创建微信菜单
	/*
		$jsonmenu = '{
				"button":[
				{
						"type":"view",
						"name":"预约服务",
						"url":"http://sjjd.idaorui.com/"
					}]
			}';
	*/
	public function WxMenu($jsonmenu)
	{
		$URL="https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$this->access_token;
		return Wxcommon::postXmlCurl($jsonmenu,$URL);			
	}
	//微信服务器事件处理
	public function WxEvent($access_check=0)
	{
		$postStr = file_get_contents("php://input");
		//接入验证
		if($access_check==1){
			echo input('echostr');
			exit();
		}
		//回调数据写入日志
		WLog('wxevent_debug',$postStr);
		$result=XmlToArray2($postStr);
		//接收事件
		if($result['MsgType']=='event'){
			if($result['Event']=='subscribe'){
				//关注事件
				
			}else if($result['Event']=='unsubscribe'){
				//取关事件
				
			}else if($result['Event']=='CLICK'){
				//点击事件
				
			}else if($result['Event']=='SCAN'){
				//扫码事件
				
			}
		}
		exit();
	}
}
?>