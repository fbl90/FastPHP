<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
// 应用公共文件
//数据数组式状态返回
function ResultReturn($Code,$Msg,$Data=null){
	//状态定义	200：表示正常执行，正常获取数据201：表示正常执行，没有获取到数据
	$Arr['Code']=$Code;
	$Arr['Msg']=$Msg;
	$Arr['Data']=$Data;
	return $Arr;
}
//新版
function Result($status,$msg,$data=null){
	return ['status'=>$status,'msg'=>$msg,'data'=>$data];
}
//Json数据包返回
function ShowJson($status,$msg,$data=''){
	$result['status']=$status;
	$result['msg']=$msg;
	$result['data']=$data;
	echo json_encode($result);
	exit();
}
//通用密码加密
function Encrypt($safecode,$password){
	return  base64_encode(md5(md5($safecode).md5($password)));
}
//通用密码校验
function CheckEncrypt($safecode,$password,$ciphertext){
	$check_ciphertext=base64_encode(md5(md5($safecode).md5($password)));
	if($check_ciphertext==$ciphertext){
		return true;
	}else{
		return false;
	}
}
//隐藏手机号部分数字
function HiddenMobile($mobile){
	return substr_replace($mobile,'*****',3,4);
}
//隐藏身份证部分数字
function HiddenIdcard($Idcard){
	return substr_replace($Idcard,'************',4,12);
}
//在字符串前生成指定数量的字符
function str_prefix($str, $n=1, $char=" "){
	for ($x=0;$x<$n;$x++){$str = $char.$str;}
	return $str;
}
//在字符串后生成指定数量的字符 
function str_suffix($str, $n=1, $char=" "){
	for ($x=0;$x<$n;$x++){$str = $str.$char;}
	return $str;
}
//清除HTML
function ClearHtml($str) {
	$str = strip_tags($str);
	$str = trim($str);//首先去掉头尾空格
	$str = preg_replace('/\s(?=\s)/', '', $str);//接着去掉两个空格以上的
	$str = preg_replace('/[\n\r\t]/', ' ', $str);//最后将非空格替换为一个空格
	return $str;
}
//获取指定长度随机字符串
function GetRandStr($length = 6) {
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	$random_str = '';
	for ($i = 0; $i < $length; $i++) {
		$random_str .= $chars[mt_rand(0, strlen($chars) - 1)];
	}
	return $random_str;
}
//函数说明：格式输出变量
function S($val) {
	echo "<pre>";
	if (is_array($val)) {
		print_r($val);
	}else if(is_object($val)){
		var_dump($val);
	} else {
		echo $val;
	}
	echo "</pre>";
}
//检测字符串是不是UTF-8的字符集
function IsUTF8($string) {
	return preg_match('%^(?:
     [\x09\x0A\x0D\x20-\x7E]            # ASCII
       | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
       |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
       | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
       |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
       |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
       | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
       |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
       )*$%xs', $string);
}
//阿拉伯数字转大写汉字
function NumToChina($num,$type='simplified', $mode = true) {
	if($type=='complex'){
		$char = array('零','壹','贰','叁','肆','伍','陆','柒','捌','玖');
		$dw = array('','拾','佰','仟','','萬','億','兆');
		$dec = '點';
	}else{
		$char = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
		$dw = array('', '十', '百', '千', '', '万', '亿', '兆');
		$dec = '点';
	}
	$retval = '';
	if ($mode) {
		preg_match_all('/^0*(\d*)\.?(\d*)/', $num, $ar);
	} else {
		preg_match_all('/(\d*)\.?(\d*)/', $num, $ar);
	}
	if ($ar[2][0] != '') {
		$retval = $dec . NumToChina($ar[2][0], false);//如果有小数，先递归处理小数
	}
	if ($ar[1][0] != '') {
		$str = strrev($ar[1][0]);
		for ($i = 0; $i < strlen($str); $i++) {
			$out[$i] = $char[$str[$i]];
			if ($mode) {
				$out[$i] .= $str[$i] != '0' ? $dw[$i % 4] : '';
				if (@$str[$i] + @$str[$i - 1] == 0) {
					$out[$i] = '';
				}
				if ($i % 4 == 0) {
					$out[$i] .= $dw[4 + floor($i / 4)];
				}
			}
		}
		$retval = join('', array_reverse($out)) . $retval;
	}
	return $retval;
}
//快速写日志
function WLog($LogFileName, $info = "") {
	$TimeName = MyDate("Y-m-d", time());
	$LogFilePath = RUNTIME_PATH . 'developer_log' .DS . $LogFileName .'_'. $TimeName . ".log";
	$Countent = "[" . MyDate('Y-m-d H:i:s', time()) . "]-INFO:" . $info . "\r\n";
	return file_put_contents($LogFilePath, $Countent, FILE_APPEND);
}
//删除数组中的空元素不包括0
function ClearNullArray($arr) {
	$l = array();
	foreach ($arr as $k => $v) {
		if ($v !== "") {
			if (is_array($v)) {
				$l[$k] = ClearNullArray($v);
			} else {
				$l[$k] = $v;
			}
		}
	}
	return $l;
}
//获取IP
function GetIP() {
	static $ip = NULL;
	if ($ip !== NULL)
		return $ip;
	
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
		$pos = array_search('unknown', $arr);
		if (false !== $pos)
			unset($arr[$pos]);
		$ip = trim($arr[0]);
	} else if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} else if (isset($_SERVER['REMOTE_ADDR'])) {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	//IP地址合法验证
	$ip = (false !== ip2long($ip)) ? $ip : '0.0.0.0';
	return $ip;
}
//转换数据直观大小
function GetRealSize($size){
    $kb = 1024;          // Kilobyte
    $mb = 1024 * $kb;    // Megabyte
    $gb = 1024 * $mb;    // Gigabyte
    $tb = 1024 * $gb;    // Terabyte

    if($size < $kb)
        return $size.'B';

    else if($size < $mb)
        return round($size/$kb,2).'KB';

    else if($size < $gb)
        return round($size/$mb,2).'MB';

    else if($size < $tb)
        return round($size/$gb,2).'GB';

    else
        return round($size/$tb,2).'TB';
}
//上面函数的反向操作
function GetSizeToBit($filesize){
    $filesize=strtolower($filesize);
    $int_val=intval($filesize);
    if(strpos($filesize,'kb')!==false){
        return $int_val*1024;
    }elseif(strpos($filesize,'mb')!==false){
        return $int_val*1024*1024;
    }elseif(strpos($filesize,'gb')!==false){
        return $int_val*1024*1024*1024;
    }elseif(strpos($filesize,'tb')!==false){
        return $int_val*1024*1024*1024*1024;
    }elseif(strpos($filesize,'k')!==false){
         return $int_val*1024;
    }elseif(strpos($filesize,'m')!==false){
         return $int_val*1024*1024;   
    }elseif(strpos($filesize,'g')!==false){
         return $int_val*1024*1024*1024; 
    }elseif(strpos($filesize,'t')!==false){
         return $int_val*1024*1024*1024*1024;          
    }else{
        return $int_val;
    }
}
//获取文件夹大小
function GetDirSize($dir) {
	$handle = opendir($dir);
	$fsize = '';
	while (($fname = readdir($handle)) !== false) {
		if ($fname != '.' && $fname != '..') {
			if (is_dir("$dir/$fname"))
				$fsize += GetDirSize("$dir/$fname");
			else
				$fsize += filesize("$dir/$fname");
		}
	}
	closedir($handle);
	if (empty($fsize))
		$fsize = 0;

	return $fsize;
}
//获得格林威治标准时间
function MyDate($format = 'Y-m-d H:i:s', $timest = 0) {
	//设置为东8区
	$addtime = 8 * 3600;
	if (empty($format))
		$format = 'Y-m-d H:i:s';

	return gmdate($format, $timest + $addtime);
}
//获得格式化(Y-m-d H:i:s)的时间
function GetDateTime($mktime) {
	return MyDate('Y-m-d H:i:s', $mktime);
}
//获得格式化(Y-m-d)的日期
function GetDateMk($mktime) {
	return MyDate('Y-m-d', $mktime);
}
//从普通时间转换为Linux时间截
function GetMkTime($dtime) {
	if (!preg_match("/[^0-9]/", $dtime)) {
		return $dtime;
	}
	$dtime = trim($dtime);
	$dt = array(1970, 1, 1, 0, 0, 0);
	$dtime = preg_replace("/[\r\n\t]|日|秒/", " ", $dtime);
	$dtime = str_replace("年", "-", $dtime);
	$dtime = str_replace("月", "-", $dtime);
	$dtime = str_replace("时", ":", $dtime);
	$dtime = str_replace("分", ":", $dtime);
	$dtime = trim(preg_replace("/[ ]{1,}/", " ", $dtime));
	$ds = explode(" ", $dtime);
	$ymd = explode("-", $ds[0]);
	if (!isset($ymd[1]))
		$ymd = explode(".", $ds[0]);
	if (isset($ymd[0]))
		$dt[0] = $ymd[0];
	if (isset($ymd[1]))
		$dt[1] = $ymd[1];
	if (isset($ymd[2]))
		$dt[2] = $ymd[2];
	if (strlen($dt[0]) == 2)
		$dt[0] = '20' . $dt[0];
	if (isset($ds[1])) {
		$hms = explode(":", $ds[1]);
		if (isset($hms[0]))
			$dt[3] = $hms[0];
		if (isset($hms[1]))
			$dt[4] = $hms[1];
		if (isset($hms[2]))
			$dt[5] = $hms[2];
	}
	foreach ($dt as $k => $v) {
		$v = preg_replace("/^0{1,}/", '', trim($v));
		if ($v == '') {
			$dt[$k] = 0;
		}
	}
	$mt = mktime($dt[3], $dt[4], $dt[5], $dt[1], $dt[2], $dt[0]);
	if (!empty($mt))
		return $mt;
	else
		return time();
}
//创建多级目录
function MkDirs($dir) {
	return is_dir($dir) or (MkDirs(dirname($dir)) and mkdir($dir, 0777));
}
// 参数解释 $string： 明文 或 密文 $operation：DECODE表示解密,其它表示加密 $key： 密匙 $expiry：密文有效期
function AuthCode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
	// 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙
	// 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。
	// 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方
	// 当此值为 0 时，则不产生随机密钥
	$ckey_length = 4;// 密匙
	$key = md5($key ? $key : "");// 密匙a会参与加解密
	$keya = md5(substr($key, 0, 16));// 密匙b会用来做数据完整性验证
	$keyb = md5(substr($key, 16, 16));// 密匙c用于变化生成的密文
	$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';// 参与运算的密匙
	$cryptkey = $keya . md5($keya . $keyc);
	$key_length = strlen($cryptkey);
	// 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)，解密时会通过这个密匙验证数据完整性
	// 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确
	$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
	$string_length = strlen($string);
	$result = '';
	$box = range(0, 255);
	$rndkey = array();
	
	// 产生密匙簿
	for ($i = 0; $i <= 255; $i++) {
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}
	// 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上并不会增加密文的强度
	for ($j = $i = 0; $i < 256; $i++) {
		//$j是三个数相加与256取余
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}
	// 核心加解密部分
	for ($a = $j = $i = 0; $i < $string_length; $i++) {
		//在上面基础上再加1 然后和256取余
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;//$j加$box[$a]的值 再和256取余
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		// 从密匙簿得出密匙进行异或，再转成字符，加密和解决时($box[($box[$a] + $box[$j]) % 256])的值是不变的。
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}
	if ($operation == 'DECODE') {
		// 验证数据有效性，请看未加密明文的格式
		if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
			return substr($result, 26);
		} else {
			return '';
		}
	} else {
		// 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因
		// 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码
		return $keyc . str_replace('=', '', base64_encode($result));
	}
}
/* 字符集转换 */
function Auto_Charset($fContents, $from = 'gbk', $to = 'utf-8') {
	$from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
	$to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
	if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && !is_string($fContents))) {
		// 如果编码相同或者非字符串标量则不转换
		return $fContents;
	}
	if (is_string($fContents)) {
		if (function_exists('mb_convert_encoding')) {
			return mb_convert_encoding($fContents, $to, $from);
		} elseif (function_exists('iconv')) {
			return iconv($from, $to, $fContents);
		} else {
			return $fContents;
		}
	} elseif (is_array($fContents)) {
		foreach ($fContents as $key => $val) {
			$_key = Auto_Charset($key, $from, $to);
			$fContents[$_key] = Auto_Charset($val, $from, $to);
			if ($key != $_key)
				unset($fContents[$key]);
		}
		return $fContents;
	} else {
		return $fContents;
	}
}
/* 函数说明：删除非空目录 is_root是否删除根目录 */
function DelFile($dir,$is_root=true) {
	if ($handle = opendir("$dir")) {
		while (false !== ($item = readdir($handle))) {
			if ($item != "." && $item != "..") {
				if (is_dir($dir.DS.$item)) {
					DelFile($dir.DS.$item);
				} else {
					unlink($dir.DS.$item);
				}
			}
		}
		closedir($handle);
		if($is_root){
			rmdir($dir);
		}
	}
}
/* 函数说明：扫描整个目录 */
function ScandirAll($path,$exclude='.gitignore') {
    //判断目录是否为空
    if(!file_exists($path)) {
        return [];
    }

    $files = scandir($path);
    $fileItem = [];
    foreach($files as $v) {
        $newPath = $path .DS . $v;
        if(is_dir($newPath) && $v != '.' && $v != '..') {
            $fileItem = array_merge($fileItem, ScandirAll($newPath));
        }else if(is_file($newPath)&& $v!=$exclude){
            $fileItem[] = $newPath;
        }
    }

    return $fileItem;
}
/**
 * 复制文件夹
 * @param $source
 * @param $dest
 */
function CopyDir($source, $dest)
{
    if (!file_exists($dest)) mkdir($dest);
    $handle = opendir($source);
    while (($item = readdir($handle)) !== false) {
        if ($item == '.' || $item == '..') continue;
        $_source = $source . DS . $item;
        $_dest = $dest .DS . $item;
        if (is_file($_source)) copy($_source, $_dest);
        if (is_dir($_source)) CopyDir($_source, $_dest);
    }
    closedir($handle);
}
//获得文件扩展名
function Get_FileExt($file){
    return strtolower(pathinfo($file, PATHINFO_EXTENSION));
}

//获得URL中的文件名
function Get_FileName($file_path,$is_Ext=true){
    $Array=pathinfo($file_path);
    if($is_Ext==true){
        return $Array['basename'];
    }else{
        return $Array['filename'];
    } 
}
//判断是否是通过手机访问
function IsMobileAccess() {
	if (isset($_SERVER['HTTP_X_WAP_PROFILE']))//如果有HTTP_X_WAP_PROFILE则一定是移动设备
		return TRUE;
	//如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
	if (isset($_SERVER['HTTP_VIA'])) {
		//找不到为flase,否则为true
		return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
	}
	//判断手机发送的客户端标志,兼容性有待提高
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		$clientkeywords = array('nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile');

		//从HTTP_USER_AGENT中查找手机浏览器的关键字
		if (preg_match('/(' . implode('|', $clientkeywords) . ')/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			return TRUE;
		}
	}
	//协议法，因为有可能不准确，放到最后判断
	if (isset($_SERVER['HTTP_ACCEPT'])) {
		//如果只支持wml并且不支持html那一定是移动设备
		//如果支持wml和html但是wml在html之前则是移动设备
		if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
			return TRUE;
		}
	}
	return FALSE;
}
//radio、checkbox、list字段原数据解析
function UnFieldData($data){
	$list=array_filter(explode("\n",$data));
	$array=array();
	foreach($list as $row){
		$fd=explode(':',$row);
		$array[trim($fd[1])]=trim($fd[0]);
	}
	return $array;
}
//截取指定长度的字符串  utf-8专用 汉字和大写字母长度算1，其它字符长度算0.5 $str  原字符串  $len  截取长度  $etc  省略字符...  截取后的字符串
function ReStrLen($str, $len = 10, $etc = '...') {
	$restr = '';
	$i = 0;
	$n = 0.0;
	$strlen = strlen($str);//字符串的字节数
	while (($n < $len) and ($i < $strlen)) {
		$temp_str = substr($str, $i, 1);
		$ascnum = ord($temp_str); //得到字符串中第$i位字符的ASCII码
		//如果ASCII位高与252
		if ($ascnum >= 252) {
			$restr = $restr . substr($str, $i, 6);//根据UTF-8编码规范，将6个连续的字符计为单个字符
			$i = $i + 6;//实际Byte计为6
			$n++;//字串长度计1
		} else if ($ascnum >= 248) {
			$restr = $restr . substr($str, $i, 5);
			$i = $i + 5;
			$n++;
		} else if ($ascnum >= 240) {
			$restr = $restr . substr($str, $i, 4);
			$i = $i + 4;
			$n++;
		} else if ($ascnum >= 224) {
			$restr = $restr . substr($str, $i, 3);
			$i = $i + 3;
			$n++;
		} else if ($ascnum >= 192) {
			$restr = $restr . substr($str, $i, 2);
			$i = $i + 2;
			$n++;
		}
		//如果是大写字母 I除外
		else if ($ascnum >= 65 and $ascnum <= 90 and $ascnum != 73) {
			$restr = $restr . substr($str, $i, 1);
			$i = $i + 1;//实际的Byte数仍计1个
			$n++;//但考虑整体美观，大写字母计成一个高位字符
		}
		//%,&,@,m,w 字符按1个字符宽
		else if (!(array_search($ascnum, array(37, 38, 64, 109, 119)) === FALSE)) {
			$restr = $restr . substr($str, $i, 1);
			$i = $i + 1;//实际的Byte数仍计1个
			$n++;//但考虑整体美观，这些字条计成一个高位字符
		}
		//其他情况下，包括小写字母和半角标点符号
		else {
			$restr = $restr . substr($str, $i, 1);
			$i = $i + 1;//实际的Byte数计1个
			$n = $n + 0.5;//其余的小写字母和半角标点等与半个高位字符宽
		}
	}
	//超过长度时在尾处加上省略号
	if ($i < $strlen) {
		$restr = $restr . $etc;
	}
	return $restr;
}
//快速跳转
function J($url, $msg = "") {
	if ($msg <> "") {
		$str = "<script language='javascript'>alert('" . $msg . "');document.location = '" . $url . "'</script>";
	} else {
		$str = "<script language='javascript'>document.location = '" . $url . "'</script>";
	}
	echo $str;
	exit();
}
//延时跳转
function JTime($url,$time=3){
	echo '<meta http-equiv="refresh" content="'.$time.';url='.$url.'">';
	exit();
}
//显示信息快速条跳转
function ShowMsg($msg = '', $gourl = '-1') {
	if ($gourl == '-1'){
		echo '<script>alert("' . $msg . '");history.go(-1);</script>';
	}else if ($gourl == '0'){
		echo '<script>alert("' . $msg . '");location.reload();</script>';
	}else{
		echo '<script>alert("' . $msg . '");location.href="' . $gourl . '";</script>';
	}
	exit();
}
//生成一个订单号
function CreateOrderID(){
	$date=MyDate('ymd',time());
	$order_pool=RUNTIME_PATH.'order'.DS.$date.'.cache';//订单池
	if(file_exists($order_pool)){
		$list=unserialize(file_get_contents($order_pool));
		if(count($list)>=1){
			$oid=array_pop($list);
			file_put_contents($order_pool,serialize($list));
		}else{
			$oid=CreateOrderPool();
		}
	}else{
		$oid=CreateOrderPool();
	}
	return $oid;
}
//创建一个订单池,并返回一个
function CreateOrderPool($num=10000){
	$date=MyDate('ymdHi',time());
	$date_file=MyDate('ymd',time());
	//订单池
	$order_pool=RUNTIME_PATH.'order'.DS.$date_file.'.cache';
	$list=array();
	for($i=1;$i<=$num;$i++){
		$list[]=$date.mt_rand(10000,99999);
	}
	$list=array_unique($list);
	$p=array_pop($list);
	file_put_contents($order_pool,serialize($list));
	unset($list);
	return $p;
}
//清除地址信息中非正常的字符串
function address_clear($str){
    return str_replace(array("市辖区","市辖县","省直辖行政单位"),"",$str);
}

function Get_Server_Base_Info(){
	
	$l[4]['name']='HTTP_HOST';
	$l[4]['title']='主机地址';
	$l[4]['value']=$_SERVER['HTTP_HOST'];
	

	$l[6]['name']='SERVER_NAME';
	$l[6]['title']='网站绑定域名';
	$l[6]['value']=$_SERVER['SERVER_NAME'];
	
	$l[7]['name']='SERVER_PORT';
	$l[7]['title']='网站绑定端口';
	$l[7]['value']=$_SERVER['SERVER_PORT'];
	
	$l[8]['name']='SERVER_SOFTWARE';
	$l[8]['title']='服务器软件';
	$l[8]['value']=$_SERVER['SERVER_SOFTWARE'];
	
	$l[9]['name']='SERVER_PROTOCOL';
	$l[9]['title']='服务器协议';
	$l[9]['value']=$_SERVER['SERVER_PROTOCOL'];	
	
	$l[9]['name']='PHP_VERSION';
	$l[9]['title']='PHP 版本';
	$l[9]['value']=PHP_VERSION;		
	
	return $l;
}
function Get_PHP_Base_Info(){
	$l[1]['name']='asp_tags';
	$l[1]['title']='ASP标签';
	$l[1]['value']=(ini_get('asp_tags')==1?'支持':'不支持');
	
	$l[2]['name']='max_execution_time';
	$l[2]['title']='最大执行时间';
	$l[2]['value']=ini_get('max_execution_time').'秒';
	
	$l[3]['name']='max_input_time';
	$l[3]['title']='最大输入时间';
	$l[3]['value']=ini_get('max_input_time').'秒';
	
	$l[4]['name']='memory_limit';
	$l[4]['title']='内存使用上限';
	$l[4]['value']=ini_get('memory_limit');
	
	$l[5]['name']='display_errors';
	$l[5]['title']='是否显示错误';
	$l[5]['value']=(ini_get('display_errors')==1?'显示':'不显示');
	
	$l[6]['name']='post_max_size';
	$l[6]['title']='POST提交数据大小限止';
	$l[6]['value']=ini_get('post_max_size');
	
	$l[7]['name']='upload_max_filesize';
	$l[7]['title']='文件上传大小限止';
	$l[7]['value']=ini_get('upload_max_filesize');
	    
	$l[8]['name']='max_file_uploads';
	$l[8]['title']='文件上传数量限止';
	$l[8]['value']=ini_get('max_file_uploads'); 
	
	$l[9]['name']='date.timezone';
	$l[9]['title']='系统时间时区';
	$l[9]['value']=ini_get('date.timezone');     	
	
	return $l;
}

function Get_PHP_Ext_list(){
	$ext_list=get_loaded_extensions();
	
	foreach($ext_list as $k=>$v){
		$ext_list[$k]=strtolower($v);
	}
	
	$name_list=array(
		'Core'=>'PHP核心',
		'bcmath'=>'高精度数学运算',
		'calendar'=>'日历',
		'ctype'=>'字符串体测函数',
		'date'=>'日期时间函数',
		'filter'=>'过滤器',
		'ereg'=>'字符串比对解析',
		'ftp'=>'FTP协议',
		'hash'=>'HASH算法',
		'iconv'=>'字符集转换',
		'json'=>'JSON数据支持',
		'mcrypt'=>'Mcrypt加密算法',
		'SPL'=>'PHP标准库',
		'odbc'=>'开放数据库连接',
		'pcre'=>'PCRE正则',
		'Reflection'=>'反射',
		'session'=>'会话',
		'standard'=>'PHP标准',
		'mysqlnd'=>'MySQL Native 驱动',
		'tokenizer'=>'解析器',
		'zip'=>'ZIP解压',
		'zlib'=>'zlib解压',
		'libxml '=>'XML解析',
		'dom '=>'DOM 解析器',
		'PDO '=>'PDO支持',
		'bz2 '=>'bz2解压',
		'SimpleXML '=>'简单XML解析',
		'openssl '=>'安全套接字',
		'gd '=>'GD库',
		'mbstring '=>'多字节字符串支持',
		'exif '=>'EXIF信息',
		'mysql '=>'MYSQL驱动',
		'mysqli '=>'MYSQLi驱动',
		'pdo_mysql'=>'PDO MYSQL驱动',
		'PDO_ODBC'=>'PDO_ODBC驱动',
		'PDO_ODBC'=>'PDO_ODBC驱动',
		'pdo_sqlite'=>'PDO SQLite 驱动',
		'sqlite3'=>'SQLite3驱动',
		'sockets'=>'Sockets套接字',
		'gmagick'=>'Gmagick图像处理库',
		'mhash'=>'MHash离散算法'
	);
	foreach($name_list as $k=>$v){
		$setlist[strtolower($k)]=$v;
	}
	$ls=array();
	$i=1;
	foreach($name_list as $k=>$v){
		if(in_array($k,$ext_list)){
			$ls[$i]['name']=$k;
			$ls[$i]['title']=$v;
			$ls[$i]['value']="支持";
			$i++;
		}
	}
	return $ls;
}
//配置文件管理-获取
function Get_Config($file, $name) {
	$file=RUNTIME_PATH.'config'.DS.$file;
	if (file_exists($file)) {
		$list = unserialize(file_get_contents($file));
		if (isset($list[$name])) {
			return $list[$name];
		} else {
			return false;
		}
	} else {
		return false;
	}
}
//配置文件管理-设置
function Set_Config($file, $name, $value) {
	$file=RUNTIME_PATH.'config'.DS.$file;
	if (!file_exists($file)) {
		file_put_contents($file, serialize(array()));
	}
	$list = unserialize(file_get_contents($file));
	$new_arr = array($name => $value);
	$new = array_merge($list, $new_arr);
	if (file_put_contents($file, serialize($new))) {
		return true;
	} else {
		return false;
	}
}

//xml转换成数组
function XmlToArray( $xml ){
	return (array)simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
}
//数组转换成Xml
function ToXml($arr){
	$xml = "<xml>";
	foreach ($arr as $key=>$val){
		if (is_numeric($val)){
			$xml.="<".$key.">".$val."</".$key.">";
		}else{
			$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
		}
	}
	$xml.="</xml>";
	return $xml; 
}
//搜索并清理附件(图片)文件 1:单路径  2：多路径  3：编辑器
function ClearDocumentImages($content,$type=1){
	if($type==1){
		$file=PathDS(ROOT_PATH.$content);
		@unlink($file);
		return true;
	}else if($type==2){
		$List=unserialize($content);
	}else if($type==3){
		$preg="# src=\"(.*)\"#iUs";
		preg_match_all($preg, $content,$arr);
		$List=$arr[1];
	}else{
		return false;
	}
	foreach($List as $v){
		if(strpos($v,'http') !==0){
			@unlink(PathDS(ROOT_PATH.$file));
		}
	}
	return true;
}
//本地写入路径 斜线自动根据win、linux平台转换 （主要用于本地文件读写操作）
function PathDS($str){
	$str=str_replace("\\",DS,$str);
    return str_replace("/",DS,$str);
}
//资源路径 斜线自动转换 (主要用于相路径)
function PathDS2($str){
	return str_replace("\\","/",$str);
}
//判断是否是微信浏览器
function IsWeixin() { 
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) { 
        return true; 
    }else{
		return false;
	}
}
//获得当前页面的全路径
function GetCurrentUrl(){ 
  $current_url='http://'; 
  if(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on'){ 
    $current_url='https://'; 
  } 
//   if($_SERVER['SERVER_PORT']!='80'){ 
//     $current_url.=$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI']; 
//   }else{ 
    $current_url.=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
//   } 
  return $current_url; 
}
//CURL异步将远程链接上的内容(图片或内容)写到本地
function PutFileFromUrlContent($url, $savefile) {
	  set_time_limit ( 0 );// 设置运行时间为无限制
	  $url = trim ( $url );
	  $curl = curl_init ();
	  curl_setopt ( $curl, CURLOPT_URL, $url );// 设置你需要抓取的URL
	  curl_setopt ( $curl, CURLOPT_HEADER, 0 ); // 设置header
	  curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );// 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
	  $file = curl_exec ( $curl );// 运行cURL，请求网页
	  curl_close ( $curl );// 关闭URL请求
	  $write = @fopen ( $savefile, "w" ); // 将文件写入获得的数据
	  if ($write == false) {
	    return false;
	  }
	  if (fwrite ( $write, $file ) == false) {
	    return false;
	  }
	  if (fclose ( $write ) == false) {
	    return false;
	  }
}
//CURL POST提交数据
function Post_Curl($data, $url, $second = 30){		
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_TIMEOUT, $second);//设置超时
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);//严格校验
	curl_setopt($ch, CURLOPT_HEADER, FALSE);//设置header
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);//要求结果为字符串且输出到屏幕上
	curl_setopt($ch, CURLOPT_POST, TRUE);//post提交方式
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$data = curl_exec($ch);//运行curl
	//返回结果
	if($data){
		curl_close($ch);
		return $data;
	} else { 
		$error = curl_errno($ch);
		curl_close($ch);
		WLog('Post_Curl_Error',"CURL出错，错误码:$error");
	}
}
//CURL JSON POST提交数据
function Post_Json($data, $url){		
    $options = array(
      'http' => array(
        'method'  => 'POST',
        'header'  => 'Content-type:application/json',
        //header 需要设置为 JSON
        'content' => $data,
        'timeout' => 60
        //超时时间
      )
    );
    $context = stream_context_create( $options );
    $result = file_get_contents($url, false, $context );
    return $result;
}
/**
 * 根据经纬度和半径计算出范围
 * @param string $lat 纬度
 * @param String $lng 经度
 * @param float $radius 半径
 * @return Array 范围数组
 */
function CalcScope($lat, $lng, $radius) {
	$degree = (24901*1609)/360.0;
	$dpmLat = 1/$degree;
   
	$radiusLat = $dpmLat*$radius;
	$minLat = $lat - $radiusLat;    // 最小纬度
	$maxLat = $lat + $radiusLat;    // 最大纬度
   
	$mpdLng = $degree*cos($lat * (pi()/180));
	$dpmLng = 1 / $mpdLng;
	$radiusLng = $dpmLng*$radius;
	$minLng = $lng - $radiusLng;   // 最小经度
	$maxLng = $lng + $radiusLng;   // 最大经度
   
	/** 返回范围数组 */
	$scope = array(
	  'minLat'  => $minLat,
	  'maxLat'  => $maxLat,
	  'minLng'  => $minLng,
	  'maxLng'  => $maxLng
	  );
	return $scope;
}
/**
 * 获取两个经纬度之间的距离
 * @param string $lat1 纬一
 * @param String $lng1 经一
 * @param String $lat2 纬二
 * @param String $lng2 经二
 * @return float 返回两点之间的距离
 */
function CalcDistance($lat1, $lng1, $lat2, $lng2) {
	/** 转换数据类型为 double */
	$lat1 = doubleval($lat1);
	$lng1 = doubleval($lng1);
	$lat2 = doubleval($lat2);
	$lng2 = doubleval($lng2);
	/** 以下算法是 Google 出来的，与大多数经纬度计算工具结果一致 */
	$theta = $lng1 - $lng2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	return ($miles * 1.609344);
}
//移除微信昵称中的emoji字符
function removeEmoji($nickname) {
	
    $clean_text = $nickname;
    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $clean_text);
	
    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}
//计算周
function get_week($date){
    //强制转换日期格式
    $date_str=date('Y-m-d',strtotime($date));
    //封装成数组
    $arr=explode("-", $date_str);
    //参数赋值-年
    $year=$arr[0];
    //月，输出2位整型，不够2位右对齐
    $month=sprintf('%02d',$arr[1]);
    //日，输出2位整型，不够2位右对齐
    $day=sprintf('%02d',$arr[2]);
    //时分秒默认赋值为0；
    $hour = $minute = $second = 0;
    //转换成时间戳
    $strap = mktime($hour,$minute,$second,$month,$day,$year);
    //获取数字型周几
    $number_wk=date("w",$strap);
    //自定义周数组
    $weekArr=array("周日","周一","周二","周三","周四","周五","周六");
    //获取数字对应的周
    return $weekArr[$number_wk];
}
/**
 * 求两个日期之间相差的天数
 * (针对1970年1月1日之后，求之前可以采用泰勒公式)
 * @param string $day1
 * @param string $day2
 * @return number
 */
 function diffBetweenTwoDays ($day1, $day2)
 {
   $second1 = strtotime($day1);
   $second2 = strtotime($day2);
	 
   if ($second1 < $second2) {
	 $tmp = $second2;
	 $second2 = $second1;
	 $second1 = $tmp;
   }
   return ($second1 - $second2) / 86400;
 }
//数组转换成字符串包括key
function array2string($array){
    $string = [];
    if($array && is_array($array)){
        foreach ($array as $key=> $value){
            $string[] = $key.':'.$value;
        }
    }
    return implode(",",$string);
}
//img标签相对图片路径替换服务器绝对路径 type=1 单图片 2:多图片 3:编辑器
function ImageToServer($content,$server_url,$type=1){
	if($type==1){
        if(0===strpos($content,'http') || (0===strpos($content,'data:') && false!==strpos($content,'base64')))return $content;
		if(trim($content)==""){
			return '';
		}else{
			return PathDS2($server_url.$content);
		}		
	}else if($type==2){
        $imgs=explode(",",$content);
		$nlst=array();
		foreach($imgs as $k=>$img){
			if(trim($img)!="" && false===strpos($img,'http') && false === strpos($img,'data:') && false === strpos($img,'base64')){
				$nlst[$k]=PathDS2($server_url.$img);
			}
		}
		return $nlst;
	}else{
		preg_match_all('/(?<=img.src=").*?(?=")/',$content, $out, PREG_PATTERN_ORDER);      //正则匹配img标签的src属性，返回二维数组
		if (!empty($out)) {
			foreach ($out as $v) {
				foreach ($v as $j) {
                    if(false===strpos($j,'http') && false===strpos($j,'data:') && false === strpos($j,'base64')){
                        $url = $server_url.$j; 
					    $content = str_replace($j, $url,$content);   //替换相对路径为绝对路径
                    }
				}
			}
        }
		return $content;
	}
}
//获取输入流并把JSON转换成数组,主要用于小程序和APP数据传输接收
function get_input_stream(){
    $postStr = file_get_contents("php://input");
    return json_decode($postStr,true);
}

/**
 * 判断当前的时分是否在指定的时间段内
 * @param $start 开始时分  eg:10:30
 * @param $end  结束时分   eg:15:30
 * @author:mzc
 * @date:2018/8/9 10:46
 * @return: bool  1：在范围内，0:没在范围内
 */
function checkIsBetweenTime($start,$end){
    $date= date('H:i');
    $curTime = strtotime($date);//当前时分
    $assignTime1 = strtotime($start);//获得指定分钟时间戳，00:00
    $assignTime2 = strtotime($end);//获得指定分钟时间戳，01:00
    $result = 0;
    if($curTime>$assignTime1&&$curTime<$assignTime2){
        $result = 1;
    }
    return $result;
}
//php模块安装检测
function check_phpext($name){
	$ext_list=get_loaded_extensions();
	if(in_array($name,$ext_list)){
		return true;
	}else{
		return false;
	}
}
//检测系统数据库是否安装正常
function CheckDataBase(){
	//连接数据库测试
	//获得默认数据库配置
	$config = config('database');
	$mysql_conn=@mysqli_connect($config['hostname'],$config['username'],$config['password'],$config['database']);
	if(!$mysql_conn){
		J('/install','FastPHP数据库连接失败，请重新安装！');
	}else{
		return true;
	}
}
//获得汉字的首字母-一般用于数组汉字排序
function GetFirstChar($s0) {
    $fchar = ord(substr($s0, 0, 1));
    if (($fchar >= ord("a") and $fchar <= ord("z"))or($fchar >= ord("A") and $fchar <= ord("Z"))) return strtoupper(chr($fchar));
    $s = iconv("UTF-8", "GBK", $s0);
    $asc = ord($s{0}) * 256 + ord($s{1})-65536;
    if ($asc >= -20319 and $asc <= -20284)return "A";
    if ($asc >= -20283 and $asc <= -19776)return "B";
    if ($asc >= -19775 and $asc <= -19219)return "C";
    if ($asc >= -19218 and $asc <= -18711)return "D";
    if ($asc >= -18710 and $asc <= -18527)return "E";
    if ($asc >= -18526 and $asc <= -18240)return "F";
    if ($asc >= -18239 and $asc <= -17923)return "G";
    if ($asc >= -17922 and $asc <= -17418)return "H";
    if ($asc >= -17417 and $asc <= -16475)return "J";
    if ($asc >= -16474 and $asc <= -16213)return "K";
    if ($asc >= -16212 and $asc <= -15641)return "L";
    if ($asc >= -15640 and $asc <= -15166)return "M";
    if ($asc >= -15165 and $asc <= -14923)return "N";
    if ($asc >= -14922 and $asc <= -14915)return "O";
    if ($asc >= -14914 and $asc <= -14631)return "P";
    if ($asc >= -14630 and $asc <= -14150)return "Q";
    if ($asc >= -14149 and $asc <= -14091)return "R";
    if ($asc >= -14090 and $asc <= -13319)return "S";
    if ($asc >= -13318 and $asc <= -12839)return "T";
    if ($asc >= -12838 and $asc <= -12557)return "W";
    if ($asc >= -12556 and $asc <= -11848)return "X";
    if ($asc >= -11847 and $asc <= -11056)return "Y";
    if ($asc >= -11055 and $asc <= -10247)return "Z";
    return null;
}
function FieldConverList($data,$fields=[])
{
     //设置系统默认处理规则
     $default=array(
        'createtime'=>'time',
        'updatetime'=>'time',
        'deletetime'=>'time',
        'posttime'=>'time',
        'paytime'=>'time',
        'sendtime'=>'time',
        'arrivetime'=>'time',
        'receivetime'=>'time',
        'jointime'=>'time',
        'logintime'=>'time',
        'prevtime'=>'time',

        'picurl'=>'image',
        'videourl'=>'image',
        'avatar'=>'image',

        'picarr'=>'images',
        'videoarr'=>'images',
		'content'=>'editor',
		'desc'=>'editor',
    );
    if(count($fields)>0){
        $default = array_merge($default,$fields);
    }
    return FieldConver($data,$default);
}
//数据字段输出预处理方法
function FieldConver(&$data,$fields)
{
    if(is_object($data))$data = $data->toArray();
    $weburl = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'];
    foreach($data as $k=>&$v){
        if(is_object($v))$v = $v->toArray();
        if(!is_array($v)){
            $type = $fields[$k]??$k;
            switch($type){
                case 'time':
                    if(is_int($v) && $v>10000){
                        $v = date('Y-m-d H:i:s',$v);
                    }elseif(empty($v)){
                        $v = "无";
                    }else{
                        $v;
                    }		
					break;
				case 'date':
					if(is_int($v) && $v>10000){
                        $v = date('Y-m-d',$v);
                    }elseif(empty($v)){
                        $v = "无";
                    }else{
                        $v;
                    }		
					break;
                case 'image':			$v=ImageToServer($v,$weburl,1);	                break;
                case 'images':		    $v=ImageToServer($v,$weburl,2);	                break;
                case 'editor':			$v=ImageToServer($v,$weburl,3);	                break;
                case 'mobile_hidden':	$v=HiddenMobile($v);							break;
            }
        }else{
            FieldConver($v,$fields);
        }
    }
    return $data;
}
?>