<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class Feedback extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    /*
        批量操作(删除、启用、禁用、...)
    */
    static public function Bat($data)
    {
        $type='bat_'.$data['type'];
        $ids=\implode(',',array_unique(array_filter(\explode(',',$data['ids']))));
        return self::$type($ids);
    }
    /*
        批量启用 status:0 启用 1:禁用
    */
    static public function bat_enable($ids)
    {
        return self::where("`id` IN ($ids)")->update(['status'=>0]);
    }
    /*
        批量禁用 status:0 启用 1:禁用
    */
    static public function bat_disable($ids)
    {
        return self::where("`id` IN ($ids)")->update(['status'=>1]);
    }
    /*
        批量删除
    */
    static public function bat_del($ids)
    {
        $Feedback=new Feedback;
        return $Feedback->destroy($ids);
    }    
}