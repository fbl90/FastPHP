<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Models;
use \app\base\Tree;
use \app\admin\model\Syslog;

class Cats extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';
    /*
        读取树型列表
    */
    static public function TreeList()
    {   
		$list=self::alias('c')
		->join("model m",'m.id=c.mid')
		->field('m.model_name,c.*')
		->order('c.orderid ASC')->select();
		//生成树型结构
        $tree=Tree::Create_Tree_Html($list,'parent','classname',0);
        if (count($list)>=1) {
            $list=Tree::Create_Tree($list, 'parent', 0);
            foreach ($list as $k=>$v) {
                $list[$v['id']]=$v;
                if ($v['level']>=2) {
                    $list[$v['id']]['classname_indent']=str_prefix('├', $v['level']-1, '&nbsp;&nbsp;&nbsp;&nbsp;');
                } else {
                    $list[$v['id']]['classname_indent']="";
                }
                $list[$v['id']]['status_s']=($v['status']==0?'已启用':'已禁用');
            }
        }
        return ['tree'=>$tree,'list'=>$list];
    }
    /*
        保存数据
    */
    static public function SaveData($data)
    {   
        $id=$data['id']??0;
        
        unset($data['id']);
        $data['posttime']=time();

		if($data['parent']>=1){
			//获得上级分类
			$p=self::get($data['parent'])->toArray();
			if(!is_array($p)){
				return \Result(1,'无郊上级');
			}
			$path=$p['path'].$data['parent'].',';
		}else{
			$path=',0,';
		}

        //获得模型信息
        $model=Models::get($data['mid'])->toArray();
		if(!is_array($model)){
			return \Result(1,'无郊模型');
        }
        
        $count=self::where("`classname`='".$data['classname']."' AND `parent`='".$data['parent']."' AND `id`<>'$id'")->count();
        if($count>=1){
            return \Result(1,'栏目已存在！');
        }
        //计算栏目层级
        $data['level']=count(ClearNullArray(explode(',',$path)));
        $data['path']=$path;
        if($id>0){
            Syslog::Rec(1,"更新了栏目".$data['classname'],0);
            return (self::where('id',$id)->update($data))?\Result(0,'操作成功'):Result(1,'操作失败');
        }else{
            Syslog::Rec(1,"创建了栏目".$data['classname'],0);
            return (self::create($data))?\Result(0,'操作成功'):Result(1,'操作失败');
        }
    }
    /*
        删除
    */
    static public function del($id)
    {
        $subcats=self::where('parent',$id)->count();
        if($subcats>=1){
            return \Result(1,'存在子栏目,删除失败');
        }
        Syslog::Rec(1,"删除了栏目".$id,0);
        return self::destroy($id)?\Result(0,'操作成功'):Result(1,'操作失败');
    }    
    /*
        排序
    */
    static public function Sort($data)
    {
        $Cats=new Cats;
        foreach($data['field_ids'] as $k=>$v){
            $list[]=['id'=>$v,'orderid'=>$data['orderid_ipt'][$k]];
        }
        Syslog::Rec(1,"更新了栏目排序",0);
        return $Cats->isUpdate()->saveAll($list);
    }
    /*
        批量操作(删除、启用、禁用、...)
    */
    static public function Bat($data)
    {
        $type='bat_'.$data['type'];
        $ids=\implode(',',array_unique(array_filter(\explode(',',$data['ids']))));
        return self::$type($ids);
    }
    /*
        批量启用 status:0 启用 1:禁用
    */
    static public function bat_enable($ids)
    {
        Syslog::Rec(1,"批量启用了栏目[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>0]);
    }
    /*
        批量禁用 status:0 启用 1:禁用
    */
    static public function bat_disable($ids)
    {
        Syslog::Rec(1,"批量禁用了栏目[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>1]);
    }

}