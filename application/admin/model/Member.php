<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class Member extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    public function getTypeAttr($val)
    {   
        $types=array('site'=>'站内','qq'=>'QQ','wx'=>'微信','weibo'=>'微博','baidu'=>'百度','taobao'=>'淘宝');
        return $types[$val];
    }
    /*
        读取分页
    */
    static public function GetPage($kw)
    {
        $where=[];
    	if($kw!=""){
            $where[]="(username LIKE '%$kw%' OR nickname LIKE '%$kw%' OR mobile LIKE '%$kw%')";
        }
        $list=self::where(\implode("and",$where))->order("regtime DESC")->paginate(30, false, ['query' => request()->param()]);
        return ['list'=>$list,'pagelist'=>$list->render()];
    }
    /*
        批量操作(删除、启用、禁用、...)
    */
    static public function Bat($data)
    {
        $type='bat_'.$data['type'];
        $ids=\implode(',',array_unique(array_filter(\explode(',',$data['ids']))));
        return self::$type($ids);
    }
    /*
        批量启用 status:0 启用 1:禁用
    */
    static public function bat_enable($ids)
    {
        Syslog::Rec(1,"批量启用会员[".$ids."]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>0]);
    }
    /*
        批量禁用 status:0 启用 1:禁用
    */
    static public function bat_disable($ids)
    {
        Syslog::Rec(1,"批量禁用会员[".$ids."]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>1]);
    }
    /*
        批量删除
    */
    static public function bat_del($ids)
    {
        $Member=new Member;
        Syslog::Rec(1,"批量删除会员[".$ids."]",0);
        return $Member->destroy($ids);
    }
}