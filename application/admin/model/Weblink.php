<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\WeblinkClass;
use \app\base\Tree;
use \app\admin\model\Syslog;

class Weblink extends Model
{
    use SoftDelete;

    protected $deleteTime = 'deltime';

    static public function getWeblinkClassListId()
    {
        $l=WeblinkClass::select();
        $lst=array();
        foreach($l as $v){
            $lst[$v['id']]=$v;
        }
        return $lst;
    }

    static public function GetPage()
    {
        $classlist=self::getWeblinkClassListId();
        $list=self::order('id DESC')->paginate(30, false, ['query' => request()->param()]);
        $slst=array();
        foreach($list as $k=>$v){
        	$slst[$k]=$v;
        	if(isset($classlist[$v['classid']]['classname'])){
        		$slst[$k]['classname']=$classlist[$v['classid']]['classname'];
        	}else{
        		$slst[$k]['classname']='默认分类';
        	}
        }
        return [
            'classlist'=>$classlist,
            'pagelist'=>$list->render(),
            'list'=>$slst
        ];
    }
    /*
        保存
    */
    static public function SaveData($data)
    {
        $data['picurl']=$data['picurl']??'';
        $data['posttime']=time();
        if($data['id']>0){
            Syslog::Rec(1,"更新友情链接".$data['webname'],0);
            return self::update($data)?\Result(0,'操作成功'):\Result(1,'操作失败');
        }else{
            unset($data['id']);
            Syslog::Rec(1,"创建友情链接".$data['webname'],0);
            return self::create($data)?\Result(0,'操作成功'):\Result(1,'操作失败');
        }
    }
    /*
        排序
    */
    static public function Sort($data)
    {
        $Weblink=new Weblink;
        foreach($data['field_ids'] as $k=>$v){
            $list[]=['id'=>$v,'orderid'=>$data['orderid_ipt'][$k]];
        }
        Syslog::Rec(1,"更新友情链接排序",0);
        return $Weblink->isUpdate()->saveAll($list);
    }    
}