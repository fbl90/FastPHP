<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class WeblinkClass extends Model
{
    use SoftDelete;

    protected $deleteTime = 'deltime';
    
    static public function SaveData($data)
    {
        $data['picurl']= $data['picurl']??'';
        $data['posttime']=time();
        if($data['id']>0){
            Syslog::Rec(1,"更新友情链接分类".$data['classname'],0);
            return self::update($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }else{
            unset($data['id']);
            Syslog::Rec(1,"创建友情链接分类".$data['classname'],0);
            return self::create($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }      
    }
}