<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\shop\GoodsClass;
use \app\admin\model\Syslog;

class Goods extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';
    /*
        读取分页
    */
    static public function GetPage($cid,$kw='')
    {   
        $where=[];
        if($kw!=""){
            $where[]="(`gid` like '%$kw%' OR `goodsname` like '%$kw%')";
        }

        if($cid>0){
            $where[]="(`classid`='$cid' OR `path` like '%$cid%')";
        }

        $list=self::where(\implode("and",$where))->order("orderid DESC")->paginate(30, false, ['query' => request()->param()]);

        $lst=array();
        $class_list=GoodsClass::GetGoodsClassListId();
		foreach($list as $k=>$v){
			$lst[$k]=$v;
			if(isset($class_list[$v['classid']])){
				$lst[$k]['classname']=$class_list[$v['classid']]['classname'];
			}else{
				$lst[$k]['classname']='默认分类';
			}
		}
        return ['list'=>$lst,'pagelist'=>$list->render()];
    }
    /*
        保存数据
    */
    static public function SaveData($data)
    {   
        $id=$data['id']??0;
        
        unset($data['cid']);
        
        $data['picurl']=$data['picurl']??'';
		$data['picarr']=$data['picarr']??[];
		$data['flag']=$data['flag']??[];
		
		$data['picarr']=implode(',',$data['picarr']);
		$data['flag']=implode(',',$data['flag']);
		$data['posttime']=GetMkTime($data['posttime']);
		
		//获得分类和品牌信息
		
		$classinfo=GoodsClass::get($data['classid']);
		if($classinfo){
			$data['path']=$classinfo['path'];
		}else{
			$data['path']="";
		}
		if($id>0){
            Syslog::Rec(1,'更新了商品【'.$data['goodsname'].'】',0);
            return self::update($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }else{
            unset($data['id']);
            Syslog::Rec(1,'创建了商品【'.$data['goodsname'].'】',0);
            return self::create($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }
    }
    /*
        排序
    */
    static public function Sort($data)
    {
        $Goods=new Goods;
        foreach($data['field_ids'] as $k=>$v){
            $list[]=['id'=>$v,'orderid'=>$data['orderid_ipt'][$k]];
        }
        Syslog::Rec(1,"更新了商品分类排序",0);
        return $Goods->isUpdate()->saveAll($list);
    }
    /*
        批量操作(删除、启用、禁用、...)
    */
    static public function Bat($data)
    {
        $type='bat_'.$data['type'];
        $ids=\implode(',',array_unique(array_filter(\explode(',',$data['ids']))));
        return self::$type($ids);
    }
    /*
        批量启用 status:0 启用 1:禁用
    */
    static public function bat_enable($ids)
    {
        Syslog::Rec(1,"批量操作商品了启用[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>0]);
    }
    /*
        批量禁用 status:0 启用 1:禁用
    */
    static public function bat_disable($ids)
    {
        Syslog::Rec(1,"批量操作商品了禁用[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>1]);
    }
    /*
        批量删除
    */
    static public function bat_del($ids)
    {
        $Goods=new Goods;
        return $Goods->destroy($ids);
    }
}