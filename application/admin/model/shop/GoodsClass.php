<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\base\Tree;
use \app\admin\model\Syslog;

class GoodsClass extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';
    /*
        获取分类列表(以ID为Key)
    */
    static public function GetGoodsClassListId()
    {
        $l=GoodsClass::select();
        $lst=array();
        foreach($l as $v){
            $lst[$v['id']]=$v;
        }
        return $lst;
    }    
    /*
        读取树型列表
    */
    static public function TreeList()
    {   
        $list=self::order('orderid asc')->select();
        $tree=Tree::Create_Tree_Html($list,'parent','classname',0);

        if(count($list)>=1){
        	$list=Tree::Create_Tree($list,'parent',0);
	        foreach($list as $k=>$v){
	        	$list[$v['id']]=$v;
	        	if($v['level']>=2){
	        		$list[$v['id']]['classname_indent']=str_prefix('├', $v['level']-1, '&nbsp;&nbsp;&nbsp;&nbsp;');
	        	}else{
	        		$list[$v['id']]['classname_indent']="";
	        	}
	        	
	        	$list[$v['id']]['status_s']=($v['status']==0?'已启用':'已禁用');
	        }
        }
        return ['cats_tree'=>$tree,'list'=>$list];
    }
    /*
        保存数据
    */
    static public function SaveData($data)
    {   
        $data['picurl']=$data['picurl']??'';
        $id=$data['id']??0;
		if($data['parent']>=1){
			//获得上级分类
			$parent_info=self::get($data['parent']);
			if(!$parent_info){
				return \Result(1,'父级分类不存在');
			}
			$data['path']=$parent_info['path'].$data['parent'].',';
		}else{
			$data['path']=',0,';
        }
        
        //判断是否有重复分类名
        $where[]="(`classname`='".$data['classname']."' AND `parent`='".$data['parent']."')";
		if($id>=1){
            $where[]="(`id`<>'$id')";
		}
        $count=self::where(\implode('and',$where))->count();
		if($count>0){
            return \Result(1,'此分类下已经存在此分类了!');
		}
		//计算栏目层级
		$data['level']=count(ClearNullArray(explode(',',$data['path'])));
        $data['posttime']=time();

        if($id>0){
            Syslog::Rec(1,'更新了商品分类【'.$data['classname'].'】',0);
            return self::update($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }else{
            unset($data['id']);
            Syslog::Rec(1,'创建了商品分类【'.$data['classname'].'】',0);
            return self::create($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }
    }
    /*
        排序
    */
    static public function Sort($data)
    {
        $GoodsClass=new GoodsClass;
        foreach($data['field_ids'] as $k=>$v){
            $list[]=['id'=>$v,'orderid'=>$data['orderid_ipt'][$k]];
        }
        Syslog::Rec(1,'更新了商品分类排序',0);
        return $GoodsClass->isUpdate()->saveAll($list);
    }
    /*
        批量操作(删除、启用、禁用、...)
    */
    static public function Bat($data)
    {
        $type='bat_'.$data['type'];
        $ids=\implode(',',array_unique(array_filter(\explode(',',$data['ids']))));
        return self::$type($ids);
    }
    /*
        批量启用 status:0 启用 1:禁用
    */
    static public function bat_enable($ids)
    {
        Syslog::Rec(1,"批量操作商品分类了启用[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>0]);
    }
    /*
        批量禁用 status:0 启用 1:禁用
    */
    static public function bat_disable($ids)
    {
        Syslog::Rec(1,"批量操作商品分类了禁用[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>1]);
    }
    /*
        批量删除
    */
    static public function bat_del($ids)
    {
        return false;
    }    
}