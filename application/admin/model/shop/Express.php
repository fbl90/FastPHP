<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;

class Express extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

}