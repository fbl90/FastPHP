<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class PayRecord extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    //生成查询查询条件
    static public function build_sql($type,$status,$kw,$stime,$etime)
    {
        $where=[];
        if($kw!=""){ 
            $where[]="(`oid`='$kw' OR`payoid`='$kw' OR `uname`='$kw')";
        }
    	if($status>0){
    		$where[]="(`status`=$status)";
        }
        if($type>0){
            $where[]="(`type`=$type)";
        }
        if(is_null($stime)){
			$stime=time()-604800;
		}else{
			$stime=GetMkTime($stime);
		}
		if(is_null($etime)){
			$etime=time();
		}else{
			$etime=GetMkTime($etime);
        }
        $where[]="(`posttime`>=$stime AND `posttime` <=$etime)";
        return $where;
    }
    /*
        读取分页
    */
    static public function GetPage($type,$status,$kw,$stime,$etime)
    {   
        $where=self::build_sql($type,$status,$kw,$stime,$etime);
		$list=self::where(\implode("and",$where))->order("`posttime` DESC")->paginate(30, false, ['query' => request()->param()]);

        return ['list'=>$list,'pagelist'=>$list->render()];
    }
    /*
        导出Excel
    */
    static public function Export_Excel($type,$status,$kw,$stime,$etime)
    {
        $where=self::build_sql($type,$status,$kw,$stime,$etime);
        $list=self::where(\implode("and",$where))->order("`posttime` DESC")->select();
        
    	$data[1][0]='支付流水';
    	$data[1][1]='支付类型';
    	$data[1][2]='订单号';
    	$data[1][3]='会员名';
    	$data[1][4]='附加信息';
    	$data[1][5]='支付金额';
    	$data[1][6]='支付时间';
    	$data[1][7]='支付状态';
        
    	$status=array(0=>'未支付',1=>'已支付');
		$paytype=array(1=>'支付宝',2=>'微信支付',3=>'银行卡');
		
        $i=2;
		foreach($list as $k=>$v){
			$data[$i][0]=$v['payoid'];
			$data[$i][1]=@$paytype[$v['type']];
			$data[$i][2]=$v['oid'];
			$data[$i][3]=$v['uname'];
			$data[$i][4]=$v['msg'];
			$data[$i][5]=$v['amount'];
			$data[$i][6]=MyDate('',$v['posttime']);
			$data[$i][7]=@$status[$v['status']];
			$i++;
		}
        
		include_once(APP_PATH.'/extend/excel/Excel.php');
		$filename=DS.'runtime'.DS.'temp_xls'.DS.'export_xls_'.time().'.xls';
		$path=ROOT_PATH.$filename;
        ArrayToXls($data, $path);
        Syslog::Rec(1,"导出支付订单为Excel",0);
        return $filename;
    }    
}