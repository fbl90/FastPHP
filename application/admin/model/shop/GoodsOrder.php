<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\shop\GoodsOrderdetails;
use \app\admin\model\Syslog;

class GoodsOrder extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    //生成查询查询条件
    static public function build_sql($status,$kw,$stime,$etime)
    {
        $where=[];
        if($kw!=""){ 
            $where[]="(`oid`='$kw' OR `nickname`='$kw' OR `uname`='$kw' OR `consignee_name`='$kw' OR `consignee_mobile`='$kw')";
        }
    	if($status>0){
    		$where[]="(`status`=$status)";
        }
        if(is_null($stime)){
			$stime=time()-604800;
		}else{
			$stime=GetMkTime($stime);
		}
		if(is_null($etime)){
			$etime=time();
		}else{
			$etime=GetMkTime($etime);
        }
        $where[]="(`posttime`>=$stime AND `posttime` <=$etime)";
        return $where;
    }
    /*
        读取分页
    */
    static public function GetPage($status,$kw,$stime,$etime)
    {   
        $where=self::build_sql($status,$kw,$stime,$etime);
		$list=self::where(\implode("and",$where))->order("`posttime` DESC")->paginate(30, false, ['query' => request()->param()]);
		
		$relist=array();
		foreach($list as $k=>$v){
			$relist[$k]=$v;
			$relist[$k]['glist']=GoodsOrderdetails::where('oid',$v['oid'])->select();
        }

        return ['list'=>$relist,'pagelist'=>$list->render()];
    }
    /*
        导出Excel
    */
    static public function Export_Excel($status,$kw,$stime,$etime)
    {
        $where=self::build_sql($status,$kw,$stime,$etime);
        $list=self::where(\implode("and",$where))->order("`posttime` DESC")->select();
        
		$data[1][0]='订单号';
		$data[1][1]='昵称';
		$data[1][2]='产品列表';
		$data[1][3]='运费';
		$data[1][4]='总计';
		$data[1][5]='收货人';
		$data[1][6]='收货手机';
		$data[1][7]='收货地址';
		$data[1][8]='支付方式';
		$data[1][9]='物流公司';
		$data[1][10]='物流单号';
		$data[1][11]='状态';
        $data[1][12]='下单时间';
        
		$status=array(1=>'已下单',2=>'已支付',3=>'已发货',4=>'已收货',5=>'已评价');
        $paytype=array(1=>'货到付款',2=>'支付宝',3=>'微信支付',4=>'京东支付',5=>'快钱');

        $relist=array();
        $i=2;

		foreach($list as $k=>$v){
			$data[$i][0]=$v['oid'];
			$data[$i][1]=$v['nickname'];
			$glist=GoodsOrderdetails::where('oid', $v['oid'])->select();
			$glist_string="";
			foreach ($glist as $g) {
				$glist_string.="【".$g['gid']."】".$g['gname']."【".$g['num']."件】 \n";
			}
			$data[$i][2]=$glist_string;
			$data[$i][3]=$v['freight'];
			$data[$i][4]=$v['amount'];
			$data[$i][5]=$v['consignee_name'];
			$data[$i][6]=$v['consignee_mobile'];
			$data[$i][7]=$v['consignee_address'];
			$data[$i][8]=@$paytype[$v['paytype']];
			$data[$i][9]=$v['express_name'];
			$data[$i][10]=$v['express_id'];
			$data[$i][11]=@$status[$v['status']];
			$data[$i][12]=MyDate('', $v['posttime']);
			$i++;            
        }
        
		include_once(APP_PATH.'/extend/excel/Excel.php');
		$filename=DS.'runtime'.DS.'temp_xls'.DS.'export_xls_'.time().'.xls';
		$path=ROOT_PATH.$filename;
		ArrayToXls($data, $path);
		Syslog::Rec(1,"导出订单为Excel",0);
        return $filename;
    }
}