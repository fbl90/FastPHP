<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

/*
    会员提现模型
*/

class MemberDrawcash extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    //生成查询查询条件
    static public function build_sql($status,$kw,$stime,$etime)
    {
        $where=[];
        if($kw!=""){ 
            $where[]="(`bankname`='$kw' OR`opening_bank`='$kw' OR `cardholder`='$kw' OR `cardno`='$kw')";
        }
    	if($status>0){
    		$where[]="(`status`=$status)";
        }
        if(is_null($stime)){
			$stime=time()-604800;
		}else{
			$stime=GetMkTime($stime);
		}
		if(is_null($etime)){
			$etime=time();
		}else{
			$etime=GetMkTime($etime);
        }
        $where[]="(`posttime`>=$stime AND `posttime` <=$etime)";
        return $where;
    }
    /*
        读取分页
    */
    static public function GetPage($status,$kw,$stime,$etime)
    {   
        $where=self::build_sql($status,$kw,$stime,$etime);
		$list=self::where(\implode("and",$where))->order("`posttime` DESC")->paginate(30, false, ['query' => request()->param()]);

        return ['list'=>$list,'pagelist'=>$list->render()];
    }
    /*
        导出Excel
    */
    static public function Export_Excel($status,$kw,$stime,$etime)
    {
        $where=self::build_sql($status,$kw,$stime,$etime);
        $list=self::where(\implode("and",$where))->order("`posttime` DESC")->select();
        
        $data[1][0]='申请者';
        $data[1][1]='提现银行';
        $data[1][2]='开户行';
        $data[1][3]='持卡人';
        $data[1][4]='提现账号';
        $data[1][5]='提现金额';
        $data[1][6]='提现时间';
        $data[1][7]='提现状态';
        
        $status=array(0=>'申请中',1=>'已处理',2=>'已拒绝');
        
		
        $i=2;
        foreach ($list as $k=>$v) {
            $data[$i][0]=$v['username'];
            $data[$i][1]=$v['bankname'];
            $data[$i][2]=$v['opening_bank'];
            $data[$i][3]=$v['cardholder'];
            $data[$i][4]=$v['cardno'];
            $data[$i][5]=$v['amount'];
            $data[$i][6]=MyDate('', $v['posttime']);
            $data[$i][7]=@$status[$v['status']];
            $i++;
        }
        
		include_once(APP_PATH.'/extend/excel/Excel.php');
		$filename=DS.'runtime'.DS.'temp_xls'.DS.'export_xls_'.time().'.xls';
		$path=ROOT_PATH.$filename;
        ArrayToXls($data, $path);
        Syslog::Rec(1,"导出提现订单为Excel",0);
        return $filename;
    }    
}