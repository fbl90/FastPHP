<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class GoodsComment extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';
    /*
        读取分页
    */
    static public function Page($pagesize,$kw)
    {   
        $where=[];
        if($kw!=""){
            $where[]="(`aid` like '%$kw%' OR `title` like '%$kw%')";
        }
        return self::where(\implode("and",$where))->order("orderid DESC")->paginate(10, false, ['query' => request()->param()]);
    }
}