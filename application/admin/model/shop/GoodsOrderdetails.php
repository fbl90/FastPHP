<?php
namespace app\admin\model\shop;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class GoodsOrderdetails extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';
}