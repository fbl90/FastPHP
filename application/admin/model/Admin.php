<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class Admin extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    static public function GetPage($AdminInfo, $kw='')
    {
        $where=array();
        if ($AdminInfo['type']==1 && $AdminInfo['username']!='developer') {
            $where[]="(`type`=0)";
        } elseif ($AdminInfo['type']==0) {
            return [];
        }
        if($kw!=""){
            $where[]="(`nickname` LIKE '%kw%' OR `username` LIKE '%kw%')";
        }
        return self::where(\implode('and',$where))->order('id DESC')->paginate(30, false, ['query' => request()->param()]);
    }
    /*
        创建管理员账号
    */
    static public function CreateUser($data)
    {
    	if($data['username']=="" OR $data['password']==""){
    		return \Result(1,'用户名和密码都不能为空');
        }
        unset($data['repassword']);
    	$count=self::where('username',$data['username'])->count();
    	if($count>=1){
    		return \Result(1,'此管理员已经存在了！');
        }
        $ip			=GetIP();
        $time       =time();
    	//创建一个安全码
    	$data['safecode']	    =GetRandStr(16);
    	$data['password']	    =Encrypt($data['safecode'],$data['password']);
    	$data['regtime']		=$time;
        $data['logintime']		=$time;
        $data['regip']          =$ip;
        $data['loginip']        =$ip;
        $data['acl']            ='index/index';
        $data['facepic']        =$data['facepic']??'';
        
      	if($data['facepic']==""){
            $data['facepic']='\static\admin\images\logo_sm.png';
        }
        if(self::create($data)){
            Syslog::Rec(1,"创建了管理员账号[".$data['username']."]",0);
            return \Result(0,'操作成功');
        }else{
            return \Result(1,'操作失败');
        }
    }
    /*
        更新管理员信息
    */
    static public function SaveInfo($data)
    {
        $data['facepic']=$data['faceupload_edit'];
        unset($data['faceupload_edit']);
    	if(self::update($data)){
            Syslog::Rec(1,"更新管理员资料[".$data['id']."]",0);
    		return \Result(0,'操作成功');
    	}else{
    		return \Result(1,'操作失败');
    	}        
    }
    /*
        更新设置管理员密码
    */
    static public function SetPassword($data)
    {
        $AdminInfo=self::get($data['id']);
        if(!$AdminInfo){
            return \Result(1,'管理员不存在！');
        }
        if($data['newpassword']!=$data['repassword']){
            return \Result(1,'两次输入的密码不一致');
        }
    	if(!CheckEncrypt($AdminInfo['safecode'],$data['oldpassword'],$AdminInfo['password'])){
            return \Result(1,'管理员旧密码错误！');
        }
    	//创建一个安全码
    	$safe_code	=GetRandStr(16);
    	$password	=Encrypt($safe_code,$data['newpassword']);
        $postdata=array('id'=>$data['id'],'safecode'=>$safe_code,'password'=>$password);
        
    	if(self::update($postdata)){   	
            Syslog::Rec(1,"修改了管理员密码[".$admininfo['username']."]",0);
            return \Result(0,'操作成功');
    	}else{
    		return \Result(1,'操作失败');
    	}
    }
    /*
        保存权限
    */
    static public function SavePrivilege($data)
    {
    	$uid=$data['admin_id'];
    	$acl=implode(',',array_filter($data['menu']));
        $cache_name='admin_menu_'.$uid;
        if(self::update(['id'=>$uid,'acl'=>$acl])){
            cache($cache_name,null);
            Syslog::Rec(1,"修改了管理员权限[".$uid."]",0);
            return \Result(0,'操作成功');
        }else{
            return \Result(1,'操作失败');
        }   
    }
}
