<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class AdClass extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';
    /*
        读取列表
    */
    static public function Page($pagesize,$kw='')
    {   
        $where=[];
        if($kw!=""){
            $where[]="(`classname` like '%$kw%')";
        }

        return self::where(\implode("and",$where))->order("posttime DESC")->paginate(10, false, ['query' => request()->param()]);
    }
    /*
        保存数据
    */
    static public function SaveData($data)
    {   
        $id=$data['id']??0;
        unset($data['id']);
        if($id>0){
            Syslog::Rec(1,"更新了广告分类:".$data['classname'],0);
            return self::where("id",$id)->update($data);
        }else{
            Syslog::Rec(1,"创建了广告分类:".$data['classname'],0);
            return self::create($data);
        }
    }
    /*
        批量操作(删除、启用、禁用、...)
    */
    static public function bat($data)
    {
        $type='bat_'.$data['type'];
        $ids=\implode(',',array_unique(array_filter(\explode(',',$data['ids']))));
        return self::$type($ids);
    }
    /*
        批量启用 status:0 启用 1:禁用
    */
    static public function bat_enable($ids)
    {
        Syslog::Rec(1,"批量启用了广告分类[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>0]);
    }
    /*
        批量禁用 status:0 启用 1:禁用
    */
    static public function bat_disable($ids)
    {
        Syslog::Rec(1,"批量禁用了广告分类[$ids]",0);
        return self::where("`id` IN ($ids)")->update(['status'=>1]);
    }
    /*
        批量删除
    */
    static public function bat_del($ids)
    {
        $AdClass=new AdClass;
        Syslog::Rec(1,"批量删除了广告分类[$ids]",0);
        return $AdClass->destroy($ids);
    }
}