<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\base\Tree;
use \app\admin\model\Cats;
use \app\admin\model\Syslog;

class Slider extends Model
{
    use SoftDelete;

    protected $deleteTime = 'deltime';

    static public function getClassListId()
    {
        $list=Cats::select();
        $slst=array();
        foreach($list as $v){
            $slst[$v['id']]=$v;
        }
        return $slst;
    }
    static public function GetPage()
    {
        
        $cats_list=self::getClassListId();
        $cats_tree=Tree::Create_Tree_Html($cats_list,'parent','classname',0);
        $list=self::order('orderid asc')->paginate(30, false, ['query' => request()->param()]);
        $slist=array();
       	foreach($list as $k=>$v){
            if(isset($cats_list[$v['classid']])){
                $classname[]=$cats_list[$v['classid']]['classname'];
            }else{
                $classname[]='默认分类';
            }

            $slist[$k]=$v;
            $slist[$k]['classname']=implode('/',$classname);
            // $classname=array();
        }
    
        return [
            'cats_tree'=>$cats_tree,
            'list'=>$slist,
            'pagelist'=>$list->render()
        ];
    }
    /*
        保存
    */
    static public function SaveData($data)
    {
        $data['picurl_pc']  = $data['picurl_pc']??'';
        $data['picurl_m']   = $data['picurl_m']??'';
        $data['posttime']   =time();
        
        if($data['id']>0){
            Syslog::Rec(1,"更新幻灯片".$data['title'],0);
            return self::update($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        }else{
            unset($data['id']);
            Syslog::Rec(1,"创建幻灯片".$data['title'],0);
            return self::create($data)?\Result(0,'操作成功'):Result(1,'操作失败');
        } 
    }
}