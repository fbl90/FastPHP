<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\base\DDL;
use \app\admin\model\ModelField;
use \app\admin\model\Syslog;

class Models extends Model
{
    use SoftDelete;
    protected $table="php_model";

    protected $deleteTime = 'deltime';
    /*
        保存数据
    */
    static public function SaveData($data)
    {
        $id=$data['id']??0;
        unset($data['id']);
        $DDL=new DDL;
        //重复性判断
        $count=self::where("(`model_name`='".$data['model_name']."' OR `model_table`='".$data['model_table']."')")->count();
        if($count>0){
            return Result(1,'存在重复的表名/模型名称');
        }
        if($id>0){
            $src=self::where('id',$id)->find()->toArray();
            if(self::where('id',$id)->update($data)){
                //判断数据表是否存在
                if (!$DDL->T_Table_Exists($data['model_table'])) {
                    //创建数据表
                    $DDL->T_Create_Table($data['model_table'], $data['model_engine'], $data['model_table_comment']);
                }
                $DDL->T_Update_Field($src['model_table'], 'mid', 'int', 10, '', '', 'id', '所属模型');
                //修改数据表的名称
                if ($src['model_table']!=$data['model_table']) {
                    $DDL->T_Table_Rename($src['model_table'],$data['model_table']);
                }
                //修改数据表的引擎
                if ($src['model_engine']!=$data['model_engine']) {
                    $DDL->T_Table_Set_Engine($data['model_table'],$data['model_engine']);
                }
                //修改数据表的备注
                if ($src['model_table_comment']!=$data['model_table_comment']) {
                    $DDL->T_Table_Set_Comment($data['model_table'],$data['model_table_comment']);
                }
                return \Result(0,'操作成功');
            }else{
                return \Result(1,'操作失败');
            }
        }else{
            if(self::create($data)){
                //生成模型文件
                self::CreateModelFile($data['model_table']);
                $DDL->T_Create_Table($data['model_table'], $data['model_engine'], $data['model_table_comment']);
                $DDL->T_Update_Field($data['model_table'], 'mid', 'int', 10, '', '', 'id', '所属模型');
                $DDL->T_Update_Field($data['model_table'], 'deltime', 'int', 10, '', '', 'mid', '删除时间');
                return \Result(0,'操作成功');
            }else{
                return \Result(1,'操作失败');
            }
        }
    }
    /*
        模型删除
    */
    static public function del($id)
    {
        $DDL=new DDL;
        $model=self::get($id)->toArray();
        
        if(self::destroy($id)){
            //删除数据表
            $DDL->T_Table_Drop($model['model_table']);
            //删除模型文件
            self::DeleteModelFile($model['model_table']);
            //删除字段表数据
            ModelField::destroy(['mid'=>$model['id']]);
            return true;
        }else{
            return false;
        }
    }
    /*
        创建模型必须字段
    */
    static public function BuildDefaultFields($model)
    {
        
    }
    /*
        动态生成模型文件
    */
    static public function CreateModelFile($modelName)
    {
        $ModelFile=PathDS2(APP_PATH.'admin'.DS.'model'.DS.ucfirst($modelName).'.php');
        if(\file_exists($ModelFile)){
            return false;
        }else{
$temp='<?php
namespace app\admin\model;
use think\Model;
use traits\model\SoftDelete;
class '.ucfirst($modelName).' extends Model
{
    use SoftDelete;
    protected $deleteTime = \'deltime\';
}';
            return \file_put_contents($ModelFile,$temp);
        }
    }
    /*
        删除模型文件
    */
    static public function DeleteModelFile($modelName)
    {
        $ModelFile=PathDS2(APP_PATH.'admin'.DS.'model'.DS.ucfirst($modelName).'.php');
        if(!file_exists($ModelFile)){
            return false;
        }
        return \unlink($ModelFile);
    }
}