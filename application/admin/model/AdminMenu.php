<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;
use \app\admin\model\Syslog;

class AdminMenu extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    static public function SaveData($data)
    {

        $id=$data['id'];
        if($id>=1){
        	$count=self::where('menu_pid',$data['menu_pid'])->where('id','<>',$id)->where('menu_name',$data['menu_name'])->count();	
        }else{
        	$count=self::where('menu_pid',$data['menu_pid'])->where('menu_name',$data['menu_name'])->count();	
        }
        if($count>=1){
            return \Result(1,'菜单已经存在了');
        }

        $pinfo=self::get($data['menu_pid']);
        $level      =1;
        $node_path  =",";
        if($pinfo){
            $node_path  =$pinfo['menu_path'];
            $node_path  .=$data['menu_pid'].",";
            $node_parent_name=$pinfo['menu_name'];
            $level++;
        }else{
            $node_path  .=$data['menu_pid'].",";
        }

        $data['menu_level']=$level;
        $data['menu_path']=$node_path;
        $data['posttime']=time();

        if($id>=1){
            Syslog::Rec(1,"更新了菜单[".$data['menu_name']."]",0);
            return self::update($data)?\Result(0,'操作成功'):\Result(1,'操作失败');
        }else{
            unset($data['id']);
            Syslog::Rec(1,"创建了新菜单[".$data['menu_name']."]",0);
            return self::create($data)?\Result(0,'操作成功'):\Result(1,'操作失败');
        }
    }
    /*
        排序
    */
    static public function Sort($data)
    {
        $AdminMenu=new AdminMenu;
        foreach($data['field_ids'] as $k=>$v){
            $list[]=['id'=>$v,'orderid'=>$data['orderid_ipt'][$k]];
        }
        Syslog::Rec(1,"更新了菜单排序",0);
        return $AdminMenu->isUpdate()->saveAll($list);
    }
}