<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class ConfigClass extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    //保存配置分类
    static function SaveData($data)
    {
        $count=self::where('group',$data['group'])->where('classid',$data['classid'])->count();
        if($count>=1){
            return Result(1,'分类标识已存在了');
        }
        if(self::create($data)){
            return Result(0,'操作成功');
        }else{
            return Result(1,'操作失败');
        }
    }    
}