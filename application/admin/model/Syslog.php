<?php
namespace app\admin\model;

use think\Model;
use app\admin\model\Config;

class Syslog extends Model
{
    /*
        日志记录操作
    */
    static public function Rec($type,$msg,$result=0,$nologin=false)
    {
        $admininfo=self::getAdminInfo();

        if($nologin==true){
            $uid=0;
            $uname='';
        }else{
            $uid=$admininfo['id'];
            $uname=$admininfo['username'];
        }
        $data=array(
            'uid'=>$uid,
            'uname'=>$uname,
            'type'=>$type,
            'msg'=>$msg,
            'result'=>$result,
            'ip'=>\GetIP(),
            'time'=>time()
        );
        if($admininfo['username']=='developer'){
            WLog('developer_log',array2string($data));
            return true;
        }
        if(self::create($data)){
            return true;
        }else{
            return false;
        }

    }
    //获取当前登录账号信息
    static public function getAdminInfo()
    {
		$adminfo_Session=session('adminfo');
    	$adminfo_Session=AuthCode($adminfo_Session,'DECODE',Config::getConfig('cookie_secretkey'));
        $admininfo=@unserialize($adminfo_Session);
        return $admininfo;
    }
}