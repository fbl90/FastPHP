<?php
namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Config extends Model
{
    use SoftDelete;
    protected $deleteTime = 'deltime';

    static public function getConfig($name)
    {
        $result=self::where('name',$name)->cache(3600)->find();
        return $result['value'];
    }
    
    //保存配置
    static public function SaveData($data)
    {   
        $id=$data['id']??0;
        unset($data['id']);
        if($id<1){
            $count=self::where('name',$data['name'])->count();
            if($count>=1){
                return Result(1,'配置已存在了');
            }
            return self::create($data);
        }else{        
            return self::update($data,['id'=>$id]);
            
        }
    }   
}