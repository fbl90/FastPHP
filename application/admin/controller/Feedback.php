<?php
/*
      用户反馈留言管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\Feedback as Model;
use \app\admin\model\Syslog;

class Feedback extends Base
{
    /*
        分页列表
    */
    public function index()
    {
        $list=Model::order('posttime DESC')->paginate(30, false, ['query' => request()->param()]);   
        $this->assign('page_title', '留言管理');
        $this->assign('list', $list);
        $this->assign('pagelist', $list->render());
        return $this->fetch();
    }
    /*
        获取详情
    */
    public function get_feedback_info()
    {
        $res=Model::get(input('id'));
        \ShowJson(0,'获取成功',$res);
    }
    /*
        删除
    */
    public function del()
    {
        $id=input('id')??0;
        if(Model::destroy($id)){
            \ShowJson(0,'操作成功');
        }else{
            \ShowJson(1,'操作失败');
        }
    }
    /*
        批量删除
    */
    public function bat_set()
    {
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }
}
