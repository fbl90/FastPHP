<?php
/*
      模型管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\Models as Models;
use \app\admin\model\Syslog;

class Model extends Base
{   
    public function _initialize()
    {
        //执行父类的构造方法
        parent::_initialize();
    }
    /*
       	读取所有模型分页数据
    */
    public function index()
    {
		$list=Models::select();		
        $this->assign('page_title', '模型管理');
        $this->assign('list', $list);
        return $this->fetch();
    }
    /*
        获取详情
    */
    public function get_model_info()
    {
        $id=input('id')??0;
        $result=Models::get($id);
        \ShowJson(0,'获取成功',$result);
    }
    /*
        保存模型
    */
    public function save_model()
    {
        $data=$this->Get_Post();
        $res=Models::SaveData($data);
        \ShowJson($res['status'],$res['msg']);
    }
    /*
        删除模型
    */
    public function del()
    {
        $id=input('id')??0;
        if(Models::del($id)){
            \ShowJson(0,'操作成功');
        }else{
            \ShowJson(1,'操作失败');
        }
    }
}
