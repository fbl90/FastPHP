<?php
namespace app\admin\controller;

/*
 *	后台登录控制器 
 * */

use \app\base\Common;
use \app\base\Image;
use \app\admin\model\Admin;
use \app\admin\model\Syslog;

class Login extends AdminCommon
{
	
	//登录后存放管理员基本信息
	public $AdminInfo;
	
    public function _initialize()
    {
    	//执行父类的构造方法
    	parent::_initialize();
    	//判读是否登录
		$this->AdminInfo=$this->CheckLogin();
		$this->assign('BindRoot','/admin.php/');
    	$this->assign('Config',$this->Config);
    }
	
    public function index()
    {
    	//判读是否登录
    	if(is_array($this->AdminInfo)){
 			if($this->AdminInfo['lockscreen']=='true'){
 				$this->redirect(url('login/LockScreen'));
 			}else{
	    		$this->redirect(url('index/index'));
 			}
    	}else{
		  $safe=input('safe');
		  if($this->Config['loginsafecodesw']==1 && $safe!=$this->Config['loginsafecode']){
			 $this->error('当前环境不是安全的管理环境！');
			 return false;
		  }
		}
    	$this->assign('SiteLogo',$this->Config['adminlogo']);
        return $this->fetch();
    }
    
    /*
    	显示验证码 
    */
   	public function showcheckcode()
   	{
   		$checkcode=mt_rand(1000,9999);
   		session('checkcode',$checkcode);
   		Image::getAuthImage($checkcode);
   	}
	/*
	 *	登录操作
	 */
	public function Login()
	{
	 	$this->Admin_Login();
	}
}
