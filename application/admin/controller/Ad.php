<?php
/*
	广告管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\Ad as Model;
use \app\admin\model\AdClass as ClassModel;
use \app\admin\model\Syslog;

class Ad extends Base
{   
	/*
		广告首页
	*/
    public function index()
    {
		$kw=input('kw')??'';
		$list=Model::Page(20,$kw);
        $this->assign('list' ,$list);
        $this->assign('pagelist',$list->render());
        $this->assign('page_title' ,'广告管理');
        return $this->fetch();
    }
	/*
		加载广告发布页面
	*/
    public function publish()
    {
    	$act=input('act')??'';
    	$id=input('id')??0;
    	$this->assign('id',$id);
    	$this->assign('act',$act);
		$this->assign('orderid',Model::count()+1);
		$this->assign('classlist',ClassModel::all());
    	$this->assign('page_title','发布新广告');
    	return $this->fetch();
    }
	/*
		获得广告数据
	*/
    public function get_ad_info()
    {
		$id=input('id')??0;
    	$result=Model::get($id)->toArray();
    	\ShowJson(0,'获取成功',$result);
    }
	/*
		保存广告信息
	*/
    public function save_ad()
    {
		$post_d=$this->Get_Post();
		$result=Model::SaveData($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		删除数据
	*/
	public function del()
	{
		$id=input('id')??0;
		$result=Model::destroy($id);
		if($result){
			Syslog::Rec(1,"删除[$id]广告",0);
			\ShowJson(0,'删除成功');
		}else{
			\ShowJson(1,'删除失败');
		}
	}
	/*
		广告排序
	*/
	public function update_sort()
	{
		$post_d=$this->Get_Post();
		$result=Model::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		批量设置
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			Syslog::Rec(1,"设置广告[$id]",0);
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
}
