<?php
namespace app\admin\controller;

/*
订单管理控制器
*/

use \app\admin\model\shop\GoodsOrder;
use \app\admin\model\shop\GoodsOrderdetails as Orderdetails;
use \app\admin\model\shop\Express;
use \app\admin\model\Syslog;

class Order extends Base
{
	//加载配置主页
	public function config()
	{
		$list=$this->Get_ConfigClass('order');
		foreach ($list as $k=>$v){
			$list[$k]['nodelist']=$this->Get_ConfigData($v['classid'], 'order');
		}
		if (is_numeric(input('cid'))) {
			$cid=input('cid');
		} else {
			$cid=@$list[0]['id'];
		}
		$lastid=count($list)+1;
		$this->assign('cid', $cid);
		$this->assign('config_class_list', $list);
		$this->assign('config_class_num', $lastid);
		$this->assign('page_title', '商城系统设置');
		return $this->fetch();
	}	
	//订单管理页面初始化
	public function index()
	{
		$status=input('status')??0;
		$kw=input('kw')??'';
		$stime=input('stime');
		$etime=input('etime');

		if (is_null($stime)) {
			$stime=time()-604800;
		} else {
			$stime=GetMkTime($stime);
		}
		
		if (is_null($etime)) {
			$etime=time();
		} else {
			$etime=GetMkTime($etime);
		}
	
		$express_list=Express::select();
		
		$res=GoodsOrder::GetPage($status, $kw, $stime, $etime);

		$stime=MyDate('Y-m-d H:i:s', $stime);
		$etime=MyDate('Y-m-d H:i:s', $etime);
		
		$this->assign('stime', $stime);
		$this->assign('etime', $etime);
		$this->assign('kw', $kw);
		$this->assign('status', $status);
		$this->assign('express_list', $express_list);
		$this->assign('list', $res['list']);
		$this->assign('pagelist', $res['pagelist']);
		$this->assign('page_title', '订单管理');
		return $this->fetch();
	}

	//订单导出xls
	public function export_xls()
	{
		$status=input('status')??0;
		$kw=input('kw')??'';
		$stime=input('stime');
		$etime=input('etime');

		if (is_null($stime)) {
			$stime=time()-604800;
		} else {
			$stime=GetMkTime($stime);
		}
		if (is_null($etime)) {
			$etime=time();
		} else {
			$etime=GetMkTime($etime);
		}
		
		$filename=GoodsOrder::Export_Excel($status, $kw, $stime, $etime);
		ShowJson(0, '导出成功', $filename);
	}
	//批量发货xls 上传
	public function order_bat_send()
	{
		$xlsfile=input('xlsfile');
		$xlsfile=ROOT_PATH.DS.$xlsfile;
		if (!file_exists($xlsfile) || is_dir($xlsfile)) {
			ShowJson(1, '请先上传Excel文件');
		}
		include_once(APP_PATH.'/extend/excel/Excel.php');
		$list=XlsToArray($xlsfile, 2);
		foreach ($list as $k=>$v) {
			$list[$k]['order']=GoodsOrder::where('oid', $v['A'])->field('oid,status,uid,nickname,posttime')->find();
		}
		$TempCache_File=RUNTIME_PATH.DS.'temp_xls'.DS.time().'.cache';
		session('order_batsend_cache', $TempCache_File);
		file_put_contents($TempCache_File, serialize($list));
		Syslog::Rec(3,"上传批量发货EXCEL",0);
		ShowJson(0,'导入成功', $list);
	}

	//批量发货开始
	public function batorder_sends()
	{
		$file=session('order_batsend_cache');
		if (!file_exists($file)){
			ShowJson(1, '请先上传EXCEL文件');
		}
		$data=unserialize(file_get_contents($file));
		$list=array();
		foreach ($data as $k=>$v) {
			$order=GoodsOrder::where('oid', $v['A'])->where('status', 2)->field('oid,status,uid,uname,posttime')->find();
			if ($order && $v['J']!="" && $v['K']!="" ) {
				GoodsOrder::where('oid', $v['A'])->update(['express_name'=>$v['J'],'express_id'=>$v['K'],'status'=>3]);
				$list[]=trim($v['A']);
			}
		}
		Syslog::Rec(3,"批量发货成功",0);
		ShowJson(0, '发货成功!', $list);
	}
	//获得订单基本信息
	public function get_order_info()
	{
		$res=GoodsOrder::get(input('id'));
		\ShowJson(0,'获取成功',$res);
	}
	//更新订单发货
	public function update_express()
	{
		$data=$this->Get_Post();
		$data['status']=3;
		if (GoodsOrder::update($data)) {
			Syslog::Rec(3,"订单发货成功",0);
			ShowJson(0, '操作成功');
		} else {
			ShowJson(1, '操作失败');
		}
	}
	//更新订单总价
	public function update_amount()
	{
		$id=input('id');
		$amount=input('amount');
		if (GoodsOrder::where('id', $id)->update(['pay_amount'=>$amount])) {
			Syslog::Rec(3,"订单修改了价格[".$id."]:".$amount,0);
			ShowJson(0, '更新成功');
		} else {
			ShowJson(1, '更新失败');
		}
	}
	//删除订单
	public function del()
	{
		$id=input('id');
		$order=GoodsOrder::get($id);

		if(GoodsOrder::destroy($id)){
			Syslog::Rec(3,"删除订单".$id,0);
			Orderdetails::destroy(['oid'=>$order['oid']]);
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
}
