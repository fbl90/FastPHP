<?php
namespace app\admin\controller;

/*
 	通用文档信息管理控制器
 */

use \app\admin\model\Document as Model;
use \app\admin\model\Cats;
use \app\base\Tree;
use \app\admin\model\Syslog;

class Document extends Base
{
	public $ClassList=array();
	public function _initialize(){
    	//执行父类的构造方法
    	parent::_initialize();
    	$List=db('cats')->select();
    	foreach($List as $k=>$v){
    		$this->ClassList[$v['id']]=$v;
    	}
	}
    //栏目列表页面初始化
    public function index()
    {
    	$cid=input('cid')??0;
    	$kw=input('kw')??'';
    	$kw_field=input('kw_field')??'';
    	if($cid<=0){
    		//没有找到分类，加载分类选择模板
			$result=Cats::TreeList();
	        $this->assign('list',$result['list']);
	        $this->assign('page_title' ,'选择发布分类');
	        return $this->fetch('select_class');
    	}else{
			$res=Model::DocPage($cid,$kw,$kw_field);
			if($res['status']!=0){
				J(url('document/index'),$res['msg']);
			}

    		$this->assign('kw',$res['data']['kw']);
	        $this->assign('search_names',$res['data']['search_names']);
	        $this->assign('search_fields',$res['data']['search_fields']);
	        $this->assign('fields_title',$res['data']['fields_title']);
	        $this->assign('list',$res['data']['list']);
	        $this->assign('pagelist',$res['data']['pagelist']);
	        $this->assign('page_classid' ,$res['data']['page_classid']);
	        $this->assign('page_classname' ,$res['data']['page_classname']);
	        $this->assign('page_title' ,$res['data']['page_title']);
	        $this->assign('model_name' ,'document');

	        return $this->fetch();
    	}
    }
    //编辑页面
    public function publish(){
    	
    	$id=input('id')??0;
    	$cid=input('cid')??0;
    	$act=input('act')??'';
		
    	if($cid<=0){
    		//没有找到分类，加载分类选择模板
			$result=Cats::TreeList();
	        $this->assign('list',$result['list']);
	        $this->assign('page_title' ,'选择发布分类');
	        return $this->fetch('select_class');
    	}else{
			//构建编辑表单数据
			$res=Model::build_form($cid,$act,$id);
			if($res['status']!=0){
				J(url('document/index'),$res['msg']);
			}

			$this->assign('model_name' ,'document');
			$this->assign('model_info' ,$res['data']['model_info']);
	        $this->assign('model_id' ,$res['data']['model_id']);
	        $this->assign('fields_types' ,$res['data']['fields_types']);
	        $this->assign('fields' ,$res['data']['fields']);
	        $this->assign('page_title' ,$res['data']['page_title']);
	        $this->assign('doc_id' ,$res['data']['doc_id']);
	        $this->assign('act' ,$act);
	        
	        return $this->fetch();
    	}
	}
	//排序
	public function update_sort()
	{
		$data=$this->Get_Post();

		$res=Model::Sort($data);

		\ShowJson($res['status'],$res['msg']);
	}
    //发布文档
    function doc_save(){
		$data=$this->Get_Post();
		
		if(Model::SaveData($data)){
			ShowJson(0,'保存成功');
		}else{
			ShowJson(1,'保存失败');
		}
  	}
	/*
  		通用批量操作
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$data['excid']=input('excid')??0;
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		删除操作
	*/
	public function del()
	{
		$excid=input('excid');
		$id=input('id');
		$obj=Model::newObj($excid);
		if($obj::destroy($id)){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		Word转html，需要在windows/linux上安装liboffice
	*/
	public function WordToHtml()
	{
		$path=input('path');
		
		if($this->Config['swword2html']==2){
			ShowJson(1,'Office转Html功能已经关闭！');
		}
		
		if(!file_exists($this->Config['sofficepath'])){
			unlink(PathDS(ROOT_PATH.$path));
			ShowJson(1,'导入失败[LibreOffice]没有安装!');
			return false;
		}
		
		if(PHP_OS=='Linux'){
        	$command="sudo ".$this->Config['sofficepath'].' --invisible --convert-to html --outdir '.$this->Config['WordHtmlDir'].' '.ROOT_PATH.$path;
        }else{
        	$command='"'.$this->Config['sofficepath'].'" --convert-to html --outdir "'.$this->Config['WordHtmlDir'].'" '.ROOT_PATH.$path;
		}
		
		$file=pathinfo(ROOT_PATH.$path);
		$filename=$this->Config['WordHtmlDir'].DS.$file['filename'].'.html';
		$log=shell_exec($command);
		WLog('libreoffice_log',$log.':'.$command);
		$content=file_get_contents($filename);
		$preg="#<body(.*)</body>#iUs";
		preg_match($preg,$content,$result);
		//字符集转换
		$result=Auto_Charset($result[0]);
		$result=preg_replace("/<(\/?body.*?)>/si","",$result); //过滤body标签
		
		//过滤出所有图片并移动到upload目录
		$preg="#src=\"(.*)\"#iUs";
		preg_match_all($preg,$result,$pic_list);
		if(isset($pic_list[1]) and is_array($pic_list[1])){
			foreach($pic_list[1] as $k=>$v){
				$imginfo=pathinfo($v);
				$imgfile=$this->Config['WordHtmlDir'].DS.$v;
				
				$dir=DS.'uploads'.DS.'images'.DS.MyDate('Ymd',time()).DS;
				$newsdir=ROOT_PATH.$dir;
				
				$newsfile=time().mt_rand(100000,999999).'.'.$imginfo['extension'];
				if(!file_exists($newsdir)){
					mkdir($newsdir);
				}
				
				@rename($imgfile,$newsdir.$newsfile);
				//转换分隔
				$img_uri=str_replace("\\","/",$dir.$newsfile);
				$result=str_replace($v,$img_uri,$result);
			}
		}
		//清空缓存输出目录
		DelFile($this->Config['WordHtmlDir'],false);
		unlink(PathDS(ROOT_PATH.$path));
		//清空缓存
		ShowJson(0,'处理成功！',$result);
	}
}
