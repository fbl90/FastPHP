<?php
/*
      说明书控制器
*/
namespace app\admin\controller;

use \app\admin\model\Manual as Model;
use \app\admin\model\Syslog;

class Manual extends Base
{
	/*
		分页列表
	*/
    public function index()
    {
        $list=Model::order('id asc')->paginate(30, false, ['query' => request()->param()]);
        $this->assign('page_title', '说明书管理');
        $this->assign('list', $list);
        $this->assign('pagelist', $list->render());
        return $this->fetch();
    }
	/*
		发布、修改页面
	*/
    public function publish()
    {
        $id=input('id');
        $act=input('act');
        if ($act=='edit') {
            $this->assign('doc_id', $id);
            $this->assign('page_title', '修改说明书');
        } else {
            $this->assign('doc_id', 0);
            $this->assign('page_title', '发布说明书');
        }
        $this->assign('act', $act);
        $this->assign('page_title', '发布网站说明书');
        return $this->fetch();
    }
	/*
		查看详情
	*/
    public function show()
    {
        $result=Model::get(input('id'));
        $this->assign('info', $result);
        $this->assign('page_title', '说明书详情');
        return $this->fetch();
    }
	/*
		获取详情
	*/
    public function get_manual_info()
    {
        $result=Model::get(input('id'));
        \ShowJson(0,'获取成功');
    }
	/*
		保存
	*/
    public function save_manual()
    {
       	$data=$this->Get_Post();
        
		$data['author']=$this->AdminInfo['nickname'];
		$data['posttime']=time();

		$id=$data['id']??0;
		
		if($id>0){
			return Model::update($data)?\Result(0,'操作成功'):Result(1,'操作失败');
		}else{
			unset($data['id']);
			return Model::create($data)?\Result(0,'操作成功'):Result(1,'操作失败');;
		}
    }
    /*
     	删除
     */
    public function del()
    {
		if(Model::destroy(input('id'))){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }
}
