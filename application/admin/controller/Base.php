<?php
namespace app\admin\controller;
/*
 * 管理中心基础控制器 
 */
use \app\extend\upload\Uploader;
use \app\admin\model\{AdminMenu,Config,ConfigClass,Syslog};

class Base extends AdminCommon
{
	//登录后存放管理员基本信息
	public $AdminInfo;
	//登录后获得管理员的权限列表
	public $Admin_ACL;
	
	public $MC=null;

    public function _initialize()
    {
    	//执行父类的构造方法
    	parent::_initialize();
    	
    	//判读是否登录
    	$this->AdminInfo=$this->CheckLogin();
    	
    	if(!$this->AdminInfo){
    		$this->redirect(url('login/index'));
    		exit();
    	}
    	
    	$this->Admin_ACL=explode(',',$this->AdminInfo['acl']);
        /*
         * 读取管理菜单数据
         * 读取所有权限节点分页数据
         */
        $cache_name='admin_menu_'.$this->AdminInfo['id'];
        $tree=array();
        //读取缓存
        $AdminMenu=cache($cache_name);
        if($AdminMenu==""){
			//读取一级菜单
	        $list=AdminMenu::where('menu_pid',0)->where('isshow','1')->order('orderid asc')->select();
	        foreach($list as $v){
	        	//开发者跳过权限设置
	            if($this->AdminInfo['username']=='developer'){
		            $slist=AdminMenu::where('isshow','1')->where('menu_pid',$v['id'])->order('orderid asc')->select();
		            $v['menu_list']=$slist;
		            $tree[]=$v;
			         
	            }else if($v['menu_url']!='model/index' and $v['menu_url']!='menu/index'){
	            	//非开发者禁用模型管理和后台菜单管理
		        	if(in_array($v['menu_url'],$this->Admin_ACL)){
			            $slist=AdminMenu::where('isshow','1')->where('menu_pid',$v['id'])->order('orderid asc')->select();
			            $sslst=array();
			            foreach($slist as $sv){
			            	if(in_array($sv['menu_url'],$this->Admin_ACL)){
			            		$sslst[]=$sv;
			            	}
			            }
			            $v['menu_list']=$sslst;
			            $tree[]=$v;
		        	}
	            }
	        }
	        cache($cache_name,serialize($tree));
        }else{
        	$tree=unserialize($AdminMenu);
        }
        
        $action=$this->Controller.'/'.$this->Method;
        $Menu_List=$this->Get_Admin_Menus();
        //管理操作权限设置
        //开发者管理跳过权限验证
        if($this->AdminInfo['username']!='developer'){
        	//非开发者不能操作的方法
	   		$sys_list=array(
	   			1=>'model/index',
	   			2=>'model/field_index',
	   			3=>'menu/index',
	   			4=>'manual/publish'
	   		);
	   		
	   		if(in_array($action,$sys_list)){
				$this->error('您无权操作此功能！');
	        	exit();
	   		}
	        //判断是否在权限范围内
	        if(in_array($action,$Menu_List)){
	        	//判断当前管理员账户是否有权限操作当前动作
		        if(!in_array($action,$this->Admin_ACL)){
					$this->error('您无权操作此功能 ！');
		        	exit();
		        }
	        }
		}

		$this->assign('BindRoot','/admin.php/');
		$this->assign('Controller',$this->Controller);
		$this->assign('Method',$this->Method);
        $this->assign('Config',$this->Config);
        $this->assign('AdminInfo',$this->AdminInfo);
        $this->assign('menu_list',$tree);
        $this->assign('menu_list_count',count($tree));
        $this->assign('menu_action',$action);
    }
    /*
     	加载默认首页
     */
    public function index()
    {
        return $this->fetch();
    }
	/*
	 	错误提示页面
	 */
	public function show_msg(){
		
		$this->assign('page_title' ,'操作提示');
		return $this->fetch();
		
	}
	/*
		后台菜单搜索 
	*/
	public function menu_search(){
		$kw=trim(input('kw'));
		if($kw==""){
			$this->redirect('index');
		}else{
			$result=db('admin_menu')->where("`menu_name` like '%$kw%' OR `menu_url` like '%$kw%'")->find();
			if(is_array($result)){
				$this->redirect($result['menu_url']);
			}else{
				$this->redirect('index');
			}
		}
	}
	/*
	 	清空缓存
	*/
    public function CacheClear()
    {
		Syslog::Rec(1,'清空缓存！',0);
    	$this->Clear_All_cache();
    	ShowJson(0,'已经完全清空缓存！');
    }
	/*
		通用文件上传
	*/
    public function file_upload()
    {
        $upload=new \app\base\Upload;
		$result=$upload->upload_do();
    	echo json_encode($result);
    }
    /*
     	获得配置分类
     */
    public function Get_ConfigClass($group='system')
    {
    	if($this->AdminInfo['username']=='developer'){
    		$list=ConfigClass::where('group',$group)->order('orderid ASC')->cache(true,3600)->select();
    	}else{
    		$list=ConfigClass::where('group',$group)->where('isdeveloper',0)->order('orderid ASC')->cache(true,3600)->select();
    	}
    	return $list;
    }
    
    /*
     	读取指定分组分类中所有配置数据
    */
   	public function Get_ConfigData($class,$group='system')
   	{
   		//定义带选择的配置项目类型
    	$datalist=array('radio','checkbox','list');
		
		if($this->AdminInfo['username']=='developer'){
			$node=Config::where('group',$group)->where('classid',$class)->order('orderid','ASC')->select();
		}else{
			$node=Config::where('group',$group)->where('classid',$class)->where('isdeveloper',0)->order('orderid','ASC')->select();
		}
		//开始解析配置项目中带选择的项目
		foreach($node as $sk=>$sv){
			if(in_array($sv['type'],$datalist)){
				$data1=explode("\n",$sv['data']);
				$d2=array();
				foreach($data1 as $key=>$val){
					$dd=explode(":",$val);
					if(isset($dd[0])and(isset($dd[1]))){
						$kk=trim($dd[1]);
						$vv=trim($dd[0]);
						$d2[$key]['key']=$kk;
						$d2[$key]['value']=$vv;
						$d2[$key]['ischecked']="";
						
						if($sv['value']==''){
							$sv['value']=$sv['defaultvalue'];
						}
						if($sv['type']=='radio' and $sv['value']==$kk){
							$d2[$key]['ischecked']="checked";
						}else{
							$d3=ClearNullArray(explode(',',$sv['value']));
							if(in_array($kk,$d3)){
								$d2[$key]['ischecked']="checked";
							}
						}
					}
				}
				$node[$sk]['datanode']=$d2;
			}
		}
   		return $node;
   	}
   	/*
   	 	获得所有预设管理权限菜单-tree版本
   	*/
   	public function Get_Admin_Menu_List()
   	{
   		$table="admin_menu";
        $list=AdminMenu::where('menu_pid',0)->order('orderid asc')->cache(true,3600)->select();
        foreach($list as $v){
            $slist=AdminMenu::where('menu_pid',$v['id'])->order('id asc')->cache(true,3600)->select();
            $v['menu_list']=$slist;
            $tree[]=$v;
        }
        return $tree;
   	}
   	/*
   	 	获得所有预设管理权限菜单，非tree版本-非详细信息版，只有URL列表
   	*/
   	public function Get_Admin_Menus()
   	{
		$lst=AdminMenu::field('menu_url')->cache(true,3600)->select();
		foreach($lst as $v){
			$list[]=$v['menu_url'];
		}
   		return $list;
	}
}
