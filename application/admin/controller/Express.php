<?php
namespace app\admin\controller;

/*
    订单管理控制器
*/

use \app\admin\model\shop\Express as Model;
use \app\admin\model\Syslog;

class Express extends Base
{
	/*
		物流公司管理
	*/
	public function index()
	{
		$list=Model::paginate(30);
		
		$this->assign('list', $list);
		$this->assign('pagelist', $list->render());
		$this->assign('page_title', '物流公司管理');
		return $this->fetch();
	}
	/*
		获得物流公司信息
	*/
	public function get_express()
	{
        $res=Model::get(input('id'));
        \ShowJson(0,'获取成功',$res);
    }
    /*
        删除订单
    */
	public function del()
	{
		$id=input('id');
		if(Model::destroy($id)){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}    
	/*
		保存公司信息
	*/
	public function save_express()
	{
        $data=$this->Get_Post();
        
        $id=$data['id']??0;

        $data['logo']=$data['logo']??'';
        $data['posttime']=time();

        if($id>0){
            $res=Model::update($data);
        }else{
            unset($data['id']);
            $res=Model::create($data);
        }
        if($res){
            \ShowJson(0,'操作成功');
        }else{
            \ShowJson(1,'操作失败');
        }
	}
}
