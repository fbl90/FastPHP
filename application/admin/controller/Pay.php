<?php
namespace app\admin\controller;

/*
    支付管理控制器
*/

use \app\admin\model\shop\PayRecord as Model;
use \app\admin\model\shop\MemberDrawcash;
use \app\admin\model\Member;
use \app\admin\model\Syslog;

class Pay extends Base
{
    //加载配置主页
    public function config()
    {
        $list=$this->Get_ConfigClass('pay');
        foreach ($list as $k=>$v) {
            $list[$k]['nodelist']=$this->Get_ConfigData($v['classid'], 'pay');
        }
        if (is_numeric(input('cid'))) {
            $cid=input('cid');
        } else {
            $cid=@$list[0]['id'];
        }
        
        $lastid=count($list)+1;
        $this->assign('cid', $cid);
        $this->assign('config_class_list', $list);
        $this->assign('config_class_num', $lastid);
        $this->assign('page_title', '支付系统设置');
        return $this->fetch();
    }
    
    //支付管理页面初始化
    public function index()
    {
        $type=input('type')??0;
        $kw=input('kw')??'';
        $status=input('status')??'';
        
        $stime=input('stime');
        $etime=input('etime');
        
        if (is_null($stime)) {
            $stime=time()-604800;
        } else {
            $stime=GetMkTime($stime);
        }
        
        if (is_null($etime)) {
            $etime=time();
        } else {
            $etime=GetMkTime($etime);
        }
        $result=Model::GetPage($type, $status, $kw, $stime, $etime);
        $stime=MyDate('Y-m-d H:i:s', $stime);
        $etime=MyDate('Y-m-d H:i:s', $etime);
        $this->assign('stime', $stime);
        $this->assign('etime', $etime);
        $this->assign('kw', $kw);
        $this->assign('status', $status);
        $this->assign('type', $type);
        $this->assign('list', $result['list']);
        $this->assign('pagelist', $result['pagelist']);
        $this->assign('page_title', '支付流水');
        return $this->fetch();
    }
    

    //导出xls
    public function export_xls()
    {
        $status=input('status');
        $kw=input('kw');
        $type=input('type');
        
        $stime=input('stime');
        $etime=input('etime');
        
        if (is_null($stime)) {
            $stime=time()-604800;
        } else {
            $stime=GetMkTime($stime);
        }
        
        if (is_null($etime)) {
            $etime=time();
        } else {
            $etime=GetMkTime($etime);
        }
        $filename=Model::Export_Excel($type, $status, $kw, $stime, $etime);
        ShowJson(0, '导出成功', $filename);
    }
    
    //加载提现申请
    public function drawcash()
    {
        $status=input('status')??0;
        $kw=input('kw')??'';
        
        $stime=input('stime');
        $etime=input('etime');
        
        if (is_null($stime)) {
            $stime=time()-(86400*30);
        } else {
            $stime=GetMkTime($stime);
        }
        
        if (is_null($etime)) {
            $etime=time();
        } else {
            $etime=GetMkTime($etime);
        }
        
		$result=MemberDrawcash::GetPage($status, $kw, $stime, $etime);
        
        $stime=MyDate('Y-m-d H:i:s', $stime);
        $etime=MyDate('Y-m-d H:i:s', $etime);
        
        $this->assign('stime', $stime);
        $this->assign('etime', $etime);
        
        $this->assign('kw', $kw);
        $this->assign('status', $status);
        $this->assign('list',$result['list']);
        $this->assign('pagelist', $result['pagelist']);
        $this->assign('page_title', '会员提现管理');
        return $this->fetch();
    }
    public function drawcash_export_xls()
    {
        $status=input('status');
        $kw=input('kw');
        
        $stime=input('stime');
        $etime=input('etime');
        
        if (is_null($stime)) {
            $stime=time()-(86400*30);
        } else {
            $stime=GetMkTime($stime);
        }
        
        if (is_null($etime)) {
            $etime=time();
        } else {
            $etime=GetMkTime($etime);
        }
        
        $filename=MemberDrawcash::Export_Excel($status, $kw, $stime, $etime);
        ShowJson(0, '导出成功', $filename);
    }
    //通过提现申请
    public function drawcash_allow()
    {
        $id=input('id');
        $record=MemberDrawcash::where('id', $id)->where('status', 0)->find();
        if ($record) {
            $data=array('status'=>1,'retime'=>time());
            MemberDrawcash::where('id', $id)->update($data);
            Syslog::Rec(4,"通过提现申请[$id]",0);
            ShowJson(0, '处理成功');
        } else {
            ShowJson(1, '处理失败');
        }
    }
    //拒绝提现申请
    public function drawcash_cancel()
    {
        $id=input('id');
        $record=MemberDrawcash::where('id', $id)->where('status', 0)->find();
        if ($record) {
            $data=array('status'=>2);
            $User=Member::get($record['uid']);
            $data=array('balance'=>$User['balance']+$record['amount']);
            Member::where('id', $record['uid'])->update($data);
            MemberDrawcash::where('id', $id)->update(['status'=>2]);
            Syslog::Rec(4,"拒绝提现申请[$id]",0);
            ShowJson(0, '取消成功');
        } else {
            ShowJson(1, '取消失败');
        }
    }
}
