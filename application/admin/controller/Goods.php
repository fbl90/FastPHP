<?php
namespace app\admin\controller;
/*
	商品管理控制器
*/
use \app\admin\model\shop\Goods as Model;
use \app\admin\model\shop\GoodsClass;
use \app\admin\model\shop\GoodsFlag;
use \app\base\Tree;
use \app\admin\model\Syslog;

class Goods extends Base
{
    public function _initialize()
    {  
    	//执行父类的构造方法
    	parent::_initialize();
    	
    	//获得产品基础数据
    	$flag_list=GoodsFlag::select();
		$class_list=GoodsClass::GetGoodsClassListId();
		$class_tree=Tree::Create_Tree_Html($class_list,'parent','classname',0);
		
    	$this->assign('cats_tree',$class_tree);
    	$this->assign('cats_list',$class_list);
		$this->assign('flag_list',$flag_list);
		
    }
    //产品栏目列表页面初始化
    public function index()
    {
		$cid=input('cid')??0;
		$kw=input('kw')??'';
		$res=Model::GetPage($cid,$kw);
        $this->assign('list',$res['list']);
        $this->assign('pagelist',$res['pagelist']);
        $this->assign('page_title' ,'产品管理');
        return $this->fetch();
    }
	
	//加载配置管理主页
	public function config()
	{
    	$list=$this->Get_ConfigClass('shop');
		foreach($list as $k=>$v){
			$list[$k]['nodelist']=$this->Get_ConfigData($v['classid'],'shop');
		}
    	if(is_numeric(input('cid'))){
    		$cid=input('cid');
    	}else{
    		$cid=@$list[0]['id'];
    	}
    	$lastid=count($list)+1;
    	$this->assign('cid',$cid);
    	$this->assign('config_class_list',$list);
    	$this->assign('config_class_num',$lastid);
        $this->assign('page_title' ,'商城系统设置');
        return $this->fetch();
	}
	
	//加载商品发布页面
	public function publish()
	{
    	$id=input('id')??0;
    	$act=input('act')??'';
    	$cid=input('cid')??0;
    	
		if($id>=1){
			$goods=Model::get($id);
			if(!$goods){
				J(url('goods/index'),'没找到产品');
				exit();
			}
			$this->assign('goods' ,$goods);
			$this->assign('orderid' ,$goods['orderid']);
			$this->assign('goods_id' ,$id);
			$this->assign('page_title' ,'修改产品');
		}else{
			$orderid=Model::count();
			$this->assign('orderid' ,$orderid+1);
			$this->assign('goods_id' ,'');
			$this->assign('page_title' ,'发布新产品');
		}
		
	    $this->assign('posttime' ,MyDate('',time()));
	    $this->assign('cid' ,$cid);
	    $this->assign('act' ,$act);
	    return $this->fetch();
	}
	//保存商品
	public function save_goods()
	{
		$req=$this->Get_Post();
		$res=Model::SaveData($req);
		\ShowJson($res['status'],$res['msg']);
	}
	/*
	 	获得产品信息
	 */
	public function get_goods_info(){
		$result=Model::get(input('id'));
		\ShowJson(0,'获取成功',$result);
	}
	/*
		删除数据
	*/
	public function del()
	{
		$id=input('id')??0;
		$result=Model::destroy($id);
		if($result){
			\ShowJson(0,'删除成功');
		}else{
			\ShowJson(1,'删除失败');
		}
	}
	/*
		广告排序
	*/
	public function update_sort()
	{
		$post_d=$this->Get_Post();
		$result=Model::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		批量设置
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}	
    /*
       	获得产品标志分页列表
    */
    public function get_flaglist()
    {
        $list=GoodsFlag::order('id desc')->select();
        \ShowJson(0,'获取成功',$list);
    }
    /*
     	创建一个新产品标志
     */
    public function add_flag()
    {
    	$data=$this->Get_Post();
    	$count=GoodsFlag::where('flag',$data['flag'])->count();
    	if($count>=1){
    		ShowJson(1,'标志已经存在了');
    	}
		GoodsFlag::create($data)?\ShowJson(0,'操作成功'):ShowJson(1,'操作失败');
    }
    /*
     	删除标志
     */
    public function del_goodsflag()
    {
    	if(GoodsFlag::destroy(input('id'))){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }
    /*
     	获得产品标志列表
     */
    public function Get_GoodsFlagList()
    {
    	$list=GoodsFlag::select();
    	return $list;
    }
}
