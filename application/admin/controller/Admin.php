<?php
namespace app\admin\controller;

use \app\admin\model\Admin as Model;
use \app\admin\model\Syslog;

class Admin extends Base
{
    //管理员列表页面初始化
    public function index()
    {
		$kw=input('kw')??'';
		$AdminInfo=$this->AdminInfo;

		if($AdminInfo['type']==0){
        	$this->error('普通管理无权限设置管理员');
		}

		$list=Model::GetPage($AdminInfo,$kw);

        $this->assign('list',$list);
        $this->assign('pagelist',$list->render());
        $this->assign('page_title' ,'管理员管理');
        return $this->fetch();
        
    }
    //创建管理员
    public function add_admin()
    {
		$data=$this->Get_Post();
		$res=Model::CreateUser($data);
		\ShowJson($res['status'],$res['msg']);
    }
   	//修改管理员信息
   	public function update_admin()
   	{
		$data=$this->Get_Post();
		$res=Model::SaveInfo($data);
		\ShowJson($res['status'],$res['msg']);
   	} 
   	//修改管理员密码
   	public function update_password()
   	{
		$data=$this->Get_Post();
		$res=Model::SetPassword($data);
		\ShowJson($res['status'],$res['msg']);

   	}
    //读取管理员信息
    public function get_admininfo()
    {
		$result=Model::field('id,type,username,facepic,nickname,status,regtime,regip,logintime,loginip')->where('id',input('id'))->find();
    	$result['regtime']=MyDate('',$result['regtime']);
    	$result['logintime']=MyDate('',$result['logintime']);
    	\ShowJson(0,'获取成功',$result);
    }
    //删除管理员
    public function del()
    {
		$id=input('id')??0;
    	$admin=Model::get($id);
    	if($admin['username']=='developer'){
    		ShowJson(1,'系统管理员禁用删除！');
    	}
    	if(Model::destroy($id)){
			Syslog::Rec(1,"删除管理员[$id]",0);
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }
    //管理员权限管理
    public function privilege()
    {
    	$uid=input('uid');
        $priv_list=$this->Get_Admin_Menu_List();
		
		$Admin=$this->AdminInfo;
      	if($Admin['type']==0){
        	$this->error('普通管理员无权限设置管理员');
        }
    	if($Admin['username']=='developer'){
    		 $Admin_list=Model::order('id asc')->select();
    	}else if($this->AdminInfo['type']==1){
    		 $Admin_list=Model::where('type',0)->where("`username`<>'developer'")->order('id asc')->select();
		}
    	foreach($priv_list as $k=>$v){
    		if($v['menu_url']!='model/index' and $v['menu_url']!='menu/index'){
    			$priv_list[$k]=$v;
    		}else{
    			unset($priv_list[$k]);
    		}
    	}
        $this->assign('uid',$uid);
        $this->assign('admin_list',$Admin_list);
        $this->assign('priv_list',$priv_list);
    	$this->assign('page_title' ,'管理员权限管理');
    	return $this->fetch();
    }
    //保存管理权限设置
    public function save_privilege(){
		$data=$this->Get_Post();
		$res=Model::SavePrivilege($data);
		\ShowJson($res['status'],$res['msg']);
    }
    //获得管理权限
    public function get_privilege(){
    	$uid=input('uid')??0;
    	$admin=Model::get($uid);
		$acl['menu']=$admin['acl'];
    	\ShowJson(0,'获取成功',$acl);
	}
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			Syslog::Rec(1,"设置了管理员[$id]",0);
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}	
}
