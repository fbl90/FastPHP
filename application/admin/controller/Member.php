<?php
namespace app\admin\controller;

use \app\admin\model\Member as Model;
use \app\admin\model\Syslog;

class Member extends Base
{
    //会员列表页面初始化
    public function index()
    {
		$kw=input('kw');
		$re=Model::GetPage($kw);
        $this->assign('list',$re['list']);
        $this->assign('pagelist',$re['pagelist']);
        $this->assign('page_title' ,'会员管理');
        return $this->fetch();
    }
    //会员系统配置主页
    public function config(){
		
    	$list=$this->Get_ConfigClass('member');
		
		foreach($list as $k=>$v){
			$list[$k]['nodelist']=$this->Get_ConfigData($v['classid'],'member');
		}
		
    	if(is_numeric(input('cid'))){
    		$cid=input('cid');
    	}else{
    		$cid=@$list[0]['id'];
    	}
    	$lastid=count($list)+1;
    	$this->assign('cid',$cid);
    	$this->assign('config_class_list',$list);
    	$this->assign('config_class_num',$lastid);
        $this->assign('page_title' ,'会员系统设置');
        return $this->fetch();
        
	}
	/*
		设置密码
	*/
	public function update_password()
	{
		$data=$this->Get_Post();

    	if($data['newpassword']!=$data['repassword']){
    		ShowJson(1,'两次输入的密码不一致');
    	}
    	//创建一个安全码
    	$safe_code	=GetRandStr(16);
    	$password	=Encrypt($safe_code,$data['newpassword']);
    	$data=array('id'=>$data['id'],'safecode'=>$safe_code,'password'=>$password);
    	if(Model::update($data)){
    		ShowJson(0,'密码修改成功！');    	
    	}else{
    		ShowJson(1,'密码修改失败！');
    	}
	}
    //读取会员信息
    public function member_info()
    {
		$result=Model::field('id,username,email,qqnum,mobile,telephone,nickname,facepic,birthday,balance,integral,sex,area,address,address_location,type,regtime,regip,logintime,loginip,status')->where('id',input('id'))->find();
    	$result['regtime']=MyDate('',$result['regtime']);
    	$result['logintime']=MyDate('',$result['logintime']);
		
		$this->assign('userinfo',$result);
		return $this->fetch();
	}
	/*
		删除数据
	*/
	public function del()
	{
		$id=input('id')??0;
		$result=Model::destroy($id);
		if($result){
			\ShowJson(0,'删除成功');
		}else{
			\ShowJson(1,'删除失败');
		}
	}	
	/*
		批量设置
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}	
}
