<?php
namespace app\admin\controller;

/*
 * 管理后台基础控制器 
 * 加载全局公共控制器，并继承
 */

use app\base\Common;
use \app\admin\model\Admin;
use \app\admin\model\Syslog;

class AdminCommon extends Common
{
    public function _initialize()
    {
    	//执行父类的构造方法
    	parent::_initialize();		
    }
    /*
     *   管理员登录验证 
     */
    public function Admin_Login()
    {
		$username	=input('username')??'';
		$password	=input('password')??'';
		$autologin	=input('autologin')??'';
		$checkcode	=input('checkcode')??'';
		
		$checkcode_1=session('checkcode');
		
		if($checkcode_1!=$checkcode || $checkcode==""){
			ShowJson(1,'验证码错误！');
		}
		
    	$AdminInfo=Admin::where('username',$username)->find();
    	if(!$AdminInfo){
    		ShowJson(1,'管理员不存在！');
    		exit();
    	}
    	
    	if($AdminInfo['status']==1){
			Syslog::Rec(1,$username.'管理员已经被禁用了！',1,true);
    		ShowJson(1,'管理员已经被禁用了！');
    		exit();
    	}
    	
    	if($AdminInfo['trynum']>$this->Config['adminlogin_trynum']){
			Admin::where('username',$username)->update(['status'=>1]);
			$msg='您的登录失败的次数超过'.$this->Config['adminlogin_trynum'].'次，账户已经被禁用了！';
			Syslog::Rec(1,$username.$msg,1,true);
    		ShowJson(1,$msg);
    		exit();
    	}
    	
    	if(!CheckEncrypt($AdminInfo['safecode'],$password,$AdminInfo['password'])){
    		Admin::where('username',$username)->setInc('trynum');
			$Surplus=$this->Config['adminlogin_trynum']-$AdminInfo['trynum'];
			$msg='管理员密码错误,您还有'.$Surplus.'次机会。';
			Syslog::Rec(1,$username.'登录失败：'.$msg,1,true);
    		ShowJson(1,$msg);
    		exit();
    	}
    	
    	//登录成功后 尝试次数 置0
    	Admin::where('username',$username)->update(['trynum'=>0]);
    	
    	$loginip=GetIP();
    	$logintime=time();
    	
    	$login_data=array(
    		'id'=>$AdminInfo['id'],
    		'username'=>$AdminInfo['username'],
    		'nickname'=>$AdminInfo['nickname'],
    		'logintime'=>$logintime,
    		'loginip'=>$loginip
    	);
    	
    	$update=array('logintime'=>$logintime,'loginip'=>$loginip);
    	//记录登录时间和IP地址
    	Admin::where('username',$username)->update($update);
    	//加密管理登录信息数据
    	$ciphertext=AuthCode(serialize($login_data),'ENCODE',$this->Config['cookie_secretkey']);
    	//把密文存储到会话中
		session('adminfo',$ciphertext);
		Syslog::Rec(1,$username.'登录成功',0);		
		ShowJson(0,'登录成功！');
    }
    //验证是否登录
    public function CheckLogin()
    {
		$adminfo_Session=session('adminfo');
    	$adminfo_Session=AuthCode($adminfo_Session,'DECODE',$this->Config['cookie_secretkey']);
		$admininfo=@unserialize($adminfo_Session);

    	if(is_array($admininfo))
    	{
			$developer=Admin::where('id',1)->where('username','developer')->find();
    		if(!$developer){
	    		session('adminfo',null);
	    		cookie('adminfo',null);
	    		return false;
			}
    		$Admin=Admin::where('username',@$admininfo['username'])->find();
    		if($Admin){
    			$Admin['lockscreen']="";
    			return $Admin;
    		}else{
	    		session('adminfo',null);
	    		cookie('adminfo',null);
	    		return false;
    		}
    	}else{
    		session('adminfo',null);
    		cookie('adminfo',null);
    		return false;
    	}
    }
    //管理员密码校验
    public function AdminPasswordCheck($username,$password){
		$AdminInfo=Admin::where('username',$username)->find();
		if($AdminInfo){
	    	if(CheckEncrypt($AdminInfo['safecode'],$password,$AdminInfo['password'])){
	    		return true;
	    	}else{
	    		return false;
	    	}
		}else{
			return false;
		}
    }
    //退出登录
    public function LogOut()
    {
    	session('adminfo_lock',null);
    	session('adminfo',null);
    	cookie('adminfo',null);
    	$this->redirect(url('login/index'));
    }
    //超级管理员重置密码
    public function SuperAdminRePassword(){
//  	$u=input('username');
//  	$p=input('password');
//  	//创建一个安全码
//  	$safe_code	=GetRandStr(16);
//  	$password	=Encrypt($safe_code,$p);
//  	$data=array('safecode'=>$safe_code,'password'=>$password);
//  	$result=Admin::where('username',$u)->update($data);
    }
}
