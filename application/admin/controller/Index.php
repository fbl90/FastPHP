<?php
namespace app\admin\controller;

use \app\admin\model\Syslog;
use \app\admin\model\Nav;

class Index extends Base
{
    public function index()
    {
    	$log_lists['syslog']=Syslog::where('type',1)->order("`time` DESC")->limit(8)->select();
    	$log_lists['memlog']=Syslog::where('type',2)->order("`time` DESC")->limit(8)->select();
    	$log_lists['ordlog']=Syslog::where('type',3)->order("`time` DESC")->limit(8)->select();
    	$log_lists['paylog']=Syslog::where('type',4)->order("`time` DESC")->limit(8)->select();
    	$log_lists['smslog']=Syslog::where('type',5)->order("`time` DESC")->limit(8)->select();
    	
    	$nav_list=Nav::where('classid',11)->where('status',0)->order('orderid ASC')->select();
    	
    	$nav_list=array_chunk($nav_list,4);
    	
        $this->assign('nav_list' ,$nav_list);
        $this->assign('log_lists' ,$log_lists);
        
        $this->assign('server_info' ,array_slice(Get_Server_Base_Info(),0,8));
        $this->assign('php_info' ,array_slice(Get_PHP_Base_Info(),0,8));
        $this->assign('phpext_list' ,array_slice(Get_PHP_Ext_list(),0,8));
        $this->assign('page_title' ,'管理主页');
        return $this->fetch();
    }
}
