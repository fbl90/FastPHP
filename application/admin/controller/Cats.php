<?php
namespace app\admin\controller;

/*
 	栏目管理控制器
 */
use \app\admin\model\Models;
use \app\admin\model\Cats as Model;
use \app\admin\model\Syslog;

class Cats extends Base
{
    private $table_name='cats';
	/*
		栏目首页
	*/
    public function index()
    {
		$data=Model::TreeList();
		$orderid=Model::count();
        $this->assign('orderid',$orderid+1);
        $this->assign('cats_tree',$data['tree']);
        $this->assign('list',$data['list']);
        $this->assign('model_list',Models::select());
        $this->assign('page_title' ,'主栏目管理');
        return $this->fetch();
	}
	/*
		获得栏目信息
	*/
	public function Get_Cats_Info(){
		$id=input('id')??0;
		\ShowJson(0,'获取成功',Model::get($id));
	}
	/*
	 	保存栏目数据
	 */
	public function save_cats()
	{
		$data=$this->Get_Post();
		$result=Model::SaveData($data);
		\ShowJson($result['status'],$result['msg']);
	}
	/*
		删除数据
	*/
	public function del()
	{
		$id=input('id')??0;
		$result=Model::del($id);
		\ShowJson($result['status'],$result['msg']);
	}
	/*
		排序
	*/
	public function update_sort()
	{
		$post_d=$this->Get_Post();
		$result=Model::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		批量设置
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		通用设置
	*/
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
}
