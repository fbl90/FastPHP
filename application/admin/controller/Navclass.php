<?php
/*
      网站导航分类管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\NavClass as Model;
use \app\admin\model\Nav;
use \app\admin\model\Iconfont;
use \app\admin\model\Syslog;

class Navclass extends Base
{    
    public function index()
    {
     	$list=Model::order('id ASC')->paginate($this->Config['listpagesize']);
        $this->assign('page_title' ,'导航分类管理');
        $this->assign('list',$list);
        $this->assign('pagelist',$list->render());
        return $this->fetch();
    }
     /*
      	获得导航分类	
      */
     public function get_class_info()
     {
        $result=Model::get(input('id'));
        \ShowJson(0,'获取成功',$result);
     }
     /*
      	保存修改的导航分类
      */
     public function save_navclass()
     {
		$data=$this->Get_Post();
		$res=Model::SaveData($data);
		\ShowJson($res['status'],$res['msg']);
	 }
	 /*
	 	删除导航分类
	 */
	public function del()
	{
		$id=input('id');
		if(Model::destroy($id)){
			Nav::destroy(['classid'=>$id]);
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}

}