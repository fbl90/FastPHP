<?php
/*
       幻灯轮播管理控制器'
*/
namespace app\admin\controller;

use \app\admin\model\Slider as Model;
use \app\admin\model\Syslog;

class Slider extends Base
{    
    public function index()
    {
		$res=Model::GetPage();
		$this->assign('cats_tree',$res['cats_tree']);
        $this->assign('page_title' ,'轮播图管理');
        $this->assign('list',$res['list']);
        $this->assign('pagelist',$res['pagelist']);
        return $this->fetch();
    }
	
	public function get_silder_info()
	{
		$res=Model::get(input('id'));
		\ShowJson(0,'获取成功',$res);	
	}
	/*
		保存
	*/
	public function save_slider()
	{
		$data=$this->Get_Post();
		$res=Model::SaveData($data);
		\ShowJson($res['status'],$res['msg']);
	}
	/*
		删除
	*/
	public function del()
	{
		if(Model::destroy(input('id'))){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}	
}
