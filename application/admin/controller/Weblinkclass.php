<?php
/*
      友情链接管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\WeblinkClass as Model;
use \app\admin\model\Weblink;

class Weblinkclass extends Base
{
    /*
     	友情链接分类管理
     */
    public function index()
    {
        $list=Model::order('id ASC')->paginate(30, false, ['query' => request()->param()]);
        $this->assign('page_title', '友情链接分类管理');
        $this->assign('list', $list);
        $this->assign('pagelist', $list->render());
        return $this->fetch();
    }
    /*
     	获得友情链接分类
     */
    public function get_class_info()
    {
        $res=Model::get(input('id'));
        \ShowJson(0, '获取成功', $res);
    }
    /*
     	保存修改的友情链接分类
     */
    public function save_weblink_class()
    {
		$data=$this->Get_Post();
		$res=Model::SaveData($data);
		\ShowJson($res['status'],$res['msg']);
	}
	/*
		删除
	*/
	public function del()
	{
		if(Model::destroy(input('id'))){
			Weblink::destroy(['classid'=>input('id')]);
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
}
