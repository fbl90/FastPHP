<?php
/*
      网站导航管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\Nav as Model;
use \app\admin\model\Iconfont;
use \app\admin\model\Syslog;

class Nav extends Base
{    
    public function index()
    {
        $data=Model::TreeList();
        $orderid=Model::count();
        $icon_list=Iconfont::cache(true,86400)->select();
        
        $this->assign('icon_list',$icon_list);        
        $this->assign('orderid',$orderid+1);
        $this->assign('classlist',$data['classlist']);
        $this->assign('nav_tree',$data['nav_tree']);
        $this->assign('page_title' ,'导航管理');
        $this->assign('list',$data['list']);
        return $this->fetch();
    }
	
    public function get_nav_info()
    {
        $result=Model::get(input('id'));
        \ShowJson(0,'获取成功',$result);
    }

	/*
		保存信息
	*/
    public function save_nav()
    {
		$post_d=$this->Get_Post();
		$result=Model::SaveData($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	/*
		删除数据
	*/
	public function del()
	{
		$id=input('id')??0;
		$result=Model::destroy($id);
		if($result){
			\ShowJson(0,'删除成功');
		}else{
			\ShowJson(1,'删除失败');
		}
	}
	/*
		排序
	*/
	public function update_sort()
	{
		$post_d=$this->Get_Post();
		$result=Model::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Model::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}    
}