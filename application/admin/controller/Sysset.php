<?php
namespace app\admin\controller;

use \app\admin\model\Config;
use \app\admin\model\ConfigClass;
use \app\admin\model\Syslog;

class Sysset extends Base
{
	private $group='system';
	
    public function index()
    {
    	$list=$this->Get_ConfigClass($this->group);

		foreach($list as $k=>$v){
			$list[$k]['nodelist']=$this->Get_ConfigData($v['classid'],$this->group);
		}
    	if(is_numeric(input('cid'))){
    		$cid=input('cid');
    	}else{
    		$cid=$list[0]['id'];
    	}
    	$lastid=count($list)+1;
    	$this->assign('cid',$cid);
    	$this->assign('config_class_list',$list);
    	$this->assign('config_class_num',$lastid);
        $this->assign('page_title' ,'系统设置');
        return $this->fetch();
	}
	
	//保存配置分类
	public function saveconfigclass()
	{
		$data=$this->Get_Post();
		if(ConfigClass::SaveData($data)){
			ShowJson(0,'操作成功');
		}else{
			ShowJson(1,'操作失败');
		}
	}
	//保存配置
	public function saveconfig()
	{
		$data=$this->Get_Post();
		if(Config::SaveData($data)){
			ShowJson(0,'操作成功');
		}else{
			ShowJson(1,'操作失败');
		}
	}
    //获得配置分类
    public function get_config_class()
    {
    	if($this->AdminInfo['username']=='developer'){
    		$list=ConfigClass::where('group','system')->order('order ASC')->select();
    	}else{
    		$list=ConfigClass::where('group','system')->where('isdeveloper',0)->order('order ASC')->select();
    	}		
    	\ShowJson(0,'获取成功',$list);
    }
    //删除一个配置
    public function del_config()
    {
		$id=input('id');
    	if(Config::destroy($id)){
    		ShowJson(0,'操作成功');
    	}else{
    		ShowJson(1,'操作失败！');
    	}
    }
    //获得配置的详细信息
    public function get_config_data()
    {
		$id=input('id');
    	if($this->AdminInfo['username']=='developer'){
    		$result=Config::where('id',$id)->find();
		}else{
			$result=Config::where('id',$id)->where('isdeveloper',0)->find();
		}
    	\ShowJson(0,'获取成功',$result);
    }

    //保存配置
    public function save()
    {
    	$list=$this->Get_Post();
    	$group=$list['group'];
    	unset($list['group']);
    	unset($list['classid']);
    	foreach($list as $k=>$v){
    		if(is_array($v)){
    			$val=implode(',',$v);
    		}else{
    			$val=$v;
    		}
    		$list[$k]=$val;
    		Config::where('group',$group)->where('name',$k)->update(['value'=>$val]);	
    	}
		//清空缓存
		$this->Clear_All_cache();
    	ShowJson(0,'配置修改成功');
    }
    //获得配置分组下面的分类
	public function get_config_group()
	{
    	$group=input('group');
    	if($this->AdminInfo['username']=='developer'){
    		$list=ConfigClass::where('group',$group)->select();
		}else{
			$list=ConfigClass::where('group',$group)->where('isdeveloper',0)->select();
		}
    	\ShowJson(0,'获取成功',$list);
    }
}
