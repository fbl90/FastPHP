<?php
/*
	广告分类管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\AdClass as Model;
use \app\admin\model\Syslog;

class Adclass extends Base
{   
    /*
        分类首页
    */
    public function index()
    {
		$kw=input('kw')??'';
     	$list=Model::Page(20,$kw);
        $this->assign('page_title' ,'广告分类管理');
        $this->assign('list',$list);
        $this->assign('pagelist',$list->render());
        return $this->fetch();
    }
    /*
      	获得广告分类	
    */
     public function get_adclass_info()
     {
		$id=input('id')??0;
		$result=Model::get($id);
		\ShowJson(0,'获取成功',$result);
     }
     /*
      	保存广告分类
      */
     public function save_adclass()
     {
		$post_d=$this->Get_Post();
		$result=Model::SaveData($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
     }
     /*
        删除分类
     */
    public function del()
    {
		$id=input('id')??0;
		$result=Model::destroy($id);
		if($result){
			Syslog::Rec(1,"删除广告分类[$id]",0);
			\ShowJson(0,'删除成功');
		}else{
			\ShowJson(1,'删除失败');
		}
    }
	/*
		批量设置
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}    
}
