<?php
namespace app\admin\controller;

use \app\admin\model\Syslog as Model;

class Syslog extends Base
{
    public function index()
    {
		$type=input('type')??0;
		$kw=input('kw')??'';

		$time=time();
		$stime=input('stime')?GetMkTime(input('stime')):($time-604800);
		$etime=input('etime')?GetMkTime(input('etime')):$time;
		
		$sql[]="(`time`>='$stime' AND `time`<='$etime')";
		
		if($kw!=''){
			$sql[]="(`uname`='$kw' OR `msg` LIKE '%$kw%')";
		}
		if($type>0){
			$sql[]="(`type`='$type')";
		}
		
		$list=Model::where(\implode("and",$sql))->order("`time` DESC")->paginate(30, false, ['query' => request()->param()]);
		
        $this->assign('stime' ,MyDate('Y-m-d H:i:s',$stime));
        $this->assign('etime' ,MyDate('Y-m-d H:i:s',$etime));
        $this->assign('list' ,$list);
        $this->assign('pagelist',$list->render());
        $this->assign('page_title' ,'日志管理主页');
        $this->assign('type' ,$type);
        $this->assign('kw' ,$kw);
        return $this->fetch();
    }
}