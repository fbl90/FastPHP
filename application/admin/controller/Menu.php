<?php
/*
      菜单管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\{AdminMenu,Iconfont};
use \app\admin\model\Syslog;

class Menu extends Base
{    
    //------------------------后台操作节点管理部分------------------
    /*
       	读取所有权限节点分页数据
    */
    public function index()
    {
        
        $tree=array();
        $list=AdminMenu::where('menu_pid',0)->order('orderid asc')->paginate(30, false, ['query' => request()->param()]);
        foreach($list as $v){
            $slist=AdminMenu::where('menu_pid',$v['id'])->order('orderid asc')->select();
            $v['menu_list']=$slist;
            $tree[]=$v;
        }
        
        $icon_list=Iconfont::cache(true,86400)->select();

        $this->assign('page_title' ,'菜单管理');
        $this->assign('icon_list',$icon_list);
        $this->assign('list',$tree);
        $this->assign('pagelist',$list->render());
        return $this->fetch();
    }
    /*
       	获得一个最新的orderid
    */
    public function get_orderid()
    {
        $pid=input('pid');
        $result=AdminMenu::where('menu_pid',$pid)->count();
        $result++;
        $count['count']=$result;
        \ShowJson(0,'获取成功',$count);
    }
    /*
       	获得管理节点数据
    */
    public function get_data()
    {
        $id=input('id');
        $info=AdminMenu::get($id);
        \ShowJson(0,'获取成功',$info);
    }
    /*
       	修改保存节点
    */
    public function save_node()
    {
        $data=$this->Get_Post();
        $res=AdminMenu::SaveData($data);
        \ShowJson($res['status'],$res['msg']);
    } 
    /*
        删除
    */
    public function del()
    {
		$id=input('id')??0;
		$result=AdminMenu::destroy($id);
		if($result){
			\ShowJson(0,'删除成功');
		}else{
			\ShowJson(1,'删除失败');
		}
    }
	/*
		排序
	*/
	public function update_sort()
	{
		$post_d=$this->Get_Post();
		$result=AdminMenu::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}    
	//通用设置
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=AdminMenu::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }
    
}
