<?php
namespace app\admin\controller;

use \app\admin\model\Admin;
use \app\admin\model\Syslog;

class Profile extends Base
{    
    public function index()
    {
        $this->assign('page_title' ,'管理员中心');
        return $this->fetch();
    }
    
   	//修改基本信息
   	public function save(){
		$data=$this->Get_Post();
		$data['id']=$this->AdminInfo['id'];
   		if(Admin::update($data)){
			Syslog::Rec(1,"更新会员资料",0);
   			ShowJson(0,'修改成功');
   		}else{
   			ShowJson(1,'修改失败');
   		}
   	}
   	//修改密码
   	public function change_password(){
    	if(!CheckEncrypt($this->AdminInfo['safecode'],input('oldpassword'),$this->AdminInfo['password'])){
    		ShowJson(1,'旧密码错误！');
    	}
    	if(input('newpassword')!=input('repassword')){
    		ShowJson(1,'两次输入的密码不一致');
    	}
    	//创建一个安全码
    	$safe_code	=GetRandStr(16);
    	$password	=Encrypt($safe_code,input('newpassword'));
    	$data=array('id'=>$this->AdminInfo['id'],'safecode'=>$safe_code,'password'=>$password);
    	if(Admin::update($data)){
			Syslog::Rec(1,"修改了会员密码",0);
    		ShowJson(0,'密码修改成功！');    	
    	}else{
    		ShowJson(1,'密码修改失败！');
    	}
   	}
}
