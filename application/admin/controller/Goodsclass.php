<?php
namespace app\admin\controller;

/*
    商城栏目管理控制器
 */

use \app\admin\model\shop\GoodsClass as Model;
use \app\admin\model\shop\Goods;
use \app\admin\model\Syslog;

class Goodsclass extends Base
{
    //产品栏目列表页面初始化
    public function index()
    {
        $res=Model::TreeList();
        $orderid=Model::count();

        $this->assign('orderid', $orderid+1);
        $this->assign('cats_tree', $res['cats_tree']);
        $this->assign('list', $res['list']);
        $this->assign('page_title', '商城栏目管理');
        return $this->fetch();
    }
    //获得栏目信息
    public function get_goodsclass_info()
    {
        $res=Model::get(input('id'));
        \ShowJson(0, '获取成功', $res);
    }
    //保存栏目数据
    public function save_goodsclass()
    {
        $data=$this->Get_Post();
        $res=Model::SaveData($data);
        \ShowJson($res['status'], $res['msg']);
    }
	/*
		排序
	*/
	public function update_sort()
	{
		$post_d=$this->Get_Post();
		$result=Model::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }  
	/*
		批量设置
	*/
	public function bat_set()
	{
		$data['type']=input('type')??'';
		$data['ids']=input('ids')??'';
		$result=Model::bat($data);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}      
	//删除栏目
	public function del()
	{
		$id=input('id');
		$cats=Model::get($id);

		$subCats=Model::where("`path` LIKE '%,$id,%'")->field('id,classname')->select();
		$ids=[$id];

		foreach($subCats as $k=>$v){
			$ids[]=$v['id'];
		}
		
		if(count($ids)>0){
			//删除栏目
			//并删除栏目商品
			Model::destroy($ids);
			foreach($ids as $cid){
				Goods::destroy(['classid'=>$cid]);
			}
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
		
	}
}
