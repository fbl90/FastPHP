<?php
/*
      模型字段管理控制器
*/
namespace app\admin\controller;

use \app\admin\model\Models as Models;
use \app\admin\model\ModelField as Field;
use \app\admin\model\FieldType as FieldType;
use \app\admin\model\Syslog;

class Modelfield extends Base
{
    public function _initialize()
    {
        //执行父类的构造方法
        parent::_initialize();
    }
    /*
       	读取所有模型分页数据
    */
    public function index()
    {   
        $mid=input('mid')??0;
        $kw=input('kw')??'';

        $list=Field::Page(20,$mid,$kw);
        if($mid>0){
            $model=Models::get($mid);
            $this->assign('model_name',$model['model_name']);
        }else{
            $this->assign('model_name','所有模型');
        }
        $this->assign('model_list',Models::all());
        $this->assign('types',FieldType::all());
        $this->assign('page_title', '模型字段管理');
        $this->assign('list' ,$list);
        $this->assign('pagelist',$list->render());

        return $this->fetch();
    }
    /*
        删除字段
    */
    public function del()
    {
        $id=input('id')??0;

        if(Field::del($id)){
            \ShowJson(0,'操作成功');
        }else{
            \ShowJson(1,'操作失败');
        }

    }
    /*
        获取详情
    */
    public function get_field_info()
    {
        $id=input('id')??0;
        $result=Field::get($id);
        \ShowJson(0,'获取成功',$result);
    }
    /*
        保存
    */
    public function save_field()
    {
        $data=$this->Get_Post();
        $res=Field::SaveData($data);
        \ShowJson($res['status'],$res['msg']);
    }
    /*
        排序
    */
    public function update_sort()
    {
		$post_d=$this->Get_Post();
		$result=Field::Sort($post_d);
		if($result){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
    }  
	/*
		通用设置
	*/
	public function common_action()
	{
		$id=input('id')??0;
		$field=input('field')??'';
		$value=input('value')??'';
		if($id==0 || $field==''){
			ShowJson(1,'参数错误');
		}
		$res=Field::where('id',$id)->update([$field=>$value]);
		if($res){
			\ShowJson(0,'操作成功');
		}else{
			\ShowJson(1,'操作失败');
		}
	}    
}
