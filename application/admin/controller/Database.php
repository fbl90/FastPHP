<?php
/*
     数据库备份/还原控制器
*/
namespace app\admin\controller;

use \app\base\DDL;
use \app\admin\model\Syslog;

class Database extends Base
{
	public function index()
	{
		$list=ScandirAll(APP_PATH.DS.'data'.DS.'database');
		$new_list=array();
		foreach($list as $k=>$v){
            if(basename($v)!='install'){
                //文件路径
                $new_list[$k]['file']=$v;
                $new_list[$k]['dirname']=pathinfo($v,PATHINFO_DIRNAME);
                //文件的名称
                $new_list[$k]['basename']=basename($v);
                //文件大小
                $new_list[$k]['filesize']=GetRealSize(filesize($v));
                //文件创建时间
                $new_list[$k]['filectime']=MyDate('Y-m-d H:i:s',filectime($v));
                //文件修改时间
                $new_list[$k]['filemtime']=MyDate('Y-m-d H:i:s',filemtime($v));
                //文件最后一次访问时间
                $new_list[$k]['fileatime']=MyDate('Y-m-d H:i:s',fileatime($v));
            }
		}
		$last_names = array_column($new_list,'basename');
		array_multisort($last_names,SORT_ASC,$new_list);

		$this->assign('page_title' ,'数据库备份/还原管理');
		$this->assign('new_list',$new_list);
		return $this->fetch();
	}
	//创建新的备份
	public function new_backup()
	{
		set_time_limit(0);
		
		//获得默认数据库配置
		$config = config('database');
		$config["sqlbakname"]=$config['database'].'_'.MyDate('Y-m-d-H-i-s',time()).'.bak';
		$backup = new \app\base\Backup($config);
		$info = $backup->backup();
		ShowJson(0,$info);
	}
	//删除备份文件
	public function deletefile()
	{
		$filename=input('filename');
		$path=ROOT_PATH .'application'.DS.'data'. DS .'database'.DS.$filename;

		if(unlink($path)){
			ShowJson(0,'删除成功');
		}else{
			ShowJson(1,'删除失败');
		}
	}
	//恢复备份文件
	public function recoverfile()
	{
		$filename=input('filename');
		//获得默认数据库配置
		$config = config('database');
		$backup = new \app\base\Backup($config);
		$info = $backup->restore($filename);
		ShowJson(0,$info);
	}
	//清空所有业务数据
	public function clear_database()
	{
		$DDL=new DDL;
		$DDL->T_Table_CleatData('cats');
		$DDL->T_Table_CleatData('cats');
		$DDL->T_Table_CleatData('member');
		$DDL->T_Table_CleatData('member_favorite');
		$DDL->T_Table_CleatData('member_drawcash');
		$DDL->T_Table_CleatData('syslog');
		$DDL->T_Table_CleatData('document');
		$DDL->T_Table_CleatData('ad');
		$DDL->T_Table_CleatData('ad_class');
		$DDL->T_Table_CleatData('feedback');
		$DDL->T_Table_CleatData('goods');
		$DDL->T_Table_CleatData('goods_class');
		$DDL->T_Table_CleatData('goods_comment');
		$DDL->T_Table_CleatData('goods_order');
		$DDL->T_Table_CleatData('goods_orderdetails');
		$DDL->T_Table_CleatData('member_address');
		$DDL->T_Table_CleatData('member_shopcart');
		$DDL->T_Table_CleatData('member_token');
		$DDL->T_Table_CleatData('notice');
		$DDL->T_Table_CleatData('pay_record');
		$DDL->T_Table_CleatData('slider');
		$DDL->T_Table_CleatData('weblink');
		$DDL->T_Table_CleatData('weblink_class');
		
		ShowJson(0,'清理完成!');
	}
	//设置为模板
	public function set_temp()
	{
		$filename=input('filename');
		$path=ROOT_PATH .'application'.DS.'data'. DS .'database'.DS.$filename;
		$basefile=ROOT_PATH .'application'.DS.'data'. DS .'database'.DS.'base.bak';
		if(rename($path,$basefile)){
			ShowJson(0,'设置成功');
		}else{
			ShowJson(1,'设置失败');
		}
	}
}
