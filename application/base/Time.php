<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\base;

/*
    日期时间操作类
    Author:FastPHP
*/

class Time
{
    //返回今日开始和结束的时间戳
    public static function today()
    {
        return [
            mktime(0, 0, 0, date('m'), date('d'), date('Y')),
            mktime(23, 59, 59, date('m'), date('d'), date('Y'))
        ];
    }
    //返回昨日开始和结束的时间戳
    public static function yesterday()
    {
        $yesterday = date('d') - 1;
        return [
            mktime(0, 0, 0, date('m'), $yesterday, date('Y')),
            mktime(23, 59, 59, date('m'), $yesterday, date('Y'))
        ];
    }
    //返回本周开始和结束的时间戳
    public static function week()
    {
        $timestamp = time();
        return [
            strtotime(date('Y-m-d', strtotime("+0 week Monday", $timestamp))),
            strtotime(date('Y-m-d', strtotime("+0 week Sunday", $timestamp))) + 24 * 3600 - 1
        ];
    }
    //返回上周开始和结束的时间戳
    public static function lastWeek()
    {
        $timestamp = time();
        return [
            strtotime(date('Y-m-d', strtotime("last week Monday", $timestamp))),
            strtotime(date('Y-m-d', strtotime("last week Sunday", $timestamp))) + 24 * 3600 - 1
        ];
    }
    //返回本月开始和结束的时间戳
    public static function month($everyDay = false)
    {
        return [
            mktime(0, 0, 0, date('m'), 1, date('Y')),
            mktime(23, 59, 59, date('m'), date('t'), date('Y'))
        ];
    }
    //返回上个月开始和结束的时间戳
    public static function lastMonth()
    {
        $begin = mktime(0, 0, 0, date('m') - 1, 1, date('Y'));
        $end = mktime(23, 59, 59, date('m') - 1, date('t', $begin), date('Y'));
        return [$begin, $end];
    }
    //返回今年开始和结束的时间戳
    public static function year()
    {
        return [
            mktime(0, 0, 0, 1, 1, date('Y')),
            mktime(23, 59, 59, 12, 31, date('Y'))
        ];
    }
    //返回去年开始和结束的时间戳
    public static function lastYear()
    {
        $year = date('Y') - 1;
        return [
            mktime(0, 0, 0, 1, 1, $year),
            mktime(23, 59, 59, 12, 31, $year)
        ];
    }
    //获取几天前零点到现在/昨日结束的时间戳
    public static function dayToNow($day = 1, $now = true)
    {
        $end = time();
        if (!$now) {
            list($foo, $end) = self::yesterday();
        }

        return [
            mktime(0, 0, 0, date('m'), date('d') - $day, date('Y')),
            $end
        ];
    }
    //返回几天前的时间戳
    public static function daysAgo($day = 1)
    {
        $nowTime = time();
        return $nowTime - self::daysToSecond($day);
    }
    //返回几天后的时间戳
    public static function daysAfter($day = 1)
    {
        $nowTime = time();
        return $nowTime + self::daysToSecond($day);
    }
    //天数转换成秒数
    public static function daysToSecond($day = 1)
    {
        return $day * 86400;
    }
    //周数转换成秒数
    public static function weekToSecond($week = 1)
    {
        return self::daysToSecond() * 7 * $week;
    }
    //获取指定年份 指定数量月 的每个月开始和结束时间缀
    public static function YearMonthList($year,$month=12)
    {
        $cyear =$year;
        $cMonth = 1;
        $list=[];
        for($i=0;$i<$month;$i++){
            $nMonth = $cMonth+$i;
            $cyear = $nMonth == 13 ? ($cyear+1) : $cyear;
            $nMonth = $nMonth >= 13 ? ($nMonth-12) : $nMonth;
            $date = $cyear."-".$nMonth."-1";
            $list[$i]['start'] = (date('Y-m-01 00:00:01', strtotime($date)));
            $list[$i]['end'] = (date('Y-m-t 23:59:59', strtotime($date)));
        }
        return $list;
    }
}