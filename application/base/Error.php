<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\base;

use think\exception\Handle;
use think\exception\HttpException;

/*
    FastPHP系统异常处理类
*/ 

class Error extends Handle
{
    public function render(\Exception $e)
    {
        if ($e instanceof HttpException) {
            $statusCode = $e->getStatusCode();
        }
        //把系统错误写入开发者日志，方便API、回调等场景跟踪异常错误
        $ErrLogMSG="错误文件:".$e->getFile().'/错误行：'.$e->getLine().'/错误内容：'.$e->getMessage();
        WLog('FastPHP_SysError',$ErrLogMSG);
        //TODO::开发者对异常的操作
       //可以在此交由系统处理
        return parent::render($e);
    }
}
