<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\base;

/*
 * 系统全局基础控制器 
 */
use think\{Controller,Request,Db,Cache};
use \app\admin\model\{Syslog,Config,ConfigClass};

class Common extends Controller
{
	//用于存放所有基本配置信息
	public $Config;
	public $Controller;
	public $Method;
	public $Validatecode;
	
    protected function _initialize(){
		
		//检测系统数据库是否已经安装
		CheckDataBase();

    	//读取所有配置数据
		$this->Config		=$this->Get_Config_All();
		$request 			=Request::instance();
		$this->Controller	=strtolower($request->controller());
		$this->Method	 	=strtolower($request->action());
		
		if($this->Config['swdebug']==1){
			config('app_debug',true);
			config('app_trace',true);
		}else{
			config('app_debug',false);
			config('app_trace',false);
		}
    }
    
    //获得所有从前端提交过来的POST数据
    protected function Get_Post(){
    	return Request::instance()->post();
    }
	//获得配置信息
	protected function Get_Config($name,$group='system'){
		$r=Config::where('group',$group)->where('name',$name)->find();
		$result=$r['value'];
		return $result;
	}
	//获得所有配置
	protected function Get_Config_All($group=""){
		if($group==""){
			$r=Config::select();
		}else{
			$r=Config::where('group',$group)->select();
		}
		$list=array();
		foreach($r as $k=>$v){
			$list[$v['name']]=$v['value'];
		}
		return $list;
	}
	//清空缓存
    protected function Clear_All_cache(){
		Cache::clear();
    }
}
