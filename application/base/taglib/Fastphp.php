<?php
/* 
    Name：FastPHP
    CopyRight: Mr.Fu 2019SR0832915
*/
namespace app\base\taglib;

use think\{Controller,Request,Db,Cache,template\TagLib};
/*
	FastPHP模板标签库
*/
class Fastphp extends TagLib
{
    protected $tags = [
		'table' 		=> ['attr' => 'table,where,order,limit', 'close' => 1],
		'slider' 		=> ['attr' => 'cid,gcid,order,limit', 'close' => 1],
		'cats' 	 		=> ['attr' => 'pid,order,limit', 'close' => 1],
		'doclist' 	 	=> ['attr' => 'cid,order,limit,flag', 'close' => 1],
		'doclistpage'  	=> ['attr' => 'cid,order,page,flag', 'close' => 1],
		'weblink' 	 	=> ['attr' => 'cid,order,limit', 'close' => 1],
        'classinfo'  	=> ['attr' => 'cid,name', 'close' => 0],
        'docinfo'  		=> ['attr' => 'id,name', 'close' => 0],
        'lastdoc'  		=> ['attr' => 'id,cid,name', 'close' => 0],
        'nextdoc'  		=> ['attr' => 'id,cid,name', 'close' => 0],
        'adinfo'		=> ['attr' => 'aid','close'=>0],
        //attr : 自定义标签的属性, close : 是否闭合标签,下面有说明
	];
    /*
      调用指定表中的数据列表
		{Fastphp:table table="表名" where="SQL条件" order="orderid ASC" limit="5" key="index" id="vo"}
		<li><a href="{$vo.linkurl}"><img src="{$vo.picurl_pc}" alt="{$vo.title}" /></a></li>
		{/Fastphp:table}
     */
    public function tagTable($tag, $content)
    {
        $table 		= $tag['table']??'';
        $where 		= $tag['where']??'';
        $order 		= $tag['order']??'id ASC';
		$limit 		= $tag['limit']??10;
		$key 		= $tag['key']??'key';
		$id 		= $tag['id']??'vo';
		
        $parse  = '<?php ';
        $parse .= '$__TABLELIST__ = db(\''.$table.'\')->where("'.$where.'")->order(\''.$order.'\')->limit('.$limit.')->select();';
        $parse .= '?>';
		$parse .= '{volist name="__TABLELIST__" id="'.$id.'" key="'.$key.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;
    }	
	/*
		调用广告文档信息
		{Fastphp:adinfo name="info" aid="1" /}
	*/
    public function tagAdinfo($tags, $content)
    {
		$name  	= $tags['name'];
		$aid 	= $tags['aid'];

        $parse  = '<?php ';
        $parse .= '$'.$name.' = db(\'ad\')->where(\'aid\',"'.$aid.'")->find();';
		$parse .= '?>';
		
        return $parse;
	}	
	/*
		调用单个类目信息
		{Fastphp:classinfo name="info" cid="49" /}
		<div class="banner-bg" style="background: url({$info.picurl|PathDS2}) no-repeat center;"></div>		
	*/
    public function tagClassinfo($tags, $content)
    {
		$cid  	= $tags['cid'];
		$name  	= $tags['name'];
		
		$cid = $this->autoBuildVar($tags['cid']);

        $parse  = '<?php ';
        $parse .= '$'.$name.' = db(\'cats\')->where(\'id\','.$cid.')->find();';
		$parse .= '?>';
		
        return $parse;
	}
	/*
		调用单个文档信息
		{Fastphp:docinfo name="info" id="49" /}
		<div class="banner-bg" style="background: url({$info.picurl|PathDS2}) no-repeat center;"></div>		
	*/
    public function tagDocinfo($tags, $content)
    {
		$name  	= $tags['name'];
		
		$id = $this->autoBuildVar($tags['id']);

        $parse  = '<?php ';
        $parse .= '$'.$name.' = db(\'document\')->where(\'id\','.$id.')->find();';
        $parse .= '?>';

        return $parse;
	}
	/*
		调用指定分类、文档ID的上一个文档信息
		{Fastphp:lastdoc name="info" id="1" cid="49" /}
	*/
    public function tagLastdoc($tags, $content)
    {
		$name  	= $tags['name'];
		
		$cid = $this->autoBuildVar($tags['cid']);
		$id = $this->autoBuildVar($tags['id']);

        $parse  = '<?php ';
        $parse .= '$'.$name.' = db(\'document\')->where(\'classid\','.$cid.')->where(\'id\',"<",'.$id.')->order("posttime DESC")->find();';
        $parse .= '?>';

        return $parse;
	}
	/*
		调用指定分类、文档ID的下一个文档信息
		{Fastphp:nextdoc name="info" id="1" cid="49" /}
	*/
    public function tagNextdoc($tags, $content)
    {
		$name  	= $tags['name'];
		
		$cid = $this->autoBuildVar($tags['cid']);
		$id = $this->autoBuildVar($tags['id']);

        $parse  = '<?php ';
        $parse .= '$'.$name.' = db(\'document\')->where(\'classid\','.$cid.')->where(\'id\',">",'.$id.')->order("posttime ASC")->find();';
        $parse .= '?>';

        return $parse;
	}
    /*
      调用幻灯片 
		{Fastphp:slider cid="0" order="orderid ASC" limit="5" key="index"}
		<li><a href="{$vo.linkurl}"><img src="{$vo.picurl_pc}" alt="{$vo.title}" /></a></li>
		{/Fastphp:slider}
     */
    public function tagSlider($tag, $content)
    {
        $order 		= $tag['order']??'orderid ASC';
		$limit 		= $tag['limit']??10;
		$key 		= $tag['key']??'key';

		$cid = $this->autoBuildVar($tag['cid']);
		
        $parse  = '<?php ';
        $parse .= '$__SLIDERLIST__ = db(\'slider\')->where(\'classid\','.$cid.')->order(\''.$order.'\')->limit('.$limit.')->select();';
        $parse .= '?>';
		$parse .= '{volist name="__SLIDERLIST__" id="vo" key="'.$key.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;
    }
	/*
		调用栏目
		{Fastphp:cats pid="0"}
		{$vo.classname}
		{/Fastphp:cats}
	*/
	public function tagCats($tag,$content)
	{
        $order 		= $tag['order']??'orderid ASC';
		$limit 		= $tag['limit']??10;
		$id 		= $tag['id']??'vo';
		$key 		= $tag['key']??'key';
		
		$pid = $this->autoBuildVar($tag['pid']);

        $parse  = '<?php ';
        $parse .= '$__CATS_LIST__ = db(\'cats\')->where(\'parent\','.$pid.')->order(\''.$order.'\')->limit('.$limit.')->select();';
        $parse .= '?>';
		$parse .= '{volist name="__CATS_LIST__" id="'.$id.'" key="'.$key.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;
	}
	/*
		调用文档
		{Fastphp:doclist cid="0"}
		{$vo.title}
		{/Fastphp:doclist}				
	*/
	public function tagDoclist($tag,$content)
	{
        $order 		= $tag['order']??'orderid ASC';
		$limit 		= $tag['limit']??10;
		$id 		= $tag['id']??'vo';
		$key 		= $tag['key']??'key';
		$kw 		= $tag['kw']??'kw';
		$flag 		= $tag['flag']??'';

		$cid = $this->autoBuildVar($tag['cid']);
		$kw = $this->autoBuildVar($tag['kw']);
		
		$parse  = '<?php ';

		//判断是否需要搜索标签
		if($flag!=''){
			$flag_sql='(`flag` LIKE \'%'.$flag.'%\') AND ';
		}else{
			$flag_sql='';
		}

		if($kw!=""){
			$parse .= '$__DOCLIST__ = db(\'document\')->where("'.$flag_sql.'(`parentstr` LIKE \'%,'.$cid.',%\' OR `classid`='.$cid.') AND `title` LIKE \'%'.$kw.'%\'")->order(\''.$order.'\')->limit('.$limit.')->select();';
		}else{
			$parse .= '$__DOCLIST__ = db(\'document\')->where("'.$flag_sql.'(`parentstr` LIKE \'%,'.$cid.',%\')")->whereOr(\'classid\','.$cid.')->order(\''.$order.'\')->limit('.$limit.')->select();';
		}
		
        $parse .= '?>';
		$parse .= '{volist name="__DOCLIST__" id="'.$id.'" key="'.$key.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;
	}

	/*
		调用文档-分页
		{Fastphp:doclistpage cid="0"}
		{$vo.title}
		{/Fastphp:doclistpage}			
		//调用分页
		{$pagelist}
	*/
	public function tagDoclistpage($tag,$content)
	{
        $order 		= $tag['order']??'orderid ASC';
		$page 		= $tag['page']??10;
		$id 		= $tag['id']??'vo';
		$key 		= $tag['key']??'key';
		$kw 		= $tag['kw']??'kw';
		$flag 		= $tag['flag']??'';
		$pagelist 	= $tag['pagelist']??'pagelist';

		$cid = $this->autoBuildVar($tag['cid']);
		$kw  = $this->autoBuildVar($tag['kw']);

		$parse  = '<?php ';
		
		//判断是否需要搜索标签
		if($flag!=''){
			$flag_sql='(`flag` LIKE \'%'.$flag.'%\') AND ';
		}else{
			$flag_sql='';
		}

		if($kw!=""){
			$parse .= '$__DOCLIST__ = db(\'document\')->where("'.$flag_sql.'(`parentstr` LIKE \'%,'.$cid.',%\' OR `classid`='.$cid.') AND `title` LIKE \'%'.$kw.'%\'")->order(\''.$order.'\')->limit('.$page.')->paginate('.$page.', false, [\'query\' => request()->param()]);';
		}else{
			$parse .= '$__DOCLIST__ = db(\'document\')->where("'.$flag_sql.'(`parentstr` LIKE \'%,'.$cid.',%\')")->whereOr(\'classid\','.$cid.')->order(\''.$order.'\')->limit('.$page.')->paginate('.$page.', false, [\'query\' => request()->param()]);';
		}
		
		$parse .= '$'.$pagelist.'= $__DOCLIST__->render();';
        $parse .= '?>';
		$parse .= '{volist name="__DOCLIST__" id="'.$id.'" key="'.$key.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;
	}	
	/*
		调用友情连接
		{Fastphp:weblink cid="0"}
		{$vo.classname}
		{/Fastphp:weblink}			
	*/
	public function tagWeblink($tag,$content)
	{
		$limit 		= $tag['limit']??10;
		$id 		= $tag['id']??'vo';
		$order 		= $tag['order']??'posttime ASC';
		
		$cid = $this->autoBuildVar($tag['cid']);

        $parse  = '<?php ';
        $parse .= '$__WEBLIST__ = db(\'weblink\')->where(\'classid\',\''.$cid.'\')->order(\''.$order.'\')->limit('.$limit.')->select();';
        $parse .= '?>';
		$parse .= '{volist name="__WEBLIST__" id="'.$id.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;
	}
	
	/*
	 *	调用导航
	 */

	public function tagNav($tag,$content)
	{
        $order 		= $tag['order']??'orderid ASC';
		$limit 		= $tag['limit']??10;
		$id 		= $tag['id']??'vo';

		$cid = $this->autoBuildVar($tag['cid']);
		$pid = $this->autoBuildVar($tag['pid']);

        $parse  = '<?php ';
        $parse .= '$__NavList__ = db(\'nav\')->where(\'classid\',\''.$cid.'\')->where(\'parentid\',\''.$pid.'\')->order(\''.$order.'\')->limit('.$limit.')->select();';
        $parse .= '?>';
		$parse .= '{volist name="__NavList__" id="'.$id.'"}';
		$parse .= $content;
		$parse .= '{/volist}';
        return $parse;	
	}

}