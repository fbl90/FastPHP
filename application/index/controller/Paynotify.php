<?php
namespace app\index\controller;
/*
	支付回调处理（支持:支付宝，微信，银联）
*/
use \app\base\{Common,MemberCommon};
class Paynotify extends Common
{
	public $MC;
    public function _initialize()
    {
    	//执行父类的构造方法
    	parent::_initialize();
		$this->MC=new MemberCommon;
	}
	public function Index()
    {
    	echo '支付回调处理程序';
    }
	//支付宝支付回调
	public function Alipay_Callback()
	{
		$out_trade_no = input('out_trade_no');//商户订单号
		$trade_no = input('trade_no');//支付宝交易号
		$log['out_trade_no']=$out_trade_no;
		$log['trade_no']=$trade_no;
		WLog('Alipay_Callback','回调校验成功！:'.array2string($log));
		//验证成功
		$this->ConfirmPay($out_trade_no,$trade_no);
		echo 'SUCCESS';
	}
	//微信支付回调
	public function Wxpay_Callback()
	{
      	$postStr = file_get_contents("php://input");
      	$result=XmlToArray($postStr);
      	$WxPay=new \app\extend\weixin\Wxpay;
      	if($WxPay->Callback_SignCheck($result)){
        	WLog('Wxpay_Callback','回调校验成功！:'.array2string($result));
          	//支付订单号
          	$PayOID=$result['out_trade_no'];
          	//支付交易号
          	$tran_id=$result['transaction_id'];
          	//支付通道类型
          	$trade_type=$result['trade_type'];
          	//调用支付确认
          	$this->ConfirmPay($PayOID,$tran_id,$trade_type);
          	$WxPay->Return_Result('SUCCESS','ok');
        }else{
            WLog('Wxpay_Callback','回调校验失败！:'.array2string($result));
          	$WxPay->Return_Result('FAIL','fail');
        }
	}
  	//支付确认
  	public function ConfirmPay($PayOID,$tran_id,$trade_type="")
    {
        $pay_order=db('pay_record')->where('id',$PayOID)->find();
        if($pay_order['status']==0){
          //支付成功后立即到账
          $this->MC->ChargeBalance($pay_order['uid'],$pay_order['amount']);
          db('pay_record')->where('id',$PayOID)->update(['status'=>1,'payoid'=>$tran_id]);
          return true;
        }else{
          return true;
        }
    }
}