<?php
namespace app\index\controller;

use \app\base\Tally;
/*
	自动处理程序 
	此控制器被外部程序自动处理程序 轮训调用 
	本控制器只允许本地服务器访问
*/

class Auto extends Base
{
	public $MC=null;
    public function _initialize()
    {
    	//执行父类的构造方法
    	parent::_initialize();
		
			Config('app_debug',false);
			Config('app_trace',false);
			
			$ip=GetIP();
			$white_list=$this->Config['autoexec_whitelist'];
			
			if($ip!=$white_list){
				s('<h1>非法访问</h1>');
				exit();
			}
	}
	
	public function index()
	{
		echo '<h2>自动执行任务列表</h2>';
		//echo '1、<a href="'.url('auto/update_collect').'" target="_blank">更新统计访问记录-(请将此程序加入linux/windows计划任务中)</a>';
	}

    
}