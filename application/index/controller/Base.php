<?php
namespace app\index\controller;

/*
	前端基本核心控制器 
*/

use \app\base\{Common};

class Base extends Common
{
	//登录后用于存放会员信息
	public $UserInfo;
	public $LoginStatus;
	public $MC=null;
	
    public function _initialize()
    {
    	//执行父类的构造方法
    	parent::_initialize();
		
    	//网站开关
    	if($this->Config['webswitch']==0){
    		$this->error('网站正在维护中！');
    		exit();
    	}

    	$this->assign('WebConfig',$this->Config);
    	$this->assign('UserInfo',$this->UserInfo);
    	$this->assign('LoginStatus',$this->LoginStatus);
    }
    /*
      	 输出验证码
    */
    public function Show_CheckCode()
    {
        $checkcode = GetRandStr(4);
        session('checkbox', $checkcode);
        \app\base\Image::getAuthImage($checkcode);
    }
    /*
    	获得当前验证码
    */
    public function Get_CheckCode()
    {
        return session('checkbox');
    }
    /*
    	重置验证码
    */
    public function Reset_CheckCode()
    {
        $checkcode = GetRandStr(4);
        session('checkbox', $checkcode);
        return $checkcode;
    }
	/*
		判断验证码是否正确
	*/
	public function Verify_CheckCode($checkcode)
	{
		$cc=strtolower(session('checkbox'));
		$checkcode=strtolower($checkcode);
		if($checkcode!="" && $cc==$checkcode){
			return true;
		}else{
			return false;
		}
	}	
}